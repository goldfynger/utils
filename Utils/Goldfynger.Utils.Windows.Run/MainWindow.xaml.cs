﻿using System.Diagnostics;
using System.Windows;

using Goldfynger.Utils.Windows.Console;

namespace Goldfynger.Utils.Windows.Run
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            ConsoleHelper.Instantiate();

            Trace.Listeners.Clear();
            Trace.Listeners.Add(new ColoredConsoleTraceListener());

            bTraceInformation.Click += (sender, e) => Trace.TraceInformation(tbTraceInformation.Text);
            bTraceWarning.Click += (sender, e) => Trace.TraceWarning(tbTraceWarning.Text);
            bTraceError.Click += (sender, e) => Trace.TraceError(tbTraceError.Text);
            bWriteLine.Click += (sender, e) => Trace.WriteLine(tbWriteLine.Text);
            bFail.Click += (sender, e) => Trace.Fail(tbFail.Text);
        }
    }
}
