﻿using System.Windows;

namespace Goldfynger.Utils.Wpf.Commands
{
    /// <summary>Base class for event bindings attaching.</summary>
    /// <remarks><see href="https://www.codeproject.com/Articles/274982/Commands-in-MVVM">Commands in MVVM</see></remarks>
    public static class EventBindings
    {
        /// <summary>Event bindings collection attached <see cref="DependencyProperty"/>.</summary>
        public static readonly DependencyProperty EventBindingsProperty =
            DependencyProperty.RegisterAttached("EventBindings", typeof(EventBindingCollection), typeof(EventBindings), new PropertyMetadata(defaultValue: null, propertyChangedCallback: OnEventBindingsChanged));


        /// <summary>Gets event bindings collection for specified <see cref="DependencyObject"/>.</summary>
        /// <param name="o"><see cref="DependencyObject"/>.</param>
        /// <returns>Event binding collection.</returns>
        public static EventBindingCollection GetEventBindings(DependencyObject o) => (EventBindingCollection)o.GetValue(EventBindingsProperty);

        /// <summary>Sets event bindings collection for specified <see cref="DependencyObject"/>.</summary>
        /// <param name="o"><see cref="DependencyObject"/>.</param>
        /// <param name="value">Event binding collection.</param>
        public static void SetEventBindings(DependencyObject o, EventBindingCollection value) => o.SetValue(EventBindingsProperty, value);

        private static void OnEventBindingsChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            if (args.NewValue is EventBindingCollection newEventBindings)
            {
                foreach (var binding in newEventBindings)
                {
                    binding.Bind(o);
                }
            }
        }
    }
}