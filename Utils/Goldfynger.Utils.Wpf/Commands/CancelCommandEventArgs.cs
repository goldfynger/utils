﻿namespace Goldfynger.Utils.Wpf.Commands
{
    /// <summary>Delegate that used for event before command execution.</summary>
    /// <param name="sender">Command sender.</param>
    /// <param name="e">Event arguments.</param>
    public delegate void CancelCommandEventHandler(object sender, CancelCommandEventArgs e);


    /// <summary>Event arguments that used for event before command execution.</summary>
    public sealed class CancelCommandEventArgs : CommandEventArgs
    {
        /// <summary>Creates new <see cref="CancelCommandEventArgs"/>.</summary>
        /// <param name="parameter">Command parameter.</param>
        public CancelCommandEventArgs(object? parameter) : base(parameter)
        {
        }


        /// <summary>Can be set to <see langword="true"/> to cancel command execution.</summary>
        public bool IsCanceled { get; set; }
    }
}
