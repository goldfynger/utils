﻿using System;

namespace Goldfynger.Utils.Wpf.Commands
{
    /// <summary>Delegate that used for command execution events.</summary>
    /// <param name="sender">Command sender.</param>
    /// <param name="e">Event arguments.</param>
    public delegate void CommandEventHandler(object sender, CommandEventArgs e);


    /// <summary>Event arguments that used for command execution events.</summary>
    public class CommandEventArgs : EventArgs
    {
        /// <summary>Creates new <see cref="CommandEventArgs"/>.</summary>
        /// <param name="parameter">Command parameter.</param>
        public CommandEventArgs(object? parameter) => Parameter = parameter;


        /// <summary>Command parameter.</summary>
        public object? Parameter { get; }
    }
}
