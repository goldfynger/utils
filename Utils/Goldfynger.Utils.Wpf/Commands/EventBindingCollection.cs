﻿using System.Windows;

namespace Goldfynger.Utils.Wpf.Commands
{
    /// <summary>Collection of <see cref="EventBinding"/>.</summary>
    /// <remarks><see href="https://www.codeproject.com/Articles/274982/Commands-in-MVVM">Commands in MVVM</see></remarks>
    public sealed class EventBindingCollection : FreezableCollection<EventBinding>
    {
    }
}