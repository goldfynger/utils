﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

using Goldfynger.Utils.Extensions;

namespace Goldfynger.Utils.Wpf.Commands
{
    /// <summary>Asynchronous implemetation of <see cref="ICommand"/> interface with <see cref="DependencyProperty"/> <see cref="DependencyCommand.AllowExecute"/>.</summary>
    /// <remarks><see href="https://www.codeproject.com/Articles/274982/Commands-in-MVVM">Commands in MVVM</see></remarks>
    public class AsyncDependencyCommand : DependencyCommand, IAsyncCommand
    {        
        private static readonly DependencyPropertyKey IsExecutingPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(IsExecuting), typeof(bool), typeof(AsyncDependencyCommand),
                new PropertyMetadata { DefaultValue = false, PropertyChangedCallback = IsExecuting_PropertyChangedCallback });
        /// <summary>Is command executing now <see cref="DependencyProperty"/>.</summary>
        public static readonly DependencyProperty IsExecutingProperty = IsExecutingPropertyKey.DependencyProperty;

        private readonly Func<Task>? _function = null;
        private readonly Func<object?, Task>? _parameterizedFunction = null;

        private readonly Action<Exception>? _onFault = null;


        /// <summary>Can be used in inheritors.</summary>
        protected AsyncDependencyCommand(Action<Exception>? onFault = null) => _onFault = onFault;

        /// <summary>Creates new <see cref="AsyncDependencyCommand"/> that allowed by default.</summary>
        /// <param name="function">Function to perform.</param>
        /// <param name="onFault">Action that called on fault.</param>
        public AsyncDependencyCommand(Func<Task> function, Action<Exception>? onFault = null)
        {
            _function = function;
            _onFault = onFault;
        }

        /// <summary>Creates new <see cref="AsyncDependencyCommand"/> that allowed by default.</summary>
        /// <param name="parameterizedFunction">Parametrized function to perform.</param>
        /// <param name="onFault">Action that called on fault.</param>
        public AsyncDependencyCommand(Func<object?, Task> parameterizedFunction, Action<Exception>? onFault = null)
        {
            _parameterizedFunction = parameterizedFunction;
            _onFault = onFault;
        }


        private static void IsExecuting_PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e) => ((AsyncDependencyCommand)d).InvokeCanExecuteChanged();


        /// <summary>Is command executing now. Can be used directly or as <see cref="DependencyProperty"/>.</summary>
        public bool IsExecuting
        {
            get => (bool)GetValue(IsExecutingProperty);
            protected set => SetValue(IsExecutingPropertyKey, value);
        }


        /// <summary>Execute command with parameter with fire-and-forget semantic.</summary>
        /// <param name="parameter">Command parameter.</param>
        public override sealed void DoExecute(object? parameter) => DoExecuteAsync(parameter).FireAndForgetAsync(_onFault);

        /// <summary>Overrides function and throws <see cref="NotSupportedException"/>.</summary>
        /// <param name="parameter">Command parameter.</param>
        protected override sealed void InvokeAction(object? parameter) => throw new NotSupportedException();

        /// <summary>Adds additional condition - command can be not executed more than once simultaneously.</summary>
        /// <param name="parameter">Command parameter.</param>
        /// <returns>Is command can be executed.</returns>
        protected override bool CanExecuteProtected(object? parameter) => AllowExecute && !IsExecuting;


        /// <summary>Execute command without parameter with async-wait-for-result semantic.</summary>
        /// <returns>Wait task.</returns>
        public async Task DoExecuteAsync() => await DoExecuteAsync(null);

        /// <summary>Execute command with parameter with async-wait-for-result semantic.</summary>
        /// <param name="parameter">Command parameter.</param>
        /// <returns>Wait task.</returns>
        public virtual async Task DoExecuteAsync(object? parameter)
        {
            if (!CanExecuteProtected(parameter))
            {
                return;
            }

            CancelCommandEventArgs e = new(parameter) { IsCanceled = false };
            InvokeExecuting(e);
            if (e.IsCanceled)
            {
                return;
            }

            try
            {
                IsExecuting = true;
                InvokeCanExecuteChanged();

                await InvokeFunctionAsync(parameter); /* Possible exception will be thrown here. */

                InvokeExecuted(parameter); /* Called in case of successful execution. */
            }
            finally
            {
                IsExecuting = false;
                InvokeCanExecuteChanged();
            }
        }

        /// <summary>Performs command function.</summary>
        /// <param name="parameter">Command parameter.</param>
        protected virtual async Task InvokeFunctionAsync(object? parameter)
        {
            if (_function != null)
            {
                await _function();
            }

            if (_parameterizedFunction != null)
            {
                await _parameterizedFunction(parameter);
            }
        }


        Task IAsyncCommand.ExecuteAsync(object? parameter) => DoExecuteAsync(parameter);
    }

    /// <summary>Generic asynchronous implemetation of <see cref="ICommand"/> interface with <see cref="DependencyProperty"/> <see cref="DependencyCommand.AllowExecute"/>.</summary>
    public class AsyncDependencyCommand<T> : AsyncDependencyCommand
    {
        private readonly Func<T?, Task> _genericParameterizedFunction;


        /// <summary>Creates new <see cref="AsyncDependencyCommand{T}"/> that allowed by default.</summary>
        /// <param name="parameterizedFunction">Parametrized function to perform.</param>
        /// <param name="onFault">Action that called on fault.</param>
        public AsyncDependencyCommand(Func<T?, Task> parameterizedFunction, Action<Exception>? onFault = null) : base(onFault) => _genericParameterizedFunction = parameterizedFunction;


        /// <summary>Execute command with parameter with async-wait-for-result semantic.</summary>
        /// <param name="parameter">Command parameter.</param>
        /// <returns>Wait task.</returns>
        public override async Task DoExecuteAsync(object? parameter)
        {
            if (parameter is T t)
            {
                await DoExecuteAsync(t);
            }
            else
            {
                await DoExecuteAsync(default);
            }
        }

        /// <summary>Execute command with parameter with async-wait-for-result semantic.</summary>
        /// <param name="parameter">Command parameter.</param>
        /// <returns>Wait task.</returns>
        public async Task DoExecuteAsync(T? parameter)
        {
            if (!CanExecuteProtected(parameter))
            {
                return;
            }

            CancelCommandEventArgs e = new(parameter) { IsCanceled = false };
            InvokeExecuting(e);
            if (e.IsCanceled)
            {
                return;
            }

            try
            {
                IsExecuting = true;
                InvokeCanExecuteChanged();

                await InvokeFunctionAsync(parameter); /* Possible exception will be thrown here. */

                InvokeExecuted(parameter); /* Called in case of successful execution. */
            }
            finally
            {
                IsExecuting = false;
                InvokeCanExecuteChanged();
            }
        }

        /// <summary>Performs command function.</summary>
        /// <param name="parameter">Command parameter.</param>
        protected override async Task InvokeFunctionAsync(object? parameter)
        {
            if (parameter is T t)
            {
                await InvokeFunctionAsync(t);
            }
            else
            {
                await InvokeFunctionAsync(default);
            }
        }

        /// <summary>Performs command function.</summary>
        /// <param name="parameter">Command parameter.</param>
        protected async Task InvokeFunctionAsync(T? parameter)
        {
            if (_genericParameterizedFunction != null)
            {
                await _genericParameterizedFunction(parameter);
            }
        }
    }
}
