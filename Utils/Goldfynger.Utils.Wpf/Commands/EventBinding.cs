﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Windows;
using System.Windows.Input;

namespace Goldfynger.Utils.Wpf.Commands
{
    /// <summary>Bind event to specified command with optional parameter.</summary>
    /// <remarks><see href="https://www.codeproject.com/Articles/274982/Commands-in-MVVM">Commands in MVVM</see></remarks>
    public class EventBinding : Freezable
    {
        /// <summary>Name of event that calls command execution <see cref="DependencyProperty"/>.</summary>
        public static readonly DependencyProperty EventNameProperty = DependencyProperty.Register(nameof(EventName), typeof(string), typeof(EventBinding), new PropertyMetadata(defaultValue: null));

        /// <summary>Target command <see cref="DependencyProperty"/>.</summary>
        public static readonly DependencyProperty CommandProperty = DependencyProperty.Register(nameof(Command), typeof(ICommand), typeof(EventBinding), new PropertyMetadata(defaultValue: null));

        /// <summary>Command parameter <see cref="DependencyProperty"/>.</summary>
        public static readonly DependencyProperty CommandParameterProperty = DependencyProperty.Register(nameof(CommandParameter), typeof(object), typeof(EventBinding), new PropertyMetadata(defaultValue: null));


        /// <summary>Name of event that calls command execution.</summary>
        public string? EventName
        {
            get => (string)GetValue(EventNameProperty);
            set => SetValue(EventNameProperty, value);
        }

        /// <summary>Target command.</summary>
        public ICommand? Command
        {
            get => (ICommand)GetValue(CommandProperty);
            set => SetValue(CommandProperty, value);
        }

        /// <summary>Command parameter.</summary>
        public object? CommandParameter
        {
            get => GetValue(CommandParameterProperty);
            set => SetValue(CommandParameterProperty, value);
        }


        /// <summary>Creates a new instance of the <see cref="EventBinding"/>.</summary>
        /// <returns>The new instance.</returns>
        protected override Freezable CreateInstanceCore() => new EventBinding();

        /// <summary>Bind event of object to command.</summary>
        /// <param name="o">Object where event can be caused.</param>
        public void Bind(object o)
        {
            try
            {
                var eventInfo = o.GetType().GetEvent(EventName!)!;

                var methodInfo = GetType().GetMethod(nameof(EventProxy), BindingFlags.NonPublic | BindingFlags.Instance)!;

                var del = Delegate.CreateDelegate(eventInfo.EventHandlerType!, this, methodInfo);

                eventInfo.RemoveEventHandler(o, del);
                eventInfo.AddEventHandler(o, del);
            }
            catch (Exception ex)
            {
                Debug.Fail("Event binding exception.", ex.ToString());
            }
        }

        private void EventProxy(object o, EventArgs e) => Command?.Execute(CommandParameter);
    }
}