﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace Goldfynger.Utils.Wpf.Commands
{
    /// <summary>Async extension of <see cref="ICommand"/> interface.</summary>
    public interface IAsyncCommand : ICommand
    {
        /// <summary>Execute command async.</summary>
        /// <param name="parameter">Data used by the command. If the command does not require data to be passed, this object can be set to null.</param>
        /// <returns>Task to async execution.</returns>
        Task ExecuteAsync(object? parameter);
    }
}
