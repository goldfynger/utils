﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Goldfynger.Utils.Wpf.Commands
{
    /// <summary>Implemetation of <see cref="ICommand"/> interface with <see cref="DependencyProperty"/> <see cref="DependencyCommand.AllowExecute"/>.</summary>
    /// <remarks><see href="https://www.codeproject.com/Articles/274982/Commands-in-MVVM">Commands in MVVM</see></remarks>
    public class DependencyCommand : DependencyObject, ICommand
    {
        /// <summary>Is command execution allowed <see cref="DependencyProperty"/>.</summary>
        public static readonly DependencyProperty AllowExecuteProperty =
            DependencyProperty.Register(nameof(AllowExecute), typeof(bool), typeof(DependencyCommand),
                new PropertyMetadata { DefaultValue = true, PropertyChangedCallback = AllowExecute_PropertyChangedCallback });

        private readonly Action? _action = null;
        private readonly Action<object?>? _parameterizedAction = null;


        /// <summary>Event occurs when command "Can be executed" state (result of <see cref="ICommand.CanExecute(object?)"/>) was changed.</summary>
        /// <remarks>It is a part of <see cref="ICommand"/> interface.
        /// "State" of this virtual "property" (result of <see cref="ICommand.CanExecute(object?)"/>) can be <see langword="false"/> even if <see cref="AllowExecute"/> is <see langword="true"/>
        /// if there is additional conditions created - for example if async command is executing now and can be not run one more time before current execution is ended.</remarks>
        public event EventHandler? CanExecuteChanged;

        /// <summary>Event occurs before command execution and can be used to cancel execution.</summary>
        public event CancelCommandEventHandler? Executing;
        /// <summary>Event occurs after command successful execution.</summary>
        public event CommandEventHandler? Executed;


        /// <summary>Can be used in inheritors.</summary>
        protected DependencyCommand()
        {
        }

        /// <summary>Creates new <see cref="DependencyCommand"/> that allowed by default.</summary>
        /// <param name="action">Action to perform.</param>
        public DependencyCommand(Action action) => _action = action;

        /// <summary>Creates new <see cref="DependencyCommand"/> that allowed by default.</summary>
        /// <param name="parameterizedAction">Parametrized action to perform.</param>
        public DependencyCommand(Action<object?> parameterizedAction) => _parameterizedAction = parameterizedAction;


        private static void AllowExecute_PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e) => ((DependencyCommand)d).InvokeCanExecuteChanged();


        /// <summary>Is command execution allowed. Can be used directly or as <see cref="DependencyProperty"/>. Default value is <see langword="true"/>.</summary>
        public bool AllowExecute
        {
            get => (bool)GetValue(AllowExecuteProperty);
            set => SetValue(AllowExecuteProperty, value);
        }


        /// <summary>Execute command without parameter.</summary>
        public void DoExecute() => DoExecute(null);

        /// <summary>Execute command with parameter.</summary>
        /// <param name="parameter">Command parameter.</param>
        public virtual void DoExecute(object? parameter)
        {
            if (!CanExecuteProtected(parameter))
            {
                return;
            }

            CancelCommandEventArgs e = new(parameter) { IsCanceled = false };
            InvokeExecuting(e);
            if (e.IsCanceled)
            {
                return;
            }

            InvokeAction(parameter); /* Possible exception will be thrown here. */

            InvokeExecuted(parameter); /* Called in case of successful execution. */
        }

        /// <summary>Performs command action.</summary>
        /// <param name="parameter">Command parameter.</param>
        protected virtual void InvokeAction(object? parameter)
        {
            _action?.Invoke();
            _parameterizedAction?.Invoke(parameter);
        }


        /// <summary>Fires <see cref="Executing"/> event.</summary>
        /// <param name="e">Event args to cancel command.</param>
        protected void InvokeExecuting(CancelCommandEventArgs e) => Executing?.Invoke(this, e);
        /// <summary>Fires <see cref="Executed"/> event.</summary>
        /// <param name="parameter">Command parameter.</param>
        protected void InvokeExecuted(object? parameter) => Executed?.Invoke(this, new CommandEventArgs(parameter));

        /// <summary>Can be overrided to create additional conditions to "switch off" <see cref="CanExecuteChanged"/> event even if <see cref="AllowExecute"/> is <see langword="true"/>.</summary>
        protected virtual bool CanExecuteProtected(object? parameter) => AllowExecute;

        /// <summary>Schould be called when result of <see cref="CanExecuteProtected"/> is changed.</summary>
        protected void InvokeCanExecuteChanged() => CanExecuteChanged?.Invoke(this, EventArgs.Empty);


        bool ICommand.CanExecute(object? parameter) => CanExecuteProtected(parameter);
        void ICommand.Execute(object? parameter) => DoExecute(parameter);
    }

    /// <summary>Generic implemetation of <see cref="ICommand"/> interface with <see cref="DependencyProperty"/> <see cref="DependencyCommand.AllowExecute"/>.</summary>
    public class DependencyCommand<T> : DependencyCommand
    {
        private readonly Action<T?> _genericParameterizedAction;


        /// <summary>Creates new <see cref="DependencyCommand{T}"/> that allowed by default.</summary>
        /// <param name="parametrizedAction">Parametrized action to perform.</param>
        public DependencyCommand(Action<T?> parametrizedAction) => _genericParameterizedAction = parametrizedAction;


        /// <summary>Execute command with parameter.</summary>
        /// <param name="parameter">Command parameter.</param>
        public override void DoExecute(object? parameter)
        {
            if (parameter is T t)
            {
                DoExecute(t);
            }
            else
            {
                DoExecute(default);
            }
        }

        /// <summary>Execute command with parameter.</summary>
        /// <param name="parameter">Command parameter.</param>
        public void DoExecute(T? parameter)
        {
            if (!CanExecuteProtected(parameter))
            {
                return;
            }

            CancelCommandEventArgs e = new(parameter) { IsCanceled = false };
            InvokeExecuting(e);
            if (e.IsCanceled)
            {
                return;
            }

            InvokeAction(parameter); /* Possible exception will be thrown here. */

            InvokeExecuted(parameter); /* Called in case of successful execution. */
        }

        /// <summary>Performs command action.</summary>
        /// <param name="parameter">Command parameter.</param>
        protected override void InvokeAction(object? parameter)
        {
            if (parameter is T t)
            {
                InvokeAction(t);
            }
            else
            {
                InvokeAction(default);
            }
        }

        /// <summary>Performs command action.</summary>
        /// <param name="parameter">Command parameter.</param>
        protected void InvokeAction(T? parameter) => _genericParameterizedAction?.Invoke(parameter);
    }
}
