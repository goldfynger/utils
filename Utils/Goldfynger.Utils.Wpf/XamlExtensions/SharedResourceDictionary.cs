﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Goldfynger.Utils.Wpf.XamlExtensions
{
    /// <summary>The shared resource dictionary is a specialized resource dictionary that loads it content only once.
    /// If a second instance with the same source is created, it only merges the resources from the cache.</summary>
    /// <remarks><see href="https://stackoverflow.com/a/27505205">SharedResourceDictionary at stackoverflow.com</see></remarks>
    public class SharedResourceDictionary : ResourceDictionary
    {
        /// <summary>Cache of loaded dictionaries.</summary>
        public static readonly Dictionary<Uri, ResourceDictionary> SharedDictionaries = new();


        private Uri? _sourceUri;


        /// <summary>Gets or sets the uniform resource identifier (URI) to load resources from.</summary>
        public new Uri? Source
        {
            get { return _sourceUri; }
            set
            {
                if (value == null)
                {
                    _sourceUri = null;
                }
                else
                {
                    /* Create new URI to prevent memory leak (https://stackoverflow.com/a/13484028). */
                    _sourceUri = new Uri(value.OriginalString, UriKind.RelativeOrAbsolute);

                    /* Uri overrides Equals function and different Uri objects with same original string can be compared here. */
                    if (!SharedDictionaries.ContainsKey(value))
                    {
                        /* If the dictionary is not yet loaded, load it by setting the source of the base class. */
                        base.Source = value;

                        /* And add it to the cache */
                        SharedDictionaries.Add(value, this);
                    }
                    else
                    {
                        /* If the dictionary is already loaded, get it from the cache. */
                        MergedDictionaries.Add(SharedDictionaries[value]);
                    }
                }
            }
        }
    }
}