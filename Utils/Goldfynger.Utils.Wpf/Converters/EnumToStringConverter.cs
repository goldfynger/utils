﻿using System;
using System.Globalization;
using System.Windows;

namespace Goldfynger.Utils.Wpf.Converters
{
    /// <summary>Converter that converts enum value of specified type to <see cref="string"/> and back.</summary>
    /// <typeparam name="TEnum">Type of enum.</typeparam>
    public sealed class EnumToStringConverter<TEnum> : SingletonConverterBase<EnumToStringConverter<TEnum>> where TEnum : struct, Enum
    {
        /// <summary>Converts enum value to string.</summary>
        /// <param name="value">Specified enum value.</param>
        /// <param name="targetType">Not used.</param>
        /// <param name="parameter">Not used.</param>
        /// <param name="culture">Not used.</param>
        /// <returns>Converted string value or <see cref="DependencyProperty.UnsetValue"/> if specified value is not element of <typeparamref name="TEnum"/>.</returns>
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture) => value is TEnum e ? e.ToString() : DependencyProperty.UnsetValue;

        /// <summary>Converts string value to enum.</summary>
        /// <param name="value">Specified string value.</param>
        /// <param name="targetType">Not used.</param>
        /// <param name="parameter">Not used.</param>
        /// <param name="culture">Not used.</param>
        /// <returns>Converted enum value or <see cref="DependencyProperty.UnsetValue"/> if specified value is not <see cref="string"/> or not element of <typeparamref name="TEnum"/>.</returns>
        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => value is string s && Enum.TryParse(s, out TEnum e) ? e : DependencyProperty.UnsetValue;
    }
}
