﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Goldfynger.Utils.Wpf.Converters
{
    /// <summary>Converter that uses <see cref="object.Equals(object?)"/> function to compare array of objects with contained instance.</summary>
    public sealed class IsValuesEqualsMultiConverter : IMultiValueConverter
    {
        private readonly object? _value;


        /// <summary>Creates new converter instance.</summary>
        /// <param name="value">The object to compare.</param>
        public IsValuesEqualsMultiConverter(object? value) => _value = value;


        /// <summary>Compares the contained instance with each objects of array.</summary>
        /// <param name="values">An array of objects to compare with contained instance.</param>
        /// <param name="targetType">Not used.</param>
        /// <param name="parameter">Not used.</param>
        /// <param name="culture">Not used.</param>
        /// <returns><see langword="true"/> if each object of array is equal to the contained instance; otherwise, <see langword="false"/>.</returns>
        public object Convert(object?[] values, Type targetType, object parameter, CultureInfo culture) => values.All(v => v == null ? _value == null : v.Equals(_value));

        /// <summary>Not supported.</summary>
        /// <param name="value">Not used.</param>
        /// <param name="targetTypes">Not used.</param>
        /// <param name="parameter">Not used.</param>
        /// <param name="culture">Not used.</param>
        /// <returns>Not supported.</returns>
        /// <exception cref="NotSupportedException"/>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture) => throw new NotSupportedException();
    }
}
