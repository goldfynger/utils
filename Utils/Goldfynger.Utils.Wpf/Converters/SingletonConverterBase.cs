﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Goldfynger.Utils.Wpf.Converters
{
    /// <summary>Base of <see cref="IValueConverter"/> interface extension with singleton support and lazy initialization.</summary>
    /// <typeparam name="TConverter">Child converter type.</typeparam>
    public abstract class SingletonConverterBase<TConverter> : IValueConverter where TConverter : SingletonConverterBase<TConverter>, new()
    {
        private static readonly Lazy<TConverter> _lazyInstance = new(() => new TConverter());


        /// <summary>Singleton instance.</summary>
        public static TConverter Instance => _lazyInstance.Value;


        /// <summary>Converts a value.</summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>A converted value. If the method returns <see langword="null"/>, the valid <see langword="null"/> value is used.</returns>
        public abstract object Convert(object value, Type targetType, object parameter, CultureInfo culture);

        /// <summary>Converts a value.</summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>A converted value. If the method returns <see langword="null"/>, the valid <see langword="null"/> value is used.</returns>
        public abstract object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture);
    }
}
