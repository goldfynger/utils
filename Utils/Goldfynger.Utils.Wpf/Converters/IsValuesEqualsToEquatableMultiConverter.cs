﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Goldfynger.Utils.Wpf.Converters
{
    /// <summary>Converter that uses <see cref="IEquatable{T}"/> interface to compare array of objects with contained instance.</summary>
    /// <typeparam name="T">Type of objects.</typeparam>
    public sealed class IsValuesEqualsToEquatableMultiConverter<T> : IMultiValueConverter where T : IEquatable<T>
    {
        private readonly T _value;


        /// <summary>Creates new converter instance.</summary>
        /// <param name="value">The object to compare.</param>
        public IsValuesEqualsToEquatableMultiConverter(T value) => _value = value;


        /// <summary>Compares the contained instance with each objects of array.</summary>
        /// <param name="values">An array of objects to compare with contained instance.</param>
        /// <param name="targetType">Not used.</param>
        /// <param name="parameter">Not used.</param>
        /// <param name="culture">Not used.</param>
        /// <returns><see langword="true"/> if each object of array is equal to the contained instance; otherwise, <see langword="false"/>.</returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var typedValues = values.OfType<T>().ToList();

            return typedValues.Count == values.Length && typedValues.All(t => t.Equals(_value));
        }

        /// <summary>Not supported.</summary>
        /// <param name="value">Not used.</param>
        /// <param name="targetTypes">Not used.</param>
        /// <param name="parameter">Not used.</param>
        /// <param name="culture">Not used.</param>
        /// <returns>Not supported.</returns>
        /// <exception cref="NotSupportedException"/>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture) => throw new NotSupportedException();
    }
}
