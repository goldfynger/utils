﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Goldfynger.Utils.Wpf.Converters
{
    /// <summary>Converter that uses <see cref="IComparable{T}"/> interface to compare specified object with contained instance.</summary>
    /// <typeparam name="T">Type of objects.</typeparam>
    public sealed class IsValueEqualsToComparableConverter<T> : IValueConverter where T : IComparable
    {
        private readonly T _value;


        /// <summary>Creates new converter instance.</summary>
        /// <param name="value">The object to compare.</param>
        public IsValueEqualsToComparableConverter(T value) => _value = value;


        /// <summary>Compares the contained instance with another object of the same type.</summary>
        /// <param name="value">An object to compare with contained instance.</param>
        /// <param name="targetType">Not used.</param>
        /// <param name="parameter">Not used.</param>
        /// <param name="culture">Not used.</param>
        /// <returns><see langword="true"/> if the specified object is equal to the contained instance; otherwise, <see langword="false"/>.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) => value is T t && t.CompareTo(_value) == 0;

        /// <summary>Not supported.</summary>
        /// <param name="value">Not used.</param>
        /// <param name="targetType">Not used.</param>
        /// <param name="parameter">Not used.</param>
        /// <param name="culture">Not used.</param>
        /// <returns>Not supported.</returns>
        /// <exception cref="NotSupportedException"/>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotSupportedException();
    }
}
