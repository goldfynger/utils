﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Goldfynger.Utils.Wpf.Converters
{
    /// <summary>Converter that uses <see cref="object.Equals(object?)"/> function to compare specified object with contained instance.</summary>
    public sealed class IsValueEqualsConverter : IValueConverter
    {
        private readonly object? _value;


        /// <summary>Creates new converter instance.</summary>
        /// <param name="value">The object to compare.</param>
        public IsValueEqualsConverter(object? value) => _value = value;


        /// <summary>Compares the contained instance with another object.</summary>
        /// <param name="value">An object to compare with contained instance.</param>
        /// <param name="targetType">Not used.</param>
        /// <param name="parameter">Not used.</param>
        /// <param name="culture">Not used.</param>
        /// <returns><see langword="true"/> if the specified object is equal to the contained instance; otherwise, <see langword="false"/>.</returns>
        public object Convert(object? value, Type targetType, object parameter, CultureInfo culture) => value == null ? _value == null : value.Equals(_value);

        /// <summary>Not supported.</summary>
        /// <param name="value">Not used.</param>
        /// <param name="targetType">Not used.</param>
        /// <param name="parameter">Not used.</param>
        /// <param name="culture">Not used.</param>
        /// <returns>Not supported.</returns>
        /// <exception cref="NotSupportedException"/>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotSupportedException();
    }
}
