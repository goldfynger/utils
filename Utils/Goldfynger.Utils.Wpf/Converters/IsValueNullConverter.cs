﻿using System;
using System.Globalization;

namespace Goldfynger.Utils.Wpf.Converters
{
    /// <summary>Converter that compares specified object and <see langword="null"/>.</summary>
    public sealed class IsValueNullConverter : SingletonConverterBase<IsValueNullConverter>
    {
        /// <summary>Compares specified object and <see langword="null"/>.</summary>
        /// <param name="value">An object to compare with <see langword="null"/>.</param>
        /// <param name="targetType">Not used.</param>
        /// <param name="parameter">Not used.</param>
        /// <param name="culture">Not used.</param>
        /// <returns><see langword="true"/> if the specified object is <see langword="null"/>; otherwise, <see langword="false"/>.</returns>
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture) => value == null;

        /// <summary>Not supported.</summary>
        /// <param name="value">Not used.</param>
        /// <param name="targetType">Not used.</param>
        /// <param name="parameter">Not used.</param>
        /// <param name="culture">Not used.</param>
        /// <returns>Not supported.</returns>
        /// <exception cref="NotSupportedException"/>
        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotSupportedException();
    }
}
