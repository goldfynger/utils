﻿using System;
using System.Globalization;
using System.Linq;

namespace Goldfynger.Utils.Wpf.Converters
{
    /// <summary>Converter that compares each object of array and <see langword="null"/>.</summary>
    public sealed class IsValuesNotNullMultiConverter : SingletonMultiConverterBase<IsValuesNotNullMultiConverter>
    {
        /// <summary>Compares each object of array and <see langword="null"/>.</summary>
        /// <param name="values">An array of objects to compare  with <see langword="null"/>.</param>
        /// <param name="targetType">Not used.</param>
        /// <param name="parameter">Not used.</param>
        /// <param name="culture">Not used.</param>
        /// <returns><see langword="true"/> if each object of array is not <see langword="null"/>; otherwise, <see langword="false"/>.</returns>
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture) => values.All(v => v != null);

        /// <summary>Not supported.</summary>
        /// <param name="value">Not used.</param>
        /// <param name="targetTypes">Not used.</param>
        /// <param name="parameter">Not used.</param>
        /// <param name="culture">Not used.</param>
        /// <returns>Not supported.</returns>
        /// <exception cref="NotSupportedException"/>
        public override object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture) => throw new NotSupportedException();
    }
}
