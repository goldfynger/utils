﻿using System;
using System.Globalization;
using System.Windows;

namespace Goldfynger.Utils.Wpf.Converters
{
    /// <summary>Converter that inverses boolean value.</summary>
    public sealed class InverseBooleanConverter : SingletonConverterBase<InverseBooleanConverter>
    {
        /// <summary>Inverse boolean value.</summary>
        /// <param name="value">Value to inverse.</param>
        /// <param name="targetType">Not used.</param>
        /// <param name="parameter">Not used.</param>
        /// <param name="culture">Not used.</param>
        /// <returns>Inversed boolean value or <see cref="DependencyProperty.UnsetValue"/> if specified value is not <see langword="true"/> or <see langword="false"/>.</returns>
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture) => value is bool b ? !b : DependencyProperty.UnsetValue;

        /// <summary>Inverse boolean value.</summary>
        /// <param name="value">Value to inverse.</param>
        /// <param name="targetType">Not used.</param>
        /// <param name="parameter">Not used.</param>
        /// <param name="culture">Not used.</param>
        /// <returns>Inversed boolean value or <see cref="DependencyProperty.UnsetValue"/> if specified value is not <see langword="true"/> or <see langword="false"/>.</returns>
        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => value is bool b ? !b : DependencyProperty.UnsetValue;
    }
}
