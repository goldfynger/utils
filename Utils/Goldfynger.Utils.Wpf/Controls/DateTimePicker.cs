﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;

using Goldfynger.Utils.Extensions;
using Goldfynger.Utils.Wpf.Commands;

namespace Goldfynger.Utils.Wpf.Controls
{
    /// <summary></summary>
    [TemplatePart(Name = ElementTimeView, Type = typeof(Grid))]
    [TemplatePart(Name = ElementMinuteSecondView, Type = typeof(Grid))]
    [TemplatePart(Name = ElementMonthView, Type = typeof(Grid))]
    [TemplatePart(Name = ElementYearView, Type = typeof(Grid))]
    public sealed class DateTimePicker : Control
    {
        /// <summary></summary>
        private const string ElementTimeView = "PART_TimeView";
        private const string ElementMinuteSecondView = "PART_MinuteSecondView";
        private const string ElementMonthView = "PART_MonthView";
        private const string ElementYearView = "PART_YearView";

        private const string HourGroupName = "HourGroup";
        private const string MinuteDigitOneGroupName = "MinuteDigitOneGroup";
        private const string MinuteDigitTwoGroupName = "MinuteDigitTwoGroup";
        private const string SecondDigitOneGroupName = "SecondDigitOneGroup";
        private const string SecondDigitTwoGroupName = "SecondDigitTwoGroup";
        private const string DayGroupName = "DayGroup";


        static DateTimePicker() => DefaultStyleKeyProperty.OverrideMetadata(typeof(DateTimePicker), new FrameworkPropertyMetadata(typeof(DateTimePicker)));


        /// <summary></summary>
        public static readonly DependencyProperty DateTimeProperty =
            DependencyProperty.Register(nameof(DateTime), typeof(DateTime), typeof(DateTimePicker), new PropertyMetadata(defaultValue: DateTime.Now, DateTimePropertyChanged));

        /// <summary></summary>
        public static readonly DependencyProperty DateViewModeProperty =
            DependencyProperty.Register(nameof(DateViewMode), typeof(DateViewMode), typeof(DateTimePicker), new PropertyMetadata(defaultValue: DateViewMode.Month, DateViewModePropertyChanged));

        private static readonly DependencyPropertyKey DateViewDescriptionPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(DateViewDescription), typeof(string), typeof(DateTimePicker), new PropertyMetadata(defaultValue: null));
        /// <summary></summary>
        public static readonly DependencyProperty DateViewDescriptionProperty = DateViewDescriptionPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey NowDateTimePropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(NowDateTime), typeof(DateTime), typeof(DateTimePicker), new PropertyMetadata(defaultValue: DateTime.Now));
        /// <summary></summary>
        public static readonly DependencyProperty NowDateTimeProperty = NowDateTimePropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey UpDescriptionPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(UpDescription), typeof(string), typeof(DateTimePicker), new PropertyMetadata(defaultValue: null));
        /// <summary></summary>
        public static readonly DependencyProperty UpDescriptionProperty = UpDescriptionPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey PreviousDescriptionPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(PreviousDescription), typeof(string), typeof(DateTimePicker), new PropertyMetadata(defaultValue: null));
        /// <summary></summary>
        public static readonly DependencyProperty PreviousDescriptionProperty = PreviousDescriptionPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey NextDescriptionPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(NextDescription), typeof(string), typeof(DateTimePicker), new PropertyMetadata(defaultValue: null));
        /// <summary></summary>
        public static readonly DependencyProperty NextDescriptionProperty = NextDescriptionPropertyKey.DependencyProperty;


        private static readonly DependencyPropertyKey PreviousCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(PreviousCommand), typeof(DependencyCommand), typeof(DateTimePicker), new PropertyMetadata(defaultValue: null));
        /// <summary></summary>
        public static readonly DependencyProperty PreviousCommandProperty = PreviousCommandPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey UpCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(UpCommand), typeof(DependencyCommand), typeof(DateTimePicker), new PropertyMetadata(defaultValue: null));
        /// <summary></summary>
        public static readonly DependencyProperty UpCommandProperty = UpCommandPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey NextCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(NextCommand), typeof(DependencyCommand), typeof(DateTimePicker), new PropertyMetadata(defaultValue: null));
        /// <summary></summary>
        public static readonly DependencyProperty NextCommandProperty = NextCommandPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey NowCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(NowCommand), typeof(DependencyCommand), typeof(DateTimePicker), new PropertyMetadata(defaultValue: null));
        /// <summary></summary>
        public static readonly DependencyProperty NowCommandProperty = NowCommandPropertyKey.DependencyProperty;


        private static readonly DependencyPropertyKey IsSecondaryPropertyKey =
            DependencyProperty.RegisterAttachedReadOnly("IsSecondary", typeof(bool), typeof(DateTimePicker), new PropertyMetadata(defaultValue: false));
        /// <summary></summary>
        public static readonly DependencyProperty IsSecondaryProperty = IsSecondaryPropertyKey.DependencyProperty;


        /// <summary></summary>
        public static readonly RoutedEvent DateTimeChangedEvent =
            EventManager.RegisterRoutedEvent(nameof(DateTimeChanged), RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<DateTime>), typeof(DateTimePicker));


        private static void DateTimePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (DateTimePicker)d;

            control.DateTimeChanged?.Invoke(control, new RoutedPropertyChangedEventArgs<DateTime>((DateTime)e.OldValue, (DateTime)e.NewValue));

            if (!control._isTemplateApplied) return;

            control.UpdateTimeView();

            control.UpdateDateView();
        }

        private static void DateViewModePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (DateTimePicker)d;

            control.UpCommand.AllowExecute = control.DateViewMode != DateViewMode.Decade;

            control.FillCommandDescriptions();

            if (!control._isTemplateApplied) return;

            control.FillDateView();
        }


        /// <summary></summary>
        public DateTime DateTime
        {
            get => (DateTime)GetValue(DateTimeProperty);
            set => SetValue(DateTimeProperty, value);
        }

        /// <summary></summary>
        public DateViewMode DateViewMode
        {
            get => (DateViewMode)GetValue(DateViewModeProperty);
            set => SetValue(DateViewModeProperty, value);
        }

        /// <summary></summary>
        public string DateViewDescription
        {
            get => (string)GetValue(DateViewDescriptionProperty);
            private set => SetValue(DateViewDescriptionPropertyKey, value);
        }

        /// <summary></summary>
        public DateTime NowDateTime
        {
            get => (DateTime)GetValue(NowDateTimeProperty);
            private set => SetValue(NowDateTimePropertyKey, value);
        }

        /// <summary></summary>
        public string UpDescription
        {
            get => (string)GetValue(UpDescriptionProperty);
            private set => SetValue(UpDescriptionPropertyKey, value);
        }

        /// <summary></summary>
        public string PreviousDescription
        {
            get => (string)GetValue(PreviousDescriptionProperty);
            private set => SetValue(PreviousDescriptionPropertyKey, value);
        }

        /// <summary></summary>
        public string NextDescription
        {
            get => (string)GetValue(NextDescriptionProperty);
            private set => SetValue(NextDescriptionPropertyKey, value);
        }


        /// <summary></summary>
        public DependencyCommand PreviousCommand
        {
            get => (DependencyCommand)GetValue(PreviousCommandProperty);
            private set => SetValue(PreviousCommandPropertyKey, value);
        }

        /// <summary></summary>
        public DependencyCommand UpCommand
        {
            get => (DependencyCommand)GetValue(UpCommandProperty);
            private set => SetValue(UpCommandPropertyKey, value);
        }

        /// <summary></summary>
        public DependencyCommand NextCommand
        {
            get => (DependencyCommand)GetValue(NextCommandProperty);
            private set => SetValue(NextCommandPropertyKey, value);
        }

        /// <summary></summary>
        public DependencyCommand NowCommand
        {
            get => (DependencyCommand)GetValue(NowCommandProperty);
            private set => SetValue(NowCommandPropertyKey, value);
        }


        /// <summary></summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool GetIsSecondary(DependencyObject obj) => (bool)obj.GetValue(IsSecondaryProperty);

        /// <summary></summary>
        /// <param name="obj"></param>
        /// <param name="value"></param>
        private static void SetIsSecondary(DependencyObject obj, bool value) => obj.SetValue(IsSecondaryPropertyKey, value);


        /// <summary></summary>
        public event RoutedPropertyChangedEventHandler<DateTime>? DateTimeChanged;


        private Grid _timeView = null!;
        private Grid _minuteSecondView = null!;
        private Grid _monthView = null!;
        private Grid _yearView = null!;

        private bool _dateTimeLocker;
        private bool _dateViewLocker;

        private DateTime _dateInView;

        private bool _isTemplateApplied;

        private double _timeSmallButtonSide;

        private double _dateTimeButtonMarginSide;


        /// <summary>Creates new instance of <see cref="DateTimePicker"/>.</summary>
        public DateTimePicker()
        {
            PreviousCommand = new DependencyCommand(Previous);
            UpCommand = new DependencyCommand(Up);
            NextCommand = new DependencyCommand(Next);
            NowCommand = new DependencyCommand(() => DateTime = NowDateTime);

            _dateInView = DateTime;

            var timer = new DispatcherTimer(TimeSpan.FromSeconds(1), DispatcherPriority.Normal, (sender, e) => NowDateTime = NowDateTime.AddSeconds(1), Dispatcher);

            NowDateTime = DateTime.Now;

            FillCommandDescriptions();
        }


        /// <summary></summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            _timeSmallButtonSide = (double)FindResource("TimeSmallButtonSide");

            _dateTimeButtonMarginSide = (double)FindResource("DateTimeButtonMarginSide");

            _isTemplateApplied = true;

            _timeView = (GetTemplateChild(ElementTimeView) as Grid)!;
            _minuteSecondView = (GetTemplateChild(ElementMinuteSecondView) as Grid)!;
            _monthView = (GetTemplateChild(ElementMonthView) as Grid)!;
            _yearView = (GetTemplateChild(ElementYearView) as Grid)!;

            FillTimeView();

            FillDateView();
        }

        private void UpdateTimeView()
        {
            LockDateTimeUpdate(() =>
            {
                _timeView!.Children.OfType<RadioButton>().First(rb => rb.GroupName == HourGroupName && (int)rb.Content == DateTime.Hour).IsChecked = true;

                var minuteSecondRadioButtons = _minuteSecondView!.Children.OfType<RadioButton>();

                var minuteDigitOne = DateTime.Minute / 10;
                var minuteDigitTwo = DateTime.Minute % 10;

                var secondDigitOne = DateTime.Second / 10;
                var secondDigitTwo = DateTime.Second % 10;

                minuteSecondRadioButtons.First(rb => rb.GroupName == MinuteDigitOneGroupName && (int)rb.Content == minuteDigitOne).IsChecked = true;
                minuteSecondRadioButtons.First(rb => rb.GroupName == MinuteDigitTwoGroupName && (int)rb.Content == minuteDigitTwo).IsChecked = true;
                minuteSecondRadioButtons.First(rb => rb.GroupName == SecondDigitOneGroupName && (int)rb.Content == secondDigitOne).IsChecked = true;
                minuteSecondRadioButtons.First(rb => rb.GroupName == SecondDigitTwoGroupName && (int)rb.Content == secondDigitTwo).IsChecked = true;
            });

            ComputeTranslations();
        }

        private void UpdateDateView()
        {
            LockDateTimeUpdate(() =>
            {
                DateViewMode = DateViewMode.Month;

                if (!_dateViewLocker)
                {
                    _dateInView = DateTime;
                }

                FillDateView();
            });
        }

        private void LockDateTimeUpdate(Action action)
        {
            if (_dateTimeLocker) return;

            _dateTimeLocker = true;

            action();

            _dateTimeLocker = false;
        }

        private void LockDateViewUpdate(Action action)
        {
            if (_dateViewLocker) return;

            _dateViewLocker = true;

            action();

            _dateViewLocker = false;
        }

        private void FillTimeView()
        {
            if (_timeView!.Children.Count > 1) /* First child is minute second grid. */
            {
                _timeView.Children.RemoveRange(1, _timeView.Children.Count - 1);
            }

            void hourChecked(int hour) =>
                LockDateTimeUpdate(() => DateTime = new DateTime(DateTime.Year, DateTime.Month, DateTime.Day, hour, DateTime.Minute, DateTime.Second));

            void minuteDigitOneChecked(int digitOne) =>
                LockDateTimeUpdate(() => DateTime = new DateTime(DateTime.Year, DateTime.Month, DateTime.Day, DateTime.Hour, digitOne * 10 + DateTime.Minute % 10, DateTime.Second));
            void minuteDigitTwoChecked(int digitTwo) =>
                LockDateTimeUpdate(() => DateTime = new DateTime(DateTime.Year, DateTime.Month, DateTime.Day, DateTime.Hour, (DateTime.Minute / 10) * 10 + digitTwo, DateTime.Second));

            void secondDigitOneChecked(int digitOne) =>
                LockDateTimeUpdate(() => DateTime = new DateTime(DateTime.Year, DateTime.Month, DateTime.Day, DateTime.Hour, DateTime.Minute, digitOne * 10 + DateTime.Second % 10));
            void secondDigitTwoChecked(int digitTwo) =>
                LockDateTimeUpdate(() => DateTime = new DateTime(DateTime.Year, DateTime.Month, DateTime.Day, DateTime.Hour, DateTime.Minute, (DateTime.Second / 10) * 10 + digitTwo));


            for (var i = 0; i < 2; i++)
            {
                for (var j = 0; j < 12; j++)
                {
                    var hour = i * 12 + j;

                    var radioButton = new RadioButton { Content = hour, GroupName = HourGroupName, ToolTip = "Select hour" };

                    Grid.SetColumn(radioButton, i);
                    Grid.SetRow(radioButton, j);

                    radioButton.Checked += (sender, e) => hourChecked(hour);

                    _timeView.Children.Add(radioButton);
                }
            }

            for (var i = 0; i < 6; i++)
            {
                var minuteRadioButton = new RadioButton { Content = i, GroupName = MinuteDigitOneGroupName, ToolTip = "Select minute" };
                var secondRadioButton = new RadioButton { Content = i, GroupName = SecondDigitOneGroupName, ToolTip = "Select second" };

                Grid.SetColumn(minuteRadioButton, 0);
                Grid.SetColumn(secondRadioButton, 2);

                Grid.SetRow(minuteRadioButton, i);
                Grid.SetRow(secondRadioButton, i);

                var digit = i;

                minuteRadioButton.Checked += (sender, e) => minuteDigitOneChecked(digit);
                secondRadioButton.Checked += (sender, e) => secondDigitOneChecked(digit);

                minuteRadioButton.RenderTransform = new TranslateTransform();
                secondRadioButton.RenderTransform = new TranslateTransform();

                _minuteSecondView.Children.Add(minuteRadioButton);
                _minuteSecondView.Children.Add(secondRadioButton);
            }

            for (var i = 0; i < 10; i++)
            {
                var minuteRadioButton = new RadioButton { Content = i, GroupName = MinuteDigitTwoGroupName, ToolTip = "Select minute" };
                var secondRadioButton = new RadioButton { Content = i, GroupName = SecondDigitTwoGroupName, ToolTip = "Select second" };

                Grid.SetColumn(minuteRadioButton, 1);
                Grid.SetColumn(secondRadioButton, 3);

                Grid.SetRow(minuteRadioButton, i);
                Grid.SetRow(secondRadioButton, i);

                var digit = i;

                minuteRadioButton.Checked += (sender, e) => minuteDigitTwoChecked(digit);
                secondRadioButton.Checked += (sender, e) => secondDigitTwoChecked(digit);

                minuteRadioButton.RenderTransform = new TranslateTransform();
                secondRadioButton.RenderTransform = new TranslateTransform();

                _minuteSecondView.Children.Add(minuteRadioButton);
                _minuteSecondView.Children.Add(secondRadioButton);
            }

            UpdateTimeView();
        }

        private void FillMonthView()
        {
            _monthView.Children.Clear();

            var firstDayOfMonth = new DateTime(_dateInView.Year, _dateInView.Month, 1);

            var firstDayOffset = (int)firstDayOfMonth.DayOfWeek - 1;
            if (firstDayOffset < 1) firstDayOffset += 7; /* 6 for sunday and 7 from monday (first week is full in prevoius month). */

            var format = CultureInfo.InvariantCulture.DateTimeFormat;

            for (var i = 0; i < 6; i++)
            {
                for (var j = 0; j < 7; j++)
                {
                    var day = firstDayOfMonth.AddDays(i * 7 + j - firstDayOffset);

                    var radioButton = new RadioButton { Content = day.Day, GroupName = DayGroupName, IsChecked = day == DateTime.Date, ToolTip = "Select day" };

                    Grid.SetColumn(radioButton, j);
                    Grid.SetRow(radioButton, i + 1);

                    if (day.Month != firstDayOfMonth.Month)
                    {
                        SetIsSecondary(radioButton, true);
                    }

                    _monthView.Children.Add(radioButton);

                    radioButton.Checked += (sender, e) => LockDateViewUpdate(() => DateTime = new DateTime(day.Year, day.Month, day.Day, DateTime.Hour, DateTime.Minute, DateTime.Second));
                }
            }

            for (var i = 0; i < 7; i++)
            {
                var dayOfWeek = i == 6 ? DayOfWeek.Sunday : (DayOfWeek)(i + 1);

                var label = new Label { Content = format.GetAbbreviatedDayName(dayOfWeek) };

                Grid.SetColumn(label, i);

                _monthView.Children.Add(label);
            }

            DateViewDescription = firstDayOfMonth.ToString(format.YearMonthPattern, format);
        }

        private void FillYearViewWithMonth()
        {
            _yearView.Children.Clear();

            var firstMonthOfYear = new DateTime(_dateInView.Year, 1, 1);

            const int offset = 2; /* Months of prevoius year. */

            var format = CultureInfo.InvariantCulture.DateTimeFormat;

            for (var i = 0; i < 4; i++)
            {
                for (var j = 0; j < 4; j++)
                {
                    var month = firstMonthOfYear.AddMonths(i * 4 + j - offset);

                    var button = new Button { Content = format.GetAbbreviatedMonthName(month.Month), ToolTip = "Select month" };

                    button.Click += (sender, e) =>
                    {
                        _dateInView = month;

                        DateViewMode = DateViewMode.Month;

                        FillDateView();
                    };

                    Grid.SetColumn(button, j);
                    Grid.SetRow(button, i);

                    if (month.Year != firstMonthOfYear.Year)
                    {
                        SetIsSecondary(button, true);
                    }

                    _yearView.Children.Add(button);
                }
            }

            DateViewDescription = firstMonthOfYear.Year.ToString();
        }

        private void FillYearViewWithYears()
        {
            _yearView.Children.Clear();

            var firstYearOfDecade = new DateTime((_dateInView.Year / 10) * 10, 1, 1);

            var offset = (firstYearOfDecade.Year % 4 == 0) ? 4 : 2; /* 4 prevoius years if fist year divides for 4. */

            for (var i = 0; i < 4; i++)
            {
                for (var j = 0; j < 4; j++)
                {
                    var year = firstYearOfDecade.AddYears(i * 4 + j - offset);

                    var button = new Button { Content = year.Year, ToolTip = "Select year" };

                    button.Click += (sender, e) =>
                    {
                        _dateInView = year;

                        DateViewMode = DateViewMode.Year;

                        FillDateView();
                    };

                    Grid.SetColumn(button, j);
                    Grid.SetRow(button, i);

                    if (year.Year / 10 != firstYearOfDecade.Year / 10)
                    {
                        SetIsSecondary(button, true);
                    }

                    _yearView.Children.Add(button);
                }
            }

            DateViewDescription = $"{firstYearOfDecade.Year}-{firstYearOfDecade.AddYears(9).Year}";
        }

        private void FillDateView()
        {
            if (DateViewMode == DateViewMode.Month)
            {
                FillMonthView();

                _yearView.Visibility = Visibility.Collapsed;
                _monthView.Visibility = Visibility.Visible;
            }
            else if (DateViewMode == DateViewMode.Year)
            {
                FillYearViewWithMonth();

                _monthView.Visibility = Visibility.Collapsed;
                _yearView.Visibility = Visibility.Visible;
            }
            else if (DateViewMode == DateViewMode.Decade)
            {
                FillYearViewWithYears();

                _monthView.Visibility = Visibility.Collapsed;
                _yearView.Visibility = Visibility.Visible;
            }
        }

        private void Previous()
        {
            if (DateViewMode == DateViewMode.Month)
            {
                _dateInView = _dateInView.AddMonths(-1);
            }
            else if (DateViewMode == DateViewMode.Year)
            {
                _dateInView = _dateInView.AddYears(-1);
            }
            else if (DateViewMode == DateViewMode.Decade)
            {
                _dateInView = _dateInView.AddYears(-10);
            }

            FillDateView();
        }

        private void Up()
        {
            if (DateViewMode == DateViewMode.Decade) return;

            DateViewMode = (DateViewMode)((int)DateViewMode + 1);

            FillDateView();
        }

        private void Next()
        {
            if (DateViewMode == DateViewMode.Month)
            {
                _dateInView = _dateInView.AddMonths(1);
            }
            else if (DateViewMode == DateViewMode.Year)
            {
                _dateInView = _dateInView.AddYears(1);
            }
            else if (DateViewMode == DateViewMode.Decade)
            {
                _dateInView = _dateInView.AddYears(10);
            }

            FillDateView();
        }

        private void FillCommandDescriptions()
        {
            if (DateViewMode == DateViewMode.Month)
            {
                UpDescription = "Switch to year";
                PreviousDescription = "Previous month";
                NextDescription = "Next month";
            }
            else if (DateViewMode == DateViewMode.Year)
            {
                UpDescription = "Switch to decade";
                PreviousDescription = "Previous year";
                NextDescription = "Next year";
            }
            else if (DateViewMode == DateViewMode.Decade)
            {
                PreviousDescription = "Previous decade";
                NextDescription = "Next decade";
            }
        }

        private void ComputeTranslations()
        {
            var timeOffsetUnit = _timeSmallButtonSide + _dateTimeButtonMarginSide * 2;

            var minuteSecondRadioButtons = _minuteSecondView.Children.OfType<RadioButton>();

            var minuteDigitOne = (int)minuteSecondRadioButtons.First(rb => rb.GroupName == MinuteDigitOneGroupName && rb.IsChecked!.Value).Content;
            var minuteDigitTwo = (int)minuteSecondRadioButtons.First(rb => rb.GroupName == MinuteDigitTwoGroupName && rb.IsChecked!.Value).Content;
            var secondDigitOne = (int)minuteSecondRadioButtons.First(rb => rb.GroupName == SecondDigitOneGroupName && rb.IsChecked!.Value).Content;
            var secondDigitTwo = (int)minuteSecondRadioButtons.First(rb => rb.GroupName == SecondDigitTwoGroupName && rb.IsChecked!.Value).Content;

            var idealMinuteOneOffset = 7 - minuteDigitOne;
            var idealMinuteTwoOffset = 7 - minuteDigitTwo;

            var idealSecondOneOffset = 7 - secondDigitOne;
            var idealSecondTwoOffset = 7 - secondDigitTwo;


            var minuteDiff = (9 - minuteDigitTwo) - 7;

            if (minuteDiff > 0)
            {
                idealMinuteOneOffset -= minuteDiff;
                idealMinuteTwoOffset -= minuteDiff;
            }
            else
            {
                minuteDiff = minuteDigitTwo - 7;

                if (minuteDiff > 0)
                {
                    idealMinuteOneOffset += minuteDiff;
                    idealMinuteTwoOffset += minuteDiff;
                }
            }
            

            var secondDiff = (9 - secondDigitTwo) - 7;

            if (secondDiff > 0)
            {
                idealSecondOneOffset -= secondDiff;
                idealSecondTwoOffset -= secondDiff;
            }
            else
            {
                secondDiff = secondDigitTwo - 7;

                if (secondDiff > 0)
                {
                    idealSecondOneOffset += secondDiff;
                    idealSecondTwoOffset += secondDiff;
                }
            }


            var realMinuteOneOffset = idealMinuteOneOffset * timeOffsetUnit;
            var realMinuteTwoOffset = idealMinuteTwoOffset * timeOffsetUnit;

            var realSecondOneOffset = idealSecondOneOffset * timeOffsetUnit;
            var realSecondTwoOffset = idealSecondTwoOffset * timeOffsetUnit;

            var timeSpan = TimeSpan.FromMilliseconds(100);

            minuteSecondRadioButtons.Where(rb => rb.GroupName == MinuteDigitOneGroupName).
                ForEach(rb => ((TranslateTransform)rb.RenderTransform).BeginAnimation(TranslateTransform.YProperty, new DoubleAnimation(realMinuteOneOffset, timeSpan)));
            minuteSecondRadioButtons.Where(rb => rb.GroupName == MinuteDigitTwoGroupName).
                ForEach(rb => ((TranslateTransform)rb.RenderTransform).BeginAnimation(TranslateTransform.YProperty, new DoubleAnimation(realMinuteTwoOffset, timeSpan)));

            minuteSecondRadioButtons.Where(rb => rb.GroupName == SecondDigitOneGroupName).
                ForEach(rb => ((TranslateTransform)rb.RenderTransform).BeginAnimation(TranslateTransform.YProperty, new DoubleAnimation(realSecondOneOffset, timeSpan)));
            minuteSecondRadioButtons.Where(rb => rb.GroupName == SecondDigitTwoGroupName).
                ForEach(rb => ((TranslateTransform)rb.RenderTransform).BeginAnimation(TranslateTransform.YProperty, new DoubleAnimation(realSecondTwoOffset, timeSpan)));
        }
    }


    /// <summary>Current mode of date view.</summary>
    public enum DateViewMode
    {
        /// <summary>Month view mode.</summary>
        Month,
        /// <summary>Year view mode.</summary>
        Year,
        /// <summary>Decade view mode.</summary>
        Decade
    }
}