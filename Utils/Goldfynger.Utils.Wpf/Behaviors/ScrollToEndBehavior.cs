﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

using Microsoft.Xaml.Behaviors;

namespace Goldfynger.Utils.Wpf.Behaviors
{
    /// <summary>Adds autoscroll behavior for text box.</summary>
    public sealed class ScrollToEndBehavior : Behavior<TextBoxBase>
    {
        /// <summary>Is autoscroll enabled <see cref="DependencyProperty"/>.</summary>
        public static readonly DependencyProperty IsEnabledProperty =
            DependencyProperty.Register(nameof(IsEnabled), typeof(bool), typeof(ScrollToEndBehavior),
                new PropertyMetadata { DefaultValue = true });


        private void AssociatedObject_TextChanged(object? sender, TextChangedEventArgs e)
        {
            if (IsEnabled)
            {
                AssociatedObject.ScrollToEnd();
            }
        }


        /// <summary>Is autoscroll enabled.</summary>
        public bool IsEnabled
        {
            get => (bool)GetValue(IsEnabledProperty);
            set => SetValue(IsEnabledProperty, value);
        }


        /// <summary>Called after the behavior is attached to an AssociatedObject.</summary>
        protected override void OnAttached()
        {
            base.OnAttached();

            WeakEventManager<TextBoxBase, TextChangedEventArgs>.AddHandler(AssociatedObject, nameof(TextBoxBase.TextChanged), AssociatedObject_TextChanged);
        }

        /// <summary>Called when the behavior is being detached from its AssociatedObject, but before it has actually occurred.</summary>
        protected override void OnDetaching()
        {
            base.OnDetaching();

            WeakEventManager<TextBoxBase, TextChangedEventArgs>.RemoveHandler(AssociatedObject, nameof(TextBoxBase.TextChanged), AssociatedObject_TextChanged);
        }
    }
}
