﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;

namespace Goldfynger.Utils.Windows.Console
{
    /// <summary></summary>
    public sealed class ColoredConsoleTraceListener : ConsoleTraceListener
    {
        private static readonly string[] NewLineStringArray = new string[] { Environment.NewLine };

        private static readonly string[] FuncAndPathSplitArray = new string[] { " in " };

        private static readonly Dictionary<TraceEventType, ConsoleColor> _eventTypeColor = new()
        {
            [TraceEventType.Critical] = ConsoleColor.Red,
            [TraceEventType.Error] = ConsoleColor.DarkRed,
            [TraceEventType.Warning] = ConsoleColor.Yellow,
            [TraceEventType.Information] = ConsoleColor.Gray,
            [TraceEventType.Verbose] = ConsoleColor.DarkGray,

            [TraceEventType.Start] = ConsoleColor.DarkCyan,
            [TraceEventType.Stop] = ConsoleColor.DarkCyan,
            [TraceEventType.Suspend] = ConsoleColor.White,
            [TraceEventType.Resume] = ConsoleColor.White,

            [TraceEventType.Transfer] = ConsoleColor.Magenta
        };

        private readonly bool _showEventType;

        private readonly bool _showSource;


        /// <summary></summary>
        /// <param name="showSource"></param>
        /// <param name="showEventType"></param>
        public ColoredConsoleTraceListener(bool showSource = true, bool showEventType = true)
        {            
            _showSource = showSource;
            _showEventType = showEventType;
        }

        /// <summary></summary>
        /// <param name="args"></param>
        public ColoredConsoleTraceListener(string args)
        {
            _showSource = true;
            _showEventType = true;

            if (args != null)
            {
                var splitResult = args.Replace(" ", "").Split(','); /* Split and parse for first and second boolean. */

                if (splitResult.Length >= 1) /* _showSource  */
                {
                    if (bool.TryParse(splitResult[0], out bool result))
                    {
                        _showSource = result;
                    }
                }

                if (splitResult.Length >= 2) /* _showEventType */
                {
                    if (bool.TryParse(splitResult[1], out bool result))
                    {
                        _showEventType = result;
                    }
                }                
            }
        }


        /// <summary></summary>
        /// <param name="eventCache"></param>
        /// <param name="source"></param>
        /// <param name="eventType"></param>
        /// <param name="id"></param>
        /// <param name="message"></param>
        public override void TraceEvent(TraceEventCache? eventCache, string source, TraceEventType eventType, int id, string? message)
        {
            if (IsFiltered(eventCache, source, eventType, id, message, null, null, null)) return;

            Trace(eventCache, source, eventType, id, message);
        }

        /// <summary></summary>
        /// <param name="eventCache"></param>
        /// <param name="source"></param>
        /// <param name="eventType"></param>
        /// <param name="id"></param>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public override void TraceEvent(TraceEventCache? eventCache, string source, TraceEventType eventType, int id, string format, params object?[]? args)
        {
            if (IsFiltered(eventCache, source, eventType, id, format, args, null, null)) return;

            var message = args == null ? format : string.Format(CultureInfo.InvariantCulture, format, args);

            Trace(eventCache, source, eventType, id, message);
        }

        /// <summary></summary>
        /// <param name="eventCache"></param>
        /// <param name="source"></param>
        /// <param name="eventType"></param>
        /// <param name="id"></param>
        /// <param name="data"></param>
        public override void TraceData(TraceEventCache? eventCache, string source, TraceEventType eventType, int id, object? data)
        {
            if (IsFiltered(eventCache, source, eventType, id, null, null, data, null)) return;

            var message = data == null ? string.Empty : data.ToString();

            Trace(eventCache, source, eventType, id, message);
        }

        /// <summary></summary>
        /// <param name="eventCache"></param>
        /// <param name="source"></param>
        /// <param name="eventType"></param>
        /// <param name="id"></param>
        /// <param name="data"></param>
        public override void TraceData(TraceEventCache? eventCache, string source, TraceEventType eventType, int id, params object?[]? data)
        {
            if (IsFiltered(eventCache, source, eventType, id, null, null, null, data)) return;

            var message = new StringBuilder();
            if (data != null)
            {
                for (var i = 0; i < data.Length; i++)
                {
                    if (i != 0)
                    {
                        message.Append(", ");
                    }

                    var d = data[i];

                    if (d != null)
                    {
                        message.Append(d.ToString());
                    }
                }
            }

            Trace(eventCache, source, eventType, id, message.ToString());
        }
                
        private bool IsFiltered(TraceEventCache? cache, string source, TraceEventType eventType, int id, string? formatOrMessage, object?[]? args, object? data1, object?[]? data)
        {
            return Filter != null && !Filter.ShouldTrace(cache, source, eventType, id, formatOrMessage, args, data1, data);
        }

        private void Trace(TraceEventCache? cache, string source, TraceEventType eventType, int id, string? message)
        {
            var originalColor = System.Console.ForegroundColor;
            System.Console.ForegroundColor = _eventTypeColor[eventType];

            if (_showSource) Write($"{source}: ");
            if (_showEventType) Write($"{eventType}: ");
            Write($"{id}: ");

            WriteLine(message);

            TraceFooter(cache);

            System.Console.ForegroundColor = originalColor;
        }

        private void TraceFooter(TraceEventCache? eventCache)
        {
            if (eventCache == null)
            {
                return;
            }

            IndentLevel++;

            if (IsEnabled(TraceOptions.ProcessId)) WriteLine("ProcessId: " + eventCache.ProcessId);

            if (IsEnabled(TraceOptions.LogicalOperationStack))
            {
                Write("LogicalOperationStack: ");

                Stack operationStack = eventCache.LogicalOperationStack;

                bool first = true;

                foreach (object obj in operationStack)
                {
                    if (!first)
                    {
                        Write(", ");
                    }
                    else
                    {
                        first = false;
                    }

                    Write(obj.ToString());
                }
                WriteLine(string.Empty);
            }

            if (IsEnabled(TraceOptions.ThreadId)) WriteLine("ThreadId: " + eventCache.ThreadId);

            if (IsEnabled(TraceOptions.DateTime)) WriteLine("DateTime: " + eventCache.DateTime.ToString("o", CultureInfo.InvariantCulture));

            if (IsEnabled(TraceOptions.Timestamp)) WriteLine("Timestamp: " + eventCache.Timestamp);

            if (IsEnabled(TraceOptions.Callstack))
            {
                var callstackArray = eventCache.Callstack.Split(NewLineStringArray, StringSplitOptions.None);

                WriteLine("Callstack:");
                foreach (var str in callstackArray)
                {
                    var funcAndPathArray = str.Split(FuncAndPathSplitArray, StringSplitOptions.None);
                    
                    WriteLine(funcAndPathArray[0]); /* Function. */

                    if (funcAndPathArray.Length > 1)
                    {
                        IndentLevel++;

                        WriteLine($"   in {funcAndPathArray[1]}"); /* Path. */

                        IndentLevel--;
                    }
                }
            }

            IndentLevel--;
        }

        private bool IsEnabled(TraceOptions options) => (options & TraceOutputOptions) != 0;
    }
}
