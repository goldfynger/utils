﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

using Microsoft.Win32.SafeHandles;

namespace Goldfynger.Utils.Windows.Console
{
    /// <summary>Provides additional console functional.</summary>
    public static class ConsoleHelper
    {
        /// <summary>
        /// Instantiate new console window.
        /// </summary>
        /// <param name="forceRedirect">Should be <see langword="true"/> for redirect output if application run in Visual Studio debugger.</param>
        public static void Instantiate(bool forceRedirect = false)
        {
            AllocConsole();


            if (forceRedirect)
            {
                try
                {
                    var hStdHandle =
                        CreateFile("CONOUT$", CfDesiredAccess.GENERIC_WRITE, CfShareMode.FILE_SHARE_WRITE, IntPtr.Zero, CfCreationDisposition.OPEN_EXISTING, CfFlagsAndAttributes.FILE_ATTRIBUTE_FLAG_ZERO, IntPtr.Zero);

                    if (hStdHandle.IsInvalid)
                    {
                        throw new Win32Exception(Marshal.GetLastWin32Error());
                    }

                    var fileStream = new FileStream(hStdHandle, FileAccess.Write);

                    var standardOutput = new StreamWriter(fileStream, Encoding.Default)
                    {
                        AutoFlush = true
                    };

                    System.Console.SetOut(standardOutput);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Failed to redirect standard output.", ex);
                }
            }
        }


        /// <summary>
        /// Used in <see cref="CreateFile"/> function. Specifies type of requested access to the device.
        /// </summary>
        [Flags]
        internal enum CfDesiredAccess : uint
        {
            /// <summary><see cref="CreateFile"/>: Open device for write.</summary>
            GENERIC_WRITE = 0x40000000,
        }

        /// <summary>
        /// Used in <see cref="CreateFile"/> function. Specifies type of requested sharing mode to the device.
        /// </summary>
        [Flags]
        internal enum CfShareMode : uint
        {
            /// <summary><see cref="CreateFile"/>: Enables subsequent open operations on a device to request write access.</summary>
            FILE_SHARE_WRITE = 0x00000002,
        }

        /// <summary>
        /// Used in <see cref="CreateFile"/> function. Specifies an action to take on a device that exists or does not exist.
        /// </summary>
        [Flags]
        internal enum CfCreationDisposition : uint
        {
            /// <summary><see cref="CreateFile"/>: Opens a device, only if it exists.</summary>
            OPEN_EXISTING = 0x00000003,
        }

        /// <summary>
        /// Used in <see cref="CreateFile"/> function. Specifies the device attributes and flags.
        /// </summary>
        [Flags]
        internal enum CfFlagsAndAttributes : uint
        {
            /// <summary><see cref="CreateFile"/>: No attributes and no flags.</summary>
            FILE_ATTRIBUTE_FLAG_ZERO = 0,
        }


        /// <summary>Allocates a new console for the calling process.</summary>
        /// <returns>True if successful.</returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool AllocConsole();

        /// <summary>
        /// Creates/opens a file, serial port, USB device... etc.
        /// </summary>
        /// <param name="lpFileName">Path to object to open.</param>
        /// <param name="dwDesiredAccess">Access mode. e.g. Read, write.</param>
        /// <param name="dwShareMode">Sharing mode.</param>
        /// <param name="lpSecurityAttributes">Security details (can be null).</param>
        /// <param name="dwCreationDisposition">Specifies if the file is created or opened.</param>
        /// <param name="dwFlagsAndAttributes">Any extra attributes? e.g. open overlapped.</param>
        /// <param name="hTemplateFile">Not used.</param>
        /// <returns>If the function succeeds, the return value is an open handle to the specified file, device, named pipe, or mail slot.</returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern SafeFileHandle CreateFile(
            [MarshalAs(UnmanagedType.LPWStr)] string lpFileName,
            [MarshalAs(UnmanagedType.U4)] CfDesiredAccess dwDesiredAccess,
            [MarshalAs(UnmanagedType.U4)] CfShareMode dwShareMode,
            IntPtr lpSecurityAttributes,
            [MarshalAs(UnmanagedType.U4)] CfCreationDisposition dwCreationDisposition,
            [MarshalAs(UnmanagedType.U4)] CfFlagsAndAttributes dwFlagsAndAttributes,
            IntPtr hTemplateFile);
    }
}