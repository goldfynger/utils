﻿using System.Collections.ObjectModel;
using System.Windows;

using Goldfynger.Utils.Wpf.Converters;

namespace Goldfynger.Utils.Wpf.Run
{
    public class ConvertersTabViewModel : DependencyObject
    {
        public static readonly DependencyProperty StateAProperty =
            DependencyProperty.Register(nameof(StateA), typeof(bool?), typeof(ConvertersTabViewModel));

        public static readonly DependencyProperty StateBProperty =
            DependencyProperty.Register(nameof(StateB), typeof(bool?), typeof(ConvertersTabViewModel));

        private static readonly DependencyPropertyKey StringCollectionPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(StringCollection), typeof(ReadOnlyObservableCollection<string>), typeof(ConvertersTabViewModel),
                new PropertyMetadata(defaultValue: new ReadOnlyObservableCollection<string>(new ObservableCollection<string>())));
        public static readonly DependencyProperty StringCollectionProperty = StringCollectionPropertyKey.DependencyProperty;

        public static readonly DependencyProperty StringAProperty =
            DependencyProperty.Register(nameof(StringA), typeof(string), typeof(ConvertersTabViewModel));

        public static readonly DependencyProperty StringBProperty =
            DependencyProperty.Register(nameof(StringB), typeof(string), typeof(ConvertersTabViewModel));

        private static readonly DependencyPropertyKey EnumValuesCollectionPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(EnumValuesCollection), typeof(ReadOnlyObservableCollection<EnumValues?>), typeof(ConvertersTabViewModel),
                new PropertyMetadata(defaultValue: new ReadOnlyObservableCollection<EnumValues?>(new ObservableCollection<EnumValues?>())));
        public static readonly DependencyProperty EnumValuesCollectionProperty = EnumValuesCollectionPropertyKey.DependencyProperty;

        public static readonly DependencyProperty EnumValueProperty =
            DependencyProperty.Register(nameof(EnumValue), typeof(EnumValues?), typeof(ConvertersTabViewModel));

        public const string ValueA = "Value A";
        public const string ValueB = "Value B";

        public static readonly IsValueEqualsConverter IsValueEqualsAConverter = new(ValueA);
        public static readonly IsValueEqualsConverter IsValueEqualsBConverter = new(ValueB);

        public static readonly IsValuesEqualsMultiConverter IsValuesEqualsAMultiConverter = new(ValueA);

        public static readonly IsValueEqualsToComparableConverter<string> IsValueEqualsToComparableAConverter = new(ValueA);
        public static readonly IsValueEqualsToComparableConverter<string> IsValueEqualsToComparableBConverter = new(ValueB);

        public static readonly IsValuesEqualsToComparableMultiConverter<string> IsValuesEqualsToComparableAMultiConverter = new(ValueA);

        public static readonly IsValueEqualsToEquatableConverter<string> IsValueEqualsToEquatableAConverter = new(ValueA);
        public static readonly IsValueEqualsToEquatableConverter<string> IsValueEqualsToEquatableBConverter = new(ValueB);

        public static readonly IsValuesEqualsToEquatableMultiConverter<string> IsValuesEqualsToEquatableAMultiConverter = new(ValueA);

        public static readonly EnumToStringConverter<EnumValues> EnumValuesToStringConverter = new();


        public ConvertersTabViewModel()
        {
            StringCollection = new ReadOnlyObservableCollection<string>(new ObservableCollection<string>(new string[] { ValueA, ValueB }));
            EnumValuesCollection = new ReadOnlyObservableCollection<EnumValues?>(new ObservableCollection<EnumValues?>(new EnumValues?[] { EnumValues.EnumA, EnumValues.EnumB }));
            EnumValue = EnumValues.EnumA;
        }


        public bool? StateA
        {
            get => (bool)GetValue(StateAProperty);
            set => SetValue(StateAProperty, value);
        }

        public bool? StateB
        {
            get => (bool)GetValue(StateBProperty);
            set => SetValue(StateBProperty, value);
        }

        public ReadOnlyObservableCollection<string> StringCollection
        {
            get => (ReadOnlyObservableCollection<string>)GetValue(StringCollectionProperty);
            private set => SetValue(StringCollectionPropertyKey, value);
        }

        public string? StringA
        {
            get => (string?)GetValue(StringAProperty);
            set => SetValue(StringAProperty, value);
        }

        public string? StringB
        {
            get => (string?)GetValue(StringBProperty);
            set => SetValue(StringBProperty, value);
        }

        public ReadOnlyObservableCollection<EnumValues?> EnumValuesCollection
        {
            get => (ReadOnlyObservableCollection<EnumValues?>)GetValue(EnumValuesCollectionProperty);
            private set => SetValue(EnumValuesCollectionPropertyKey, value);
        }

        public EnumValues? EnumValue
        {
            get => (EnumValues?)GetValue(EnumValueProperty);
            set => SetValue(EnumValueProperty, value);
        }


        public enum EnumValues
        {
            EnumA,
            EnumB,
        }
    }
}
