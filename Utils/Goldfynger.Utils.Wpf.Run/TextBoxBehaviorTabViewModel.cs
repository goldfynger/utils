﻿using System;
using System.Windows;

using Goldfynger.Utils.Wpf.Commands;

namespace Goldfynger.Utils.Wpf.Run
{
    public class TextBoxBehaviorTabViewModel : DependencyObject
    {
        private static readonly DependencyPropertyKey AddToTextBoxCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(AddToTextBoxCommand), typeof(DependencyCommand), typeof(TextBoxBehaviorTabViewModel), new PropertyMetadata());
        public static readonly DependencyProperty AddToTextBoxCommandProperty = AddToTextBoxCommandPropertyKey.DependencyProperty;

        public static readonly DependencyProperty TextOfTextBoxProperty =
            DependencyProperty.Register(nameof(TextOfTextBox), typeof(string), typeof(TextBoxBehaviorTabViewModel));


        public TextBoxBehaviorTabViewModel()
        {
            InitializeCommands();
        }


        public DependencyCommand AddToTextBoxCommand
        {
            get => (DependencyCommand)GetValue(AddToTextBoxCommandProperty);
            private set => SetValue(AddToTextBoxCommandPropertyKey, value);
        }

        public string TextOfTextBox
        {
            get => (string)GetValue(TextOfTextBoxProperty);
            set => SetValue(TextOfTextBoxProperty, value);
        }


        private void InitializeCommands()
        {
            AddToTextBoxCommand = new DependencyCommand(() =>
            {
                for (var idx = 1; idx <= 10; idx++)
                {
                    TextOfTextBox += $"{idx}{Environment.NewLine}";
                }
            });
        }
    }
}
