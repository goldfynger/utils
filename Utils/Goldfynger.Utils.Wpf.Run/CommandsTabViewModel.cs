﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

using Goldfynger.Utils.Wpf.Commands;

namespace Goldfynger.Utils.Wpf.Run
{
    public class CommandsTabViewModel : DependencyObject
    {
        private static readonly DependencyPropertyKey SimpleCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(SimpleCommand), typeof(DependencyCommand), typeof(CommandsTabViewModel), new PropertyMetadata());
        public static readonly DependencyProperty SimpleCommandProperty = SimpleCommandPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey ParametrizedCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(ParametrizedCommand), typeof(DependencyCommand), typeof(CommandsTabViewModel), new PropertyMetadata());
        public static readonly DependencyProperty ParametrizedCommandProperty = ParametrizedCommandPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey GenericCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(GenericCommand), typeof(DependencyCommand<string>), typeof(CommandsTabViewModel), new PropertyMetadata());
        public static readonly DependencyProperty GenericCommandProperty = GenericCommandPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey SimpleAsyncCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(SimpleAsyncCommand), typeof(AsyncDependencyCommand), typeof(CommandsTabViewModel), new PropertyMetadata());
        public static readonly DependencyProperty SimpleAsyncCommandProperty = SimpleAsyncCommandPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey ParametrizedAsyncCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(ParametrizedAsyncCommand), typeof(AsyncDependencyCommand), typeof(CommandsTabViewModel), new PropertyMetadata());
        public static readonly DependencyProperty ParametrizedAsyncCommandProperty = ParametrizedAsyncCommandPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey GenericAsyncCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(GenericAsyncCommand), typeof(AsyncDependencyCommand<string>), typeof(CommandsTabViewModel), new PropertyMetadata());
        public static readonly DependencyProperty GenericAsyncCommandProperty = GenericAsyncCommandPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey TextChangedCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(TextChangedCommand), typeof(DependencyCommand<string>), typeof(CommandsTabViewModel), new PropertyMetadata());
        public static readonly DependencyProperty TextChangedCommandProperty = TextChangedCommandPropertyKey.DependencyProperty;


        public CommandsTabViewModel()
        {
            InitializeCommands();
        }


        public DependencyCommand SimpleCommand
        {
            get => (DependencyCommand)GetValue(SimpleCommandProperty);
            private set => SetValue(SimpleCommandPropertyKey, value);
        }

        public DependencyCommand ParametrizedCommand
        {
            get => (DependencyCommand)GetValue(ParametrizedCommandProperty);
            private set => SetValue(ParametrizedCommandPropertyKey, value);
        }

        public DependencyCommand<string> GenericCommand
        {
            get => (DependencyCommand<string>)GetValue(GenericCommandProperty);
            private set => SetValue(GenericCommandPropertyKey, value);
        }

        public AsyncDependencyCommand SimpleAsyncCommand
        {
            get => (AsyncDependencyCommand)GetValue(SimpleAsyncCommandProperty);
            private set => SetValue(SimpleAsyncCommandPropertyKey, value);
        }

        public AsyncDependencyCommand ParametrizedAsyncCommand
        {
            get => (AsyncDependencyCommand)GetValue(ParametrizedAsyncCommandProperty);
            private set => SetValue(ParametrizedAsyncCommandPropertyKey, value);
        }

        public AsyncDependencyCommand<string> GenericAsyncCommand
        {
            get => (AsyncDependencyCommand<string>)GetValue(GenericAsyncCommandProperty);
            private set => SetValue(GenericAsyncCommandPropertyKey, value);
        }

        public DependencyCommand<string> TextChangedCommand
        {
            get => (DependencyCommand<string>)GetValue(TextChangedCommandProperty);
            private set => SetValue(TextChangedCommandPropertyKey, value);
        }


        private void InitializeCommands()
        {
            SimpleCommand = new DependencyCommand(() => MessageBox.Show($"{nameof(SimpleCommand)} action.", nameof(SimpleCommand), MessageBoxButton.OK, MessageBoxImage.Information));
            SimpleCommand.Executing += (sender, e) =>
            {
                var result = MessageBox.Show($"{nameof(SimpleCommand)} {nameof(DependencyCommand.Executing)}: execute?", nameof(SimpleCommand), MessageBoxButton.OKCancel, MessageBoxImage.Question);

                if (result == MessageBoxResult.Cancel)
                {
                    e.IsCanceled = true;
                }
            };
            SimpleCommand.Executed += (sender, e) => MessageBox.Show($"{nameof(SimpleCommand)} {nameof(DependencyCommand.Executed)}.", nameof(SimpleCommand), MessageBoxButton.OK, MessageBoxImage.Information);

            ParametrizedCommand = new DependencyCommand((o) => MessageBox.Show($"{nameof(ParametrizedCommand)} action \"{o}\".", nameof(ParametrizedCommand), MessageBoxButton.OK, MessageBoxImage.Information));
            ParametrizedCommand.Executing += (sender, e) =>
            {
                var result = MessageBox.Show($"{nameof(ParametrizedCommand)} {nameof(DependencyCommand.Executing)}: execute?", nameof(ParametrizedCommand), MessageBoxButton.OKCancel, MessageBoxImage.Question);

                if (result == MessageBoxResult.Cancel)
                {
                    e.IsCanceled = true;
                }
            };
            ParametrizedCommand.Executed += (sender, e) =>
                MessageBox.Show($"{nameof(ParametrizedCommand)} {nameof(DependencyCommand.Executed)} \"{e.Parameter}\".", nameof(ParametrizedCommand), MessageBoxButton.OK, MessageBoxImage.Information);

            GenericCommand = new DependencyCommand<string>((o) => MessageBox.Show($"{nameof(GenericCommand)} action \"{o}\".", nameof(GenericCommand), MessageBoxButton.OK, MessageBoxImage.Information));
            GenericCommand.Executing += (sender, e) =>
            {
                var result = MessageBox.Show($"{nameof(GenericCommand)} {nameof(DependencyCommand.Executing)}: execute?", nameof(GenericCommand), MessageBoxButton.OKCancel, MessageBoxImage.Question);

                if (result == MessageBoxResult.Cancel)
                {
                    e.IsCanceled = true;
                }
            };
            GenericCommand.Executed += (sender, e) =>
                MessageBox.Show($"{nameof(GenericCommand)} {nameof(DependencyCommand.Executed)} \"{e.Parameter}\".", nameof(GenericCommand), MessageBoxButton.OK, MessageBoxImage.Information);

            SimpleAsyncCommand = new AsyncDependencyCommand(() =>
            {
                return Task.Run(() =>
                {
                    Thread.Sleep(TimeSpan.FromSeconds(5));

                    Dispatcher.Invoke(() => MessageBox.Show($"{nameof(SimpleAsyncCommand)} function.", nameof(SimpleAsyncCommand), MessageBoxButton.OK, MessageBoxImage.Information));
                });
            });
            SimpleAsyncCommand.Executing += (sender, e) =>
            {
                var result = MessageBox.Show($"{nameof(SimpleAsyncCommand)} {nameof(AsyncDependencyCommand.Executing)}: execute (5 second)?",
                    nameof(SimpleAsyncCommand), MessageBoxButton.OKCancel, MessageBoxImage.Question);

                if (result == MessageBoxResult.Cancel)
                {
                    e.IsCanceled = true;
                }
            };
            SimpleAsyncCommand.Executed += (sender, e) =>
                MessageBox.Show($"{nameof(SimpleAsyncCommand)} {nameof(AsyncDependencyCommand.Executed)}.", nameof(SimpleAsyncCommand), MessageBoxButton.OK, MessageBoxImage.Information);

            ParametrizedAsyncCommand = new AsyncDependencyCommand((o) =>
            {
                return Task.Run(() =>
                {
                    Thread.Sleep(TimeSpan.FromSeconds(5));

                    Dispatcher.Invoke(() => MessageBox.Show($"{nameof(ParametrizedAsyncCommand)} function \"{o}\".", nameof(ParametrizedAsyncCommand), MessageBoxButton.OK, MessageBoxImage.Information));
                });
            });
            ParametrizedAsyncCommand.Executing += (sender, e) =>
            {
                var result = MessageBox.Show($"{nameof(ParametrizedAsyncCommand)} {nameof(AsyncDependencyCommand.Executing)}: execute (5 second)?",
                    nameof(ParametrizedAsyncCommand), MessageBoxButton.OKCancel, MessageBoxImage.Question);

                if (result == MessageBoxResult.Cancel)
                {
                    e.IsCanceled = true;
                }
            };
            ParametrizedAsyncCommand.Executed += (sender, e) =>
                MessageBox.Show($"{nameof(ParametrizedAsyncCommand)} {nameof(AsyncDependencyCommand.Executed)} \"{e.Parameter}\".", nameof(ParametrizedAsyncCommand), MessageBoxButton.OK, MessageBoxImage.Information);

            GenericAsyncCommand = new AsyncDependencyCommand<string>((o) =>
            {
                return Task.Run(() =>
                {
                    Thread.Sleep(TimeSpan.FromSeconds(5));

                    Dispatcher.Invoke(() => MessageBox.Show($"{nameof(GenericAsyncCommand)} function \"{o}\".", nameof(GenericAsyncCommand), MessageBoxButton.OK, MessageBoxImage.Information));
                });
            });
            GenericAsyncCommand.Executing += (sender, e) =>
            {
                var result = MessageBox.Show($"{nameof(GenericAsyncCommand)} {nameof(AsyncDependencyCommand.Executing)}: execute (5 second)?",
                    nameof(GenericAsyncCommand), MessageBoxButton.OKCancel, MessageBoxImage.Question);

                if (result == MessageBoxResult.Cancel)
                {
                    e.IsCanceled = true;
                }
            };
            GenericAsyncCommand.Executed += (sender, e) =>
                MessageBox.Show($"{nameof(GenericAsyncCommand)} {nameof(AsyncDependencyCommand.Executed)} \"{e.Parameter}\".", nameof(GenericAsyncCommand), MessageBoxButton.OK, MessageBoxImage.Information);

            TextChangedCommand = new DependencyCommand<string>((o) => MessageBox.Show($"\"{o}\"", nameof(TextChangedCommand), MessageBoxButton.OK, MessageBoxImage.Information));
        }
    }
}
