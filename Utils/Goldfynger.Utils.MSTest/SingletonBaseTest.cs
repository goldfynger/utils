﻿using System;
using System.Reflection;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Goldfynger.Utils.MSTest
{
    [TestClass]
    public class SingletonBaseTest
    {
        public sealed class SingletonImplementationOne : SingletonBase<SingletonImplementationOne>
        {
            public readonly int Value1 = 1;
        }

        public sealed class SingletonImplementationTwo : SingletonBase<SingletonImplementationTwo>
        {
            public readonly int Value2 = 2;
        }


        [TestMethod]
        public void SingletonTest()
        {
            Assert.AreEqual(1, SingletonImplementationOne.Instance.Value1);
            Assert.AreEqual(2, new SingletonImplementationTwo().Value2);

            Assert.ThrowsException<InvalidOperationException>(() => new SingletonImplementationOne());
            Assert.ThrowsException<TargetInvocationException>(() => SingletonImplementationTwo.Instance.Value2);
        }
    }
}
