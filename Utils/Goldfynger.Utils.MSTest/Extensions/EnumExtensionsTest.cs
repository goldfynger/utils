﻿using System;

using Goldfynger.Utils.Extensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Goldfynger.Utils.MSTest.Extensions
{
    [TestClass]
    public class EnumExtensionsTest
    {
        private enum EnumWithoutFlags : byte
        {
            None = 0,
            Item1 = 1,
            Item2 = 2,
        }        

        [TestMethod]
        public void EnumWithoutFlagsTest()
        {
            var none = EnumWithoutFlags.None;
            var item1 = EnumWithoutFlags.Item1;
            var item2 = EnumWithoutFlags.Item2;
            var item3 = (EnumWithoutFlags)3;
            var item4 = (EnumWithoutFlags)4;

            var item1_asFlag = EnumWithoutFlags.None | EnumWithoutFlags.Item1;
            var item2_asFlag = EnumWithoutFlags.None | EnumWithoutFlags.Item2;
            var item3_asFlag = EnumWithoutFlags.Item1 | EnumWithoutFlags.Item2;
            var item4_asFlag = (EnumWithoutFlags)4;

            none.ThrowIfNotDefined();
            item1.ThrowIfNotDefined();
            item2.ThrowIfNotDefined();
            Assert.ThrowsException<ArgumentException>(() => item3.ThrowIfNotDefined());
            Assert.ThrowsException<ArgumentException>(() => item4.ThrowIfNotDefined());

            item1_asFlag.ThrowIfNotDefined();
            item2_asFlag.ThrowIfNotDefined();
            Assert.ThrowsException<ArgumentException>(() => item3_asFlag.ThrowIfNotDefined());
            Assert.ThrowsException<ArgumentException>(() => item4_asFlag.ThrowIfNotDefined());

            item1_asFlag.ThrowIfUnknownFlags();
            item2_asFlag.ThrowIfUnknownFlags();
            item3_asFlag.ThrowIfUnknownFlags();
            Assert.ThrowsException<ArgumentException>(() => item4_asFlag.ThrowIfUnknownFlags());
        }

        [Flags]
        private enum EnumWithFlags : byte
        {
            NoFlags = 0,
            Flag1 = 1,
            Flag2 = 2,
            Flag8 = 8,
            AllFlags = Flag1 | Flag2 | Flag8,
        }

        [TestMethod]
        public void EnumWithFlagsTest()
        {
            var noFlags = EnumWithFlags.NoFlags;
            var flag1 = EnumWithFlags.Flag1;
            var flag2 = EnumWithFlags.Flag2;
            var flag3 = (EnumWithFlags)3;
            var flag4 = (EnumWithFlags)4;
            var flag5 = (EnumWithFlags)5;
            var flag6 = (EnumWithFlags)6;
            var flag7 = (EnumWithFlags)7;
            var flag8 = EnumWithFlags.Flag8;
            var flag9 = (EnumWithFlags)9;
            var flag10 = (EnumWithFlags)10;
            var flag11 = (EnumWithFlags)11;
            var allFlags = EnumWithFlags.AllFlags;

            noFlags.ThrowIfNotDefined();
            flag1.ThrowIfNotDefined();
            flag2.ThrowIfNotDefined();
            Assert.ThrowsException<ArgumentException>(() => flag3.ThrowIfNotDefined());
            Assert.ThrowsException<ArgumentException>(() => flag4.ThrowIfNotDefined());
            Assert.ThrowsException<ArgumentException>(() => flag5.ThrowIfNotDefined());
            Assert.ThrowsException<ArgumentException>(() => flag6.ThrowIfNotDefined());
            Assert.ThrowsException<ArgumentException>(() => flag7.ThrowIfNotDefined());
            flag8.ThrowIfNotDefined();
            Assert.ThrowsException<ArgumentException>(() => flag9.ThrowIfNotDefined());
            Assert.ThrowsException<ArgumentException>(() => flag10.ThrowIfNotDefined());
            flag11.ThrowIfNotDefined();
            allFlags.ThrowIfNotDefined();

            var noFlags_asFlag = EnumWithFlags.NoFlags;
            var flag1_asFlag = EnumWithFlags.NoFlags | EnumWithFlags.Flag1;
            var flag2_asFlag = EnumWithFlags.NoFlags | EnumWithFlags.Flag2;
            var flag3_asFlag = EnumWithFlags.Flag1 | EnumWithFlags.Flag2;
            var flag4_asFlag = (EnumWithFlags)4;
            var flag5_asFlag = (EnumWithFlags)5;
            var flag6_asFlag = (EnumWithFlags)6;
            var flag7_asFlag = (EnumWithFlags)7;
            var flag8_asFlag = EnumWithFlags.NoFlags | EnumWithFlags.Flag8;
            var flag9_asFlag = EnumWithFlags.Flag1 | EnumWithFlags.Flag8;
            var flag10_asFlag = EnumWithFlags.Flag2 | EnumWithFlags.Flag8;
            var flag11_asFlag = EnumWithFlags.Flag1 | EnumWithFlags.Flag2 | EnumWithFlags.Flag8;
            var allFlags_asFlag = EnumWithFlags.AllFlags;

            flag1_asFlag.ThrowIfNotDefined();
            flag2_asFlag.ThrowIfNotDefined();
            Assert.ThrowsException<ArgumentException>(() => flag3_asFlag.ThrowIfNotDefined());
            Assert.ThrowsException<ArgumentException>(() => flag4_asFlag.ThrowIfNotDefined());
            Assert.ThrowsException<ArgumentException>(() => flag5_asFlag.ThrowIfNotDefined());
            Assert.ThrowsException<ArgumentException>(() => flag6_asFlag.ThrowIfNotDefined());
            Assert.ThrowsException<ArgumentException>(() => flag7_asFlag.ThrowIfNotDefined());
            flag8_asFlag.ThrowIfNotDefined();
            Assert.ThrowsException<ArgumentException>(() => flag9_asFlag.ThrowIfNotDefined());
            Assert.ThrowsException<ArgumentException>(() => flag10_asFlag.ThrowIfNotDefined());
            flag11_asFlag.ThrowIfNotDefined();
            allFlags_asFlag.ThrowIfNotDefined();

            noFlags_asFlag.ThrowIfUnknownFlags();
            flag1_asFlag.ThrowIfUnknownFlags();
            flag2_asFlag.ThrowIfUnknownFlags();
            flag3_asFlag.ThrowIfUnknownFlags();
            Assert.ThrowsException<ArgumentException>(() => flag4_asFlag.ThrowIfUnknownFlags());
            Assert.ThrowsException<ArgumentException>(() => flag5_asFlag.ThrowIfUnknownFlags());
            Assert.ThrowsException<ArgumentException>(() => flag6_asFlag.ThrowIfUnknownFlags());
            Assert.ThrowsException<ArgumentException>(() => flag7_asFlag.ThrowIfUnknownFlags());
            flag8_asFlag.ThrowIfUnknownFlags();
            flag9_asFlag.ThrowIfUnknownFlags();
            flag10_asFlag.ThrowIfUnknownFlags();
            flag11_asFlag.ThrowIfUnknownFlags();
            allFlags_asFlag.ThrowIfUnknownFlags();
        }
    }
}
