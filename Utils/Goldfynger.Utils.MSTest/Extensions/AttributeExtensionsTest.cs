﻿using System;

using Goldfynger.Utils.Extensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Goldfynger.Utils.MSTest.Extensions
{
    [TestClass]
    public class AttributeExtensionsTest
    {
        [System.ComponentModel.Description(nameof(TypeWithDescription))]
        public class TypeWithDescription
        {
            [System.ComponentModel.Description(nameof(PublicStaticPropertyWithDescription))]
            public static object PublicStaticPropertyWithDescription { get; } = new object();

            [System.ComponentModel.Description(nameof(PublicInstancePropertyWithDescription))]
            public object PublicInstancePropertyWithDescription { get; } = new object();

            [System.ComponentModel.Description(nameof(InternalStaticPropertyWithDescription))]
            internal static object InternalStaticPropertyWithDescription { get; } = new object();

            [System.ComponentModel.Description(nameof(InternalInstancePropertyWithDescription))]
            internal object InternalInstancePropertyWithDescription { get; } = new object();

            [System.ComponentModel.Description(nameof(ProtectedStaticPropertyWithDescription))]
            protected static object ProtectedStaticPropertyWithDescription { get; } = new object();

            [System.ComponentModel.Description(nameof(ProtectedInstancePropertyWithDescription))]
            protected object ProtectedInstancePropertyWithDescription { get; } = new object();

            [System.ComponentModel.Description(nameof(PrivateStaticPropertyWithDescription))]
            private static object PrivateStaticPropertyWithDescription { get; } = new object();

            [System.ComponentModel.Description(nameof(PrivateInstancePropertyWithDescription))]
            private object PrivateInstancePropertyWithDescription { get; } = new object();
        }

        public sealed class TypeWithoutDescription
        {
            public static object PublicStaticPropertyWithoutDescription { get; } = new object();

            public object PublicInstancePropertyWithoutDescription { get; } = new object();
        }

        private enum EnumWithDescription
        {
            [System.ComponentModel.Description(nameof(ItemWithDescription))]
            ItemWithDescription,
        }

        private enum EnumWithoutDescription
        {
            ItemWithoutDescription,
        }

        [TestMethod]
        public void GetPropertyDescriptionTest()
        {
            Assert.AreEqual(nameof(TypeWithDescription.PublicStaticPropertyWithDescription), new TypeWithDescription().GetPropertyDescription(nameof(TypeWithDescription.PublicStaticPropertyWithDescription)));
            Assert.AreEqual(nameof(TypeWithDescription.PublicStaticPropertyWithDescription), typeof(TypeWithDescription).GetPropertyDescription(nameof(TypeWithDescription.PublicStaticPropertyWithDescription)));

            Assert.AreEqual(nameof(TypeWithDescription.PublicInstancePropertyWithDescription), new TypeWithDescription().GetPropertyDescription(nameof(TypeWithDescription.PublicInstancePropertyWithDescription)));
            Assert.AreEqual(nameof(TypeWithDescription.PublicInstancePropertyWithDescription), typeof(TypeWithDescription).GetPropertyDescription(nameof(TypeWithDescription.PublicInstancePropertyWithDescription)));

            Assert.AreEqual(nameof(TypeWithDescription.InternalStaticPropertyWithDescription), new TypeWithDescription().GetPropertyDescription(nameof(TypeWithDescription.InternalStaticPropertyWithDescription)));
            Assert.AreEqual(nameof(TypeWithDescription.InternalStaticPropertyWithDescription), typeof(TypeWithDescription).GetPropertyDescription(nameof(TypeWithDescription.InternalStaticPropertyWithDescription)));

            Assert.AreEqual(nameof(TypeWithDescription.InternalInstancePropertyWithDescription), new TypeWithDescription().GetPropertyDescription(nameof(TypeWithDescription.InternalInstancePropertyWithDescription)));
            Assert.AreEqual(nameof(TypeWithDescription.InternalInstancePropertyWithDescription), typeof(TypeWithDescription).GetPropertyDescription(nameof(TypeWithDescription.InternalInstancePropertyWithDescription)));

            Assert.ThrowsException<InvalidOperationException>(() => new TypeWithoutDescription().GetPropertyDescription(nameof(TypeWithoutDescription.PublicStaticPropertyWithoutDescription)));
            Assert.ThrowsException<InvalidOperationException>(() => typeof(TypeWithoutDescription).GetPropertyDescription(nameof(TypeWithoutDescription.PublicStaticPropertyWithoutDescription)));

            Assert.ThrowsException<InvalidOperationException>(() => new TypeWithoutDescription().GetPropertyDescription(nameof(TypeWithoutDescription.PublicInstancePropertyWithoutDescription)));
            Assert.ThrowsException<InvalidOperationException>(() => typeof(TypeWithoutDescription).GetPropertyDescription(nameof(TypeWithoutDescription.PublicInstancePropertyWithoutDescription)));

            Assert.ThrowsException<InvalidOperationException>(() => new TypeWithoutDescription().GetPropertyDescription("UnknownProperty"));
            Assert.ThrowsException<InvalidOperationException>(() => typeof(TypeWithoutDescription).GetPropertyDescription("UnknownProperty"));
        }

        [TestMethod]
        public void GetEnumDescriptionTest()
        {
            Assert.AreEqual(nameof(EnumWithDescription.ItemWithDescription), EnumWithDescription.ItemWithDescription.GetEnumDescription());

            Assert.ThrowsException<InvalidOperationException>(() => EnumWithoutDescription.ItemWithoutDescription.GetEnumDescription());
        }

        [TestMethod]
        public void GetTypeDescriptionTest()
        {
            Assert.AreEqual(nameof(TypeWithDescription), new TypeWithDescription().GetTypeDescription());
            Assert.AreEqual(nameof(TypeWithDescription), typeof(TypeWithDescription).GetTypeDescription());

            Assert.ThrowsException<InvalidOperationException>(() => new TypeWithoutDescription().GetTypeDescription());
            Assert.ThrowsException<InvalidOperationException>(() => typeof(TypeWithoutDescription).GetTypeDescription());
        }
    }
}
