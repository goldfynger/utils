﻿using System.Runtime.InteropServices;

using Goldfynger.Utils.Extensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Goldfynger.Utils.MSTest.Extensions
{
    [TestClass]
    public class ObjectExtensionsTest
    {
        [TestMethod]
        public void ToStructTest()
        {
            var smallArray = new byte[4];
            var mediumArray = new byte[8];
            var largeArray = new byte[16];

            smallArray.SetBoolean(0, true);
            smallArray.SetUInt8(1, 8);
            smallArray.SetUInt16(2, 16);

            mediumArray.SetBoolean(0, true);
            mediumArray.SetUInt8(1, 8);
            mediumArray.SetUInt16(2, 16);
            mediumArray.SetUInt32(4, 32);

            largeArray.SetBoolean(0, true);
            largeArray.SetUInt8(1, 8);
            largeArray.SetUInt16(2, 16);
            largeArray.SetUInt32(4, 32);
            largeArray.SetUInt32(8, 64);

            var smallArrayToSmallStruct = smallArray.ToStruct<SmallStruct>();
            var smallArrayToMediumStruct = smallArray.ToStruct<MediumStruct>();
            var smallArrayToLargeStruct = smallArray.ToStruct<LargeStruct>();

            var mediumArrayToSmallStruct = mediumArray.ToStruct<SmallStruct>();
            var mediumArrayToMediumStruct = mediumArray.ToStruct<MediumStruct>();
            var mediumArrayToLargeStruct = mediumArray.ToStruct<LargeStruct>();

            var largeArrayToSmallStruct = largeArray.ToStruct<SmallStruct>();
            var largeArrayToMediumStruct = largeArray.ToStruct<MediumStruct>();
            var largeArrayToLargeStruct = largeArray.ToStruct<LargeStruct>();

            Assert.AreEqual(smallArray.GetBoolean(0), smallArrayToSmallStruct.B);
            Assert.AreEqual(smallArray.GetUInt8(1), smallArrayToSmallStruct.U8);
            Assert.AreEqual(smallArray.GetUInt16(2), smallArrayToSmallStruct.U16);

            Assert.AreEqual(smallArray.GetBoolean(0), smallArrayToMediumStruct.B);
            Assert.AreEqual(smallArray.GetUInt8(1), smallArrayToMediumStruct.U8);
            Assert.AreEqual(smallArray.GetUInt16(2), smallArrayToMediumStruct.U16);

            Assert.AreEqual(smallArray.GetBoolean(0), smallArrayToLargeStruct.B);
            Assert.AreEqual(smallArray.GetUInt8(1), smallArrayToLargeStruct.U8);
            Assert.AreEqual(smallArray.GetUInt16(2), smallArrayToLargeStruct.U16);

            Assert.AreEqual(mediumArray.GetBoolean(0), mediumArrayToSmallStruct.B);
            Assert.AreEqual(mediumArray.GetUInt8(1), mediumArrayToSmallStruct.U8);
            Assert.AreEqual(mediumArray.GetUInt16(2), mediumArrayToSmallStruct.U16);

            Assert.AreEqual(mediumArray.GetBoolean(0), mediumArrayToMediumStruct.B);
            Assert.AreEqual(mediumArray.GetUInt8(1), mediumArrayToMediumStruct.U8);
            Assert.AreEqual(mediumArray.GetUInt16(2), mediumArrayToMediumStruct.U16);
            Assert.AreEqual(mediumArray.GetUInt32(4), mediumArrayToMediumStruct.U32);

            Assert.AreEqual(mediumArray.GetBoolean(0), mediumArrayToLargeStruct.B);
            Assert.AreEqual(mediumArray.GetUInt8(1), mediumArrayToLargeStruct.U8);
            Assert.AreEqual(mediumArray.GetUInt16(2), mediumArrayToLargeStruct.U16);
            Assert.AreEqual(mediumArray.GetUInt32(4), mediumArrayToLargeStruct.U32);

            Assert.AreEqual(largeArray.GetBoolean(0), largeArrayToSmallStruct.B);
            Assert.AreEqual(largeArray.GetUInt8(1), largeArrayToSmallStruct.U8);
            Assert.AreEqual(largeArray.GetUInt16(2), largeArrayToSmallStruct.U16);

            Assert.AreEqual(largeArray.GetBoolean(0), largeArrayToMediumStruct.B);
            Assert.AreEqual(largeArray.GetUInt8(1), largeArrayToMediumStruct.U8);
            Assert.AreEqual(largeArray.GetUInt16(2), largeArrayToMediumStruct.U16);
            Assert.AreEqual(largeArray.GetUInt32(4), largeArrayToMediumStruct.U32);

            Assert.AreEqual(largeArray.GetBoolean(0), largeArrayToLargeStruct.B);
            Assert.AreEqual(largeArray.GetUInt8(1), largeArrayToLargeStruct.U8);
            Assert.AreEqual(largeArray.GetUInt16(2), largeArrayToLargeStruct.U16);
            Assert.AreEqual(largeArray.GetUInt32(4), largeArrayToLargeStruct.U32);
            Assert.AreEqual(largeArray.GetUInt64(8), largeArrayToLargeStruct.U64);
        }

        [TestMethod]
        public void ToClassTest()
        {
            var smallArray = new byte[4];
            var mediumArray = new byte[8];
            var largeArray = new byte[16];

            smallArray.SetBoolean(0, true);
            smallArray.SetUInt8(1, 8);
            smallArray.SetUInt16(2, 16);

            mediumArray.SetBoolean(0, true);
            mediumArray.SetUInt8(1, 8);
            mediumArray.SetUInt16(2, 16);
            mediumArray.SetUInt32(4, 32);

            largeArray.SetBoolean(0, true);
            largeArray.SetUInt8(1, 8);
            largeArray.SetUInt16(2, 16);
            largeArray.SetUInt32(4, 32);
            largeArray.SetUInt32(8, 64);

            var smallArrayToSmallClass = smallArray.ToClass<SmallClass>();
            var smallArrayToMediumClass = smallArray.ToClass<MediumClass>();
            var smallArrayToLargeClass = smallArray.ToClass<LargeClass>();

            var mediumArrayToSmallClass = mediumArray.ToClass<SmallClass>();
            var mediumArrayToMediumClass = mediumArray.ToClass<MediumClass>();
            var mediumArrayToLargeClass = mediumArray.ToClass<LargeClass>();

            var largeArrayToSmallClass = largeArray.ToClass<SmallClass>();
            var largeArrayToMediumClass = largeArray.ToClass<MediumClass>();
            var largeArrayToLargeClass = largeArray.ToClass<LargeClass>();

            Assert.AreEqual(smallArray.GetBoolean(0), smallArrayToSmallClass.B);
            Assert.AreEqual(smallArray.GetUInt8(1), smallArrayToSmallClass.U8);
            Assert.AreEqual(smallArray.GetUInt16(2), smallArrayToSmallClass.U16);

            Assert.AreEqual(smallArray.GetBoolean(0), smallArrayToMediumClass.B);
            Assert.AreEqual(smallArray.GetUInt8(1), smallArrayToMediumClass.U8);
            Assert.AreEqual(smallArray.GetUInt16(2), smallArrayToMediumClass.U16);

            Assert.AreEqual(smallArray.GetBoolean(0), smallArrayToLargeClass.B);
            Assert.AreEqual(smallArray.GetUInt8(1), smallArrayToLargeClass.U8);
            Assert.AreEqual(smallArray.GetUInt16(2), smallArrayToLargeClass.U16);

            Assert.AreEqual(mediumArray.GetBoolean(0), mediumArrayToSmallClass.B);
            Assert.AreEqual(mediumArray.GetUInt8(1), mediumArrayToSmallClass.U8);
            Assert.AreEqual(mediumArray.GetUInt16(2), mediumArrayToSmallClass.U16);

            Assert.AreEqual(mediumArray.GetBoolean(0), mediumArrayToMediumClass.B);
            Assert.AreEqual(mediumArray.GetUInt8(1), mediumArrayToMediumClass.U8);
            Assert.AreEqual(mediumArray.GetUInt16(2), mediumArrayToMediumClass.U16);
            Assert.AreEqual(mediumArray.GetUInt32(4), mediumArrayToMediumClass.U32);

            Assert.AreEqual(mediumArray.GetBoolean(0), mediumArrayToLargeClass.B);
            Assert.AreEqual(mediumArray.GetUInt8(1), mediumArrayToLargeClass.U8);
            Assert.AreEqual(mediumArray.GetUInt16(2), mediumArrayToLargeClass.U16);
            Assert.AreEqual(mediumArray.GetUInt32(4), mediumArrayToLargeClass.U32);

            Assert.AreEqual(largeArray.GetBoolean(0), largeArrayToSmallClass.B);
            Assert.AreEqual(largeArray.GetUInt8(1), largeArrayToSmallClass.U8);
            Assert.AreEqual(largeArray.GetUInt16(2), largeArrayToSmallClass.U16);

            Assert.AreEqual(largeArray.GetBoolean(0), largeArrayToMediumClass.B);
            Assert.AreEqual(largeArray.GetUInt8(1), largeArrayToMediumClass.U8);
            Assert.AreEqual(largeArray.GetUInt16(2), largeArrayToMediumClass.U16);
            Assert.AreEqual(largeArray.GetUInt32(4), largeArrayToMediumClass.U32);

            Assert.AreEqual(largeArray.GetBoolean(0), largeArrayToLargeClass.B);
            Assert.AreEqual(largeArray.GetUInt8(1), largeArrayToLargeClass.U8);
            Assert.AreEqual(largeArray.GetUInt16(2), largeArrayToLargeClass.U16);
            Assert.AreEqual(largeArray.GetUInt32(4), largeArrayToLargeClass.U32);
            Assert.AreEqual(largeArray.GetUInt64(8), largeArrayToLargeClass.U64);
        }

        [TestMethod]
        public void ToByteArray()
        {
            var smallStruct = new SmallStruct
            {
                B = true,
                U8 = 8,
                U16 = 16,
            };

            var mediumStruct = new MediumStruct
            {
                B = true,
                U8 = 8,
                U16 = 16,
                U32 = 32,
            };

            var largeStruct = new LargeStruct
            {
                B = true,
                U8 = 8,
                U16 = 16,
                U32 = 32,
                U64 = 64,
            };

            var smallClass = new SmallClass(true, 8, 16);
            var mediumClass = new MediumClass(true, 8, 16, 32);
            var largeClass = new LargeClass(true, 8, 16, 32, 64);

            var smallArrayFromStruct = smallStruct.ToByteArray();
            var mediumArrayFromStruct = mediumStruct.ToByteArray();
            var largeArrayFromStruct = largeStruct.ToByteArray();

            var smallArrayFromClass = smallClass.ToByteArray();
            var mediumArrayFromClass = mediumClass.ToByteArray();
            var largeArrayFromClass = largeClass.ToByteArray();

            Assert.AreEqual(smallStruct.B, smallArrayFromStruct.GetBoolean(0));
            Assert.AreEqual(smallStruct.U8, smallArrayFromStruct.GetUInt8(1));
            Assert.AreEqual(smallStruct.U16, smallArrayFromStruct.GetUInt16(2));

            Assert.AreEqual(mediumStruct.B, mediumArrayFromStruct.GetBoolean(0));
            Assert.AreEqual(mediumStruct.U8, mediumArrayFromStruct.GetUInt8(1));
            Assert.AreEqual(mediumStruct.U16, mediumArrayFromStruct.GetUInt16(2));
            Assert.AreEqual(mediumStruct.U32, mediumArrayFromStruct.GetUInt32(4));

            Assert.AreEqual(largeStruct.B, largeArrayFromStruct.GetBoolean(0));
            Assert.AreEqual(largeStruct.U8, largeArrayFromStruct.GetUInt8(1));
            Assert.AreEqual(largeStruct.U16, largeArrayFromStruct.GetUInt16(2));
            Assert.AreEqual(largeStruct.U32, largeArrayFromStruct.GetUInt32(4));
            Assert.AreEqual(largeStruct.U64, largeArrayFromStruct.GetUInt64(8));

            Assert.AreEqual(smallClass.B, smallArrayFromClass.GetBoolean(0));
            Assert.AreEqual(smallClass.U8, smallArrayFromClass.GetUInt8(1));
            Assert.AreEqual(smallClass.U16, smallArrayFromClass.GetUInt16(2));

            Assert.AreEqual(mediumClass.B, mediumArrayFromClass.GetBoolean(0));
            Assert.AreEqual(mediumClass.U8, mediumArrayFromClass.GetUInt8(1));
            Assert.AreEqual(mediumClass.U16, mediumArrayFromClass.GetUInt16(2));
            Assert.AreEqual(mediumClass.U32, mediumArrayFromClass.GetUInt32(4));

            Assert.AreEqual(largeClass.B, largeArrayFromClass.GetBoolean(0));
            Assert.AreEqual(largeClass.U8, largeArrayFromClass.GetUInt8(1));
            Assert.AreEqual(largeClass.U16, largeArrayFromClass.GetUInt16(2));
            Assert.AreEqual(largeClass.U32, largeArrayFromClass.GetUInt32(4));
            Assert.AreEqual(largeClass.U64, largeArrayFromClass.GetUInt64(8));
        }

        [StructLayout(LayoutKind.Explicit, Size = 4)]
        private struct SmallStruct
        {
            [FieldOffset(0)] [MarshalAs(UnmanagedType.U1)] public bool B;
            [FieldOffset(1)] [MarshalAs(UnmanagedType.U1)] public byte U8;
            [FieldOffset(2)] [MarshalAs(UnmanagedType.U2)] public ushort U16;
        }

        [StructLayout(LayoutKind.Explicit, Size = 8)]
        private struct MediumStruct
        {
            [FieldOffset(0)] [MarshalAs(UnmanagedType.U1)] public bool B;
            [FieldOffset(1)] [MarshalAs(UnmanagedType.U1)] public byte U8;
            [FieldOffset(2)] [MarshalAs(UnmanagedType.U2)] public ushort U16;
            [FieldOffset(4)] [MarshalAs(UnmanagedType.U4)] public uint U32;
        }

        [StructLayout(LayoutKind.Explicit, Size = 16)]
        private struct LargeStruct
        {
            [FieldOffset(0)] [MarshalAs(UnmanagedType.U1)] public bool B;
            [FieldOffset(1)] [MarshalAs(UnmanagedType.U1)] public byte U8;
            [FieldOffset(2)] [MarshalAs(UnmanagedType.U2)] public ushort U16;
            [FieldOffset(4)] [MarshalAs(UnmanagedType.U4)] public uint U32;
            [FieldOffset(8)] [MarshalAs(UnmanagedType.U8)] public ulong U64;
        }

        [StructLayout(LayoutKind.Explicit, Size = 4)]
        private class SmallClass
        {
            [FieldOffset(0)] [MarshalAs(UnmanagedType.U1)] private readonly bool _b;
            [FieldOffset(1)] [MarshalAs(UnmanagedType.U1)] private readonly byte _u8;
            [FieldOffset(2)] [MarshalAs(UnmanagedType.U2)] private readonly ushort _u16;

            public SmallClass()
            {
            }

            public SmallClass(bool b, byte u8, ushort u16)
            {
                _b = b;
                _u8 = u8;
                _u16 = u16;
            }

            public bool B => _b;
            public byte U8 => _u8;
            public ushort U16 => _u16;
        }

        [StructLayout(LayoutKind.Explicit, Size = 8)]
        private class MediumClass
        {
            [FieldOffset(0)] [MarshalAs(UnmanagedType.U1)] private readonly bool _b;
            [FieldOffset(1)] [MarshalAs(UnmanagedType.U1)] private readonly byte _u8;
            [FieldOffset(2)] [MarshalAs(UnmanagedType.U2)] private readonly ushort _u16;
            [FieldOffset(4)] [MarshalAs(UnmanagedType.U4)] private readonly uint _u32;

            public MediumClass()
            {
            }

            public MediumClass(bool b, byte u8, ushort u16, uint u32)
            {
                _b = b;
                _u8 = u8;
                _u16 = u16;
                _u32 = u32;
            }

            public bool B => _b;
            public byte U8 => _u8;
            public ushort U16 => _u16;
            public uint U32 => _u32;
        }

        [StructLayout(LayoutKind.Explicit, Size = 16)]
        private class LargeClass
        {
            [FieldOffset(0)] [MarshalAs(UnmanagedType.U1)] private readonly bool _b;
            [FieldOffset(1)] [MarshalAs(UnmanagedType.U1)] private readonly byte _u8;
            [FieldOffset(2)] [MarshalAs(UnmanagedType.U2)] private readonly ushort _u16;
            [FieldOffset(4)] [MarshalAs(UnmanagedType.U4)] private readonly uint _u32;
            [FieldOffset(8)] [MarshalAs(UnmanagedType.U8)] private readonly ulong _u64;

            public LargeClass()
            {
            }

            public LargeClass(bool b, byte u8, ushort u16, uint u32, ulong u64)
            {
                _b = b;
                _u8 = u8;
                _u16 = u16;
                _u32 = u32;
                _u64 = u64;
            }

            public bool B => _b;
            public byte U8 => _u8;
            public ushort U16 => _u16;
            public uint U32 => _u32;
            public ulong U64 => _u64;
        }
    }
}
