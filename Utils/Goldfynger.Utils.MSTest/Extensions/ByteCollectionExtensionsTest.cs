using System;
using System.Collections.Generic;

using Goldfynger.Utils.Extensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Goldfynger.Utils.MSTest.Extensions
{
    [TestClass]
    public class ByteCollectionExtensionsTest
    {
        [TestMethod]
        public void GetUInt16Test()
        {
            var emptySource = new List<byte>(0);

            var source = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, };
            var forIndex0 = (ushort)0x3412;
            var forIndex1 = (ushort)0x5634;
            var forIndex5 = (ushort)0xCDAB;
            var forIndex6 = (ushort)0x00CD;

            Assert.ThrowsException<ArgumentException>(() => emptySource.GetUInt16(0));

            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt16(-1));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt16(source.Count - 1, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt16(source.Count, true));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt16(6, false));

            Assert.AreEqual(forIndex0, source.GetUInt16(0));
            Assert.AreEqual(forIndex1, source.GetUInt16(1));
            Assert.AreEqual(forIndex5, source.GetUInt16(5));
            Assert.AreEqual(forIndex6, source.GetUInt16(6));
        }

        [TestMethod]
        public void GetInt16Test()
        {
            var emptySource = new List<byte>(0);

            var source = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, };
            var forIndex0 = (short)0x3412;
            var forIndex1 = (short)0x5634;
            var forIndex5 = unchecked((short)0xCDAB);
            var forIndex6 = (short)0x00CD;

            Assert.ThrowsException<ArgumentException>(() => emptySource.GetInt16(0));

            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt16(-1));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt16(source.Count - 1, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt16(source.Count, true));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt16(6, false));

            Assert.AreEqual(forIndex0, source.GetInt16(0));
            Assert.AreEqual(forIndex1, source.GetInt16(1));
            Assert.AreEqual(forIndex5, source.GetInt16(5));
            Assert.AreEqual(forIndex6, source.GetInt16(6));
        }

        [TestMethod]
        public void GetUInt32Test()
        {
            var emptySource = new List<byte>(0);

            var source = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, 0x65, 0x87, 0x09, };
            var forIndex0 = (uint)0x78563412;
            var forIndex1 = 0x90785634;
            var forIndex9 = (uint)0x09876543;
            var forIndex10 = (uint)0x00098765;
            var forIndex11 = (uint)0x00000987;
            var forIndex12 = (uint)0x00000009;

            Assert.ThrowsException<ArgumentException>(() => emptySource.GetUInt32(0));

            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt32(-1));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt32(source.Count - 1, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt32(source.Count - 2, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt32(source.Count - 3, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt32(source.Count, true));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt32(10, false));

            Assert.AreEqual(forIndex0, source.GetUInt32(0));
            Assert.AreEqual(forIndex1, source.GetUInt32(1));
            Assert.AreEqual(forIndex9, source.GetUInt32(9));
            Assert.AreEqual(forIndex10, source.GetUInt32(10));
            Assert.AreEqual(forIndex11, source.GetUInt32(11));
            Assert.AreEqual(forIndex12, source.GetUInt32(12));
        }

        [TestMethod]
        public void GetInt32Test()
        {
            var emptySource = new List<byte>(0);

            var source = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, 0x65, 0x87, 0x09, };
            var forIndex0 = 0x78563412;
            var forIndex1 = unchecked((int)0x90785634);
            var forIndex9 = 0x09876543;
            var forIndex10 = 0x00098765;
            var forIndex11 = 0x00000987;
            var forIndex12 = 0x00000009;

            Assert.ThrowsException<ArgumentException>(() => emptySource.GetInt32(0));

            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt32(-1));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt32(source.Count - 1, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt32(source.Count - 2, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt32(source.Count - 3, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt32(source.Count, true));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt32(10, false));

            Assert.AreEqual(forIndex0, source.GetInt32(0));
            Assert.AreEqual(forIndex1, source.GetInt32(1));
            Assert.AreEqual(forIndex9, source.GetInt32(9));
            Assert.AreEqual(forIndex10, source.GetInt32(10));
            Assert.AreEqual(forIndex11, source.GetInt32(11));
            Assert.AreEqual(forIndex12, source.GetInt32(12));
        }

        [TestMethod]
        public void GetUInt64Test()
        {
            var emptySource = new List<byte>(0);

            var source = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, 0x65, 0x87, 0x09, };
            var forIndex0 = 0xEFCDAB9078563412;
            var forIndex1 = (ulong)0x21EFCDAB90785634;            
            var forIndex5 = (ulong)0x0987654321EFCDAB;
            var forIndex6 = (ulong)0x000987654321EFCD;
            var forIndex7 = (ulong)0x00000987654321EF;
            var forIndex8 = (ulong)0x0000000987654321;
            var forIndex9 = (ulong)0x0000000009876543;
            var forIndex10 = (ulong)0x0000000000098765;
            var forIndex11 = (ulong)0x0000000000000987;
            var forIndex12 = (ulong)0x0000000000000009;

            Assert.ThrowsException<ArgumentException>(() => emptySource.GetUInt64(0));

            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt64(-1));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt64(source.Count - 1, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt64(source.Count - 2, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt64(source.Count - 3, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt64(source.Count - 4, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt64(source.Count - 5, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt64(source.Count - 6, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt64(source.Count - 7, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt64(source.Count, true));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetUInt64(6, false));

            Assert.AreEqual(forIndex0, source.GetUInt64(0));
            Assert.AreEqual(forIndex1, source.GetUInt64(1));
            Assert.AreEqual(forIndex5, source.GetUInt64(5));
            Assert.AreEqual(forIndex6, source.GetUInt64(6));
            Assert.AreEqual(forIndex7, source.GetUInt64(7));
            Assert.AreEqual(forIndex8, source.GetUInt64(8));
            Assert.AreEqual(forIndex9, source.GetUInt64(9));
            Assert.AreEqual(forIndex10, source.GetUInt64(10));
            Assert.AreEqual(forIndex11, source.GetUInt64(11));
            Assert.AreEqual(forIndex12, source.GetUInt64(12));
        }

        [TestMethod]
        public void GetInt64Test()
        {
            var emptySource = new List<byte>(0);

            var source = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, 0x65, 0x87, 0x09, };
            var forIndex0 = unchecked((long)0xEFCDAB9078563412);
            var forIndex1 = 0x21EFCDAB90785634;
            var forIndex5 = 0x0987654321EFCDAB;
            var forIndex6 = 0x000987654321EFCD;
            var forIndex7 = 0x00000987654321EF;
            var forIndex8 = 0x0000000987654321;
            var forIndex9 = (long)0x0000000009876543;
            var forIndex10 = (long)0x0000000000098765;
            var forIndex11 = (long)0x0000000000000987;
            var forIndex12 = (long)0x0000000000000009;

            Assert.ThrowsException<ArgumentException>(() => emptySource.GetInt64(0));

            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt64(-1));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt64(source.Count - 1, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt64(source.Count - 2, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt64(source.Count - 3, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt64(source.Count - 4, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt64(source.Count - 5, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt64(source.Count - 6, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt64(source.Count - 7, false));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt64(source.Count, true));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => source.GetInt64(6, false));

            Assert.AreEqual(forIndex0, source.GetInt64(0));
            Assert.AreEqual(forIndex1, source.GetInt64(1));
            Assert.AreEqual(forIndex5, source.GetInt64(5));
            Assert.AreEqual(forIndex6, source.GetInt64(6));
            Assert.AreEqual(forIndex7, source.GetInt64(7));
            Assert.AreEqual(forIndex8, source.GetInt64(8));
            Assert.AreEqual(forIndex9, source.GetInt64(9));
            Assert.AreEqual(forIndex10, source.GetInt64(10));
            Assert.AreEqual(forIndex11, source.GetInt64(11));
            Assert.AreEqual(forIndex12, source.GetInt64(12));
        }


        [TestMethod]
        public void GetUInt16ListTest()
        {
            var emptySource = new List<byte>(0);
            var emptyResult = new List<ushort>(0);

            var source6 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, };
            var source7 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, };
            var result6 = new List<ushort> { 0x3412, 0x7856, 0xAB90, };
            var result7 = new List<ushort> { 0x3412, 0x7856, 0xAB90, 0x00CD, };

            CollectionAssert.AreEquivalent(emptyResult, emptySource.GetUInt16List());

            CollectionAssert.AreEquivalent(result6, source6.GetUInt16List(true));
            CollectionAssert.AreEquivalent(result6, source6.GetUInt16List(false));
            CollectionAssert.AreEquivalent(result7, source7.GetUInt16List(true));
            Assert.ThrowsException<ArgumentException>(() => source7.GetUInt16List(false));
        }

        [TestMethod]
        public void GetInt16ListTest()
        {
            var emptySource = new List<byte>(0);
            var emptyResult = new List<short>(0);

            var source6 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, };
            var source7 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, };
            var result6 = new List<short> { 0x3412, 0x7856, unchecked((short)0xAB90), };
            var result7 = new List<short> { 0x3412, 0x7856, unchecked((short)0xAB90), 0x00CD, };

            CollectionAssert.AreEquivalent(emptyResult, emptySource.GetInt16List());

            CollectionAssert.AreEquivalent(result6, source6.GetInt16List(true));
            CollectionAssert.AreEquivalent(result6, source6.GetInt16List(false));
            CollectionAssert.AreEquivalent(result7, source7.GetInt16List(true));
            Assert.ThrowsException<ArgumentException>(() => source7.GetInt16List(false));
        }

        [TestMethod]
        public void GetUInt32ListTest()
        {
            var emptySource = new List<byte>(0);
            var emptyResult = new List<uint>(0);

            var source8 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, };
            var source9 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, };
            var source10 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, };
            var source11 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, 0x65, };
            var result8 = new List<uint> { 0x78563412, 0xEFCDAB90, };
            var result9 = new List<uint> { 0x78563412, 0xEFCDAB90, 0x00000021, };
            var result10 = new List<uint> { 0x78563412, 0xEFCDAB90, 0x00004321, };
            var result11 = new List<uint> { 0x78563412, 0xEFCDAB90, 0x00654321, };

            CollectionAssert.AreEquivalent(emptyResult, emptySource.GetUInt32List());

            CollectionAssert.AreEquivalent(result8, source8.GetUInt32List(true));
            CollectionAssert.AreEquivalent(result8, source8.GetUInt32List(false));
            CollectionAssert.AreEquivalent(result9, source9.GetUInt32List(true));
            Assert.ThrowsException<ArgumentException>(() => source9.GetUInt32List(false));
            CollectionAssert.AreEquivalent(result10, source10.GetUInt32List(true));
            Assert.ThrowsException<ArgumentException>(() => source10.GetUInt32List(false));
            CollectionAssert.AreEquivalent(result11, source11.GetUInt32List(true));
            Assert.ThrowsException<ArgumentException>(() => source11.GetUInt32List(false));
        }

        [TestMethod]
        public void GetInt32ListTest()
        {
            var emptySource = new List<byte>(0);
            var emptyResult = new List<int>(0);

            var source8 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, };
            var source9 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, };
            var source10 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, };
            var source11 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, 0x65, };
            var result8 = new List<int> { 0x78563412, unchecked((int)0xEFCDAB90), };
            var result9 = new List<int> { 0x78563412, unchecked((int)0xEFCDAB90), 0x00000021, };
            var result10 = new List<int> { 0x78563412, unchecked((int)0xEFCDAB90), 0x00004321, };
            var result11 = new List<int> { 0x78563412, unchecked((int)0xEFCDAB90), 0x00654321, };

            CollectionAssert.AreEquivalent(emptyResult, emptySource.GetInt32List());

            CollectionAssert.AreEquivalent(result8, source8.GetInt32List(true));
            CollectionAssert.AreEquivalent(result8, source8.GetInt32List(false));
            CollectionAssert.AreEquivalent(result9, source9.GetInt32List(true));
            Assert.ThrowsException<ArgumentException>(() => source9.GetInt32List(false));
            CollectionAssert.AreEquivalent(result10, source10.GetInt32List(true));
            Assert.ThrowsException<ArgumentException>(() => source10.GetInt32List(false));
            CollectionAssert.AreEquivalent(result11, source11.GetInt32List(true));
            Assert.ThrowsException<ArgumentException>(() => source11.GetInt32List(false));
        }

        [TestMethod]
        public void GetUInt64ListTest()
        {
            var emptySource = new List<byte>(0);
            var emptyResult = new List<ulong>(0);

            var source8 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, };
            var source9 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, };
            var source10 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, };
            var source11 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, 0x65, };
            var source12 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, 0x65, 0x87,};
            var source13 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, 0x65, 0x87, 0x09, };
            var source14 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, 0x65, 0x87, 0x09, 0xBA, };
            var source15 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, 0x65, 0x87, 0x09, 0xBA, 0xDC, };
            var result8 = new List<ulong> { 0xEFCDAB9078563412, };
            var result9 = new List<ulong> { 0xEFCDAB9078563412, 0x0000000000000021 };
            var result10 = new List<ulong> { 0xEFCDAB9078563412, 0x0000000000004321 };
            var result11 = new List<ulong> { 0xEFCDAB9078563412, 0x0000000000654321 };
            var result12 = new List<ulong> { 0xEFCDAB9078563412, 0x0000000087654321 };
            var result13 = new List<ulong> { 0xEFCDAB9078563412, 0x0000000987654321 };
            var result14 = new List<ulong> { 0xEFCDAB9078563412, 0x0000BA0987654321 };
            var result15 = new List<ulong> { 0xEFCDAB9078563412, 0x00DCBA0987654321 };

            CollectionAssert.AreEquivalent(emptyResult, emptySource.GetUInt64List());

            CollectionAssert.AreEquivalent(result8, source8.GetUInt64List(true));
            CollectionAssert.AreEquivalent(result8, source8.GetUInt64List(false));
            CollectionAssert.AreEquivalent(result9, source9.GetUInt64List(true));
            Assert.ThrowsException<ArgumentException>(() => source9.GetUInt64List(false));
            CollectionAssert.AreEquivalent(result10, source10.GetUInt64List(true));
            Assert.ThrowsException<ArgumentException>(() => source10.GetUInt64List(false));
            CollectionAssert.AreEquivalent(result11, source11.GetUInt64List(true));
            Assert.ThrowsException<ArgumentException>(() => source11.GetUInt64List(false));
            CollectionAssert.AreEquivalent(result12, source12.GetUInt64List(true));
            Assert.ThrowsException<ArgumentException>(() => source11.GetUInt64List(false));
            CollectionAssert.AreEquivalent(result13, source13.GetUInt64List(true));
            Assert.ThrowsException<ArgumentException>(() => source11.GetUInt64List(false));
            CollectionAssert.AreEquivalent(result14, source14.GetUInt64List(true));
            Assert.ThrowsException<ArgumentException>(() => source11.GetUInt64List(false));
            CollectionAssert.AreEquivalent(result15, source15.GetUInt64List(true));
            Assert.ThrowsException<ArgumentException>(() => source11.GetUInt64List(false));
        }

        [TestMethod]
        public void GetInt64ListTest()
        {
            var emptySource = new List<byte>(0);
            var emptyResult = new List<long>(0);

            var source8 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, };
            var source9 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, };
            var source10 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, };
            var source11 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, 0x65, };
            var source12 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, 0x65, 0x87, };
            var source13 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, 0x65, 0x87, 0x09, };
            var source14 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, 0x65, 0x87, 0x09, 0xBA, };
            var source15 = new List<byte> { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF, 0x21, 0x43, 0x65, 0x87, 0x09, 0xBA, 0xDC, };
            var result8 = new List<long> { unchecked((long)0xEFCDAB9078563412), };
            var result9 = new List<long> { unchecked((long)0xEFCDAB9078563412), 0x0000000000000021 };
            var result10 = new List<long> { unchecked((long)0xEFCDAB9078563412), 0x0000000000004321 };
            var result11 = new List<long> { unchecked((long)0xEFCDAB9078563412), 0x0000000000654321 };
            var result12 = new List<long> { unchecked((long)0xEFCDAB9078563412), 0x0000000087654321 };
            var result13 = new List<long> { unchecked((long)0xEFCDAB9078563412), 0x0000000987654321 };
            var result14 = new List<long> { unchecked((long)0xEFCDAB9078563412), 0x0000BA0987654321 };
            var result15 = new List<long> { unchecked((long)0xEFCDAB9078563412), 0x00DCBA0987654321 };

            CollectionAssert.AreEquivalent(emptyResult, emptySource.GetInt64List());

            CollectionAssert.AreEquivalent(result8, source8.GetInt64List(true));
            CollectionAssert.AreEquivalent(result8, source8.GetInt64List(false));
            CollectionAssert.AreEquivalent(result9, source9.GetInt64List(true));
            Assert.ThrowsException<ArgumentException>(() => source9.GetInt64List(false));
            CollectionAssert.AreEquivalent(result10, source10.GetInt64List(true));
            Assert.ThrowsException<ArgumentException>(() => source10.GetInt64List(false));
            CollectionAssert.AreEquivalent(result11, source11.GetInt64List(true));
            Assert.ThrowsException<ArgumentException>(() => source11.GetInt64List(false));
            CollectionAssert.AreEquivalent(result12, source12.GetInt64List(true));
            Assert.ThrowsException<ArgumentException>(() => source11.GetInt64List(false));
            CollectionAssert.AreEquivalent(result13, source13.GetInt64List(true));
            Assert.ThrowsException<ArgumentException>(() => source11.GetInt64List(false));
            CollectionAssert.AreEquivalent(result14, source14.GetInt64List(true));
            Assert.ThrowsException<ArgumentException>(() => source11.GetInt64List(false));
            CollectionAssert.AreEquivalent(result15, source15.GetInt64List(true));
            Assert.ThrowsException<ArgumentException>(() => source11.GetInt64List(false));
        }
    }
}
