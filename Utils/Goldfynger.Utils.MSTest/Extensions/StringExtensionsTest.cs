﻿using System.Text;

using Goldfynger.Utils.Extensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Goldfynger.Utils.MSTest.Extensions
{
    [TestClass]
    public class StringExtensionsTest
    {
        [TestMethod]
        public void TrimEndMultilineTest()
        {
            Assert.AreEqual(
                null,
                StringExtensions.TrimEndMultiline((string?)null)
                );

            Assert.AreEqual(
                null,
                StringExtensions.TrimEndMultiline((StringBuilder?)null)
                );

            Assert.AreEqual(
                string.Empty,
                string.Empty.TrimEndMultiline()
                );

            Assert.AreEqual(
                "",
                " ".TrimEndMultiline()
                );

            Assert.AreEqual(
                "",
                "          ".TrimEndMultiline()
                );

            Assert.AreEqual(
                "    line",
                "    line".TrimEndMultiline()
                );

            Assert.AreEqual(
                "one line  string   with    many     spaces",
                "one line  string   with    many     spaces      ".TrimEndMultiline()
                );

            Assert.AreEqual(
                "\r",
                " \r ".TrimEndMultiline()
                );

            Assert.AreEqual(
                "\n",
                "     \n     ".TrimEndMultiline()
                );

            Assert.AreEqual(
                "\r\n",
                "          \r\n          ".TrimEndMultiline()
                );

            Assert.AreEqual(
                "    line\r\n    line",
                "    line\r\n    line".TrimEndMultiline()
                );

            Assert.AreEqual(
                "multi line\r   string    with\n      many\r\n        spaces",
                "multi line  \r   string    with     \n      many       \r\n        spaces         ".TrimEndMultiline()
                );
        }
    }
}
