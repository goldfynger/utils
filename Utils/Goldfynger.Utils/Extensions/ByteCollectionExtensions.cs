﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Goldfynger.Utils.Extensions
{
    /// <summary>Byte collection extensions.</summary>
    public static class ByteCollectionExtensions
    {
        /// <summary>Get one <see cref="byte"/> from <see cref="IList{T}"/> of <see cref="byte"/> by index.</summary>
        /// <param name="source">Source <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="index">Index of <see cref="byte"/> at source.</param>
        /// <returns>One <see cref="byte"/>.</returns>
        public static byte GetUInt8(this IList<byte> source, int index) => source[index];

        /// <summary>Get one <see cref="sbyte"/> from <see cref="IList{T}"/> of <see cref="byte"/> by index of byte.</summary>
        /// <param name="source">Source <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="index">Index of <see cref="sbyte"/> at source.</param>
        /// <returns>One <see cref="sbyte"/>.</returns>
        public static sbyte GetInt8(this IList<byte> source, int index) => unchecked((sbyte)source[index]);

        /// <summary>Get one <see cref="bool"/> from <see cref="IList{T}"/> of <see cref="byte"/> by index of byte.</summary>
        /// <param name="source">Source <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="index">Index of <see cref="bool"/> at source.</param>
        /// <returns>One <see cref="bool"/>.</returns>
        public static bool GetBoolean(this IList<byte> source, int index)
        {
            var array = new byte[1] { source[index] };

            return BitConverter.ToBoolean(array, 0);
        }

        /// <summary>Get one <see cref="ushort"/> from <see cref="IList{T}"/> of <see cref="byte"/> by index of first byte.</summary>
        /// <param name="source">Source <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="index">Index of first byte of <see cref="ushort"/> at source.</param>
        /// <param name="complementLastItem">Complement with additional zero byte if <paramref name="index"/> is exceeds <paramref name="source"/>.Count - 2.</param>
        /// <returns>One <see cref="ushort"/>.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="source"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException"><paramref name="source"/>.Count is zero.</exception>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="index"/> is less than 0.
        /// -or- <paramref name="index"/> is equal to or greater than <paramref name="source"/>.Count when <paramref name="complementLastItem"/> is <see langword="true"/>.
        /// -or- <paramref name="index"/> is equal to or greater than <paramref name="source"/>.Count - 1 when <paramref name="complementLastItem"/> is <see langword="false"/>.</exception>
        public static ushort GetUInt16(this IList<byte> source, int index, bool complementLastItem = true)
        {
            if (source.Count == 0)
            {
                throw new ArgumentException("Source can not be empty.", nameof(source));
            }

            if (index < 0 || index > source.Count - (complementLastItem ? 1 : 2))
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            var array = new byte[2];
            var bytesToCopy = index <= source.Count - 2 ? 2 : source.Count - index;

            source.CopyTo(index, array, 0, bytesToCopy);

            return BitConverter.ToUInt16(array);
        }

        /// <summary>Get one <see cref="short"/> from <see cref="IList{T}"/> of <see cref="byte"/> by index of first byte.</summary>
        /// <param name="source">Source <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="index">Index of first byte of <see cref="short"/> at source.</param>
        /// <param name="complementLastItem">Complement with additional zero byte if <paramref name="index"/> is exceeds <paramref name="source"/>.Count - 2.</param>
        /// <returns>One <see cref="short"/>.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="source"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException"><paramref name="source"/>.Count is zero.</exception>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="index"/> is less than 0.
        /// -or- <paramref name="index"/> is equal to or greater than <paramref name="source"/>.Count when <paramref name="complementLastItem"/> is <see langword="true"/>.
        /// -or- <paramref name="index"/> is equal to or greater than <paramref name="source"/>.Count - 1 when <paramref name="complementLastItem"/> is <see langword="false"/>.</exception>
        public static short GetInt16(this IList<byte> source, int index, bool complementLastItem = true)
        {
            if (source.Count == 0)
            {
                throw new ArgumentException("Source can not be empty.", nameof(source));
            }

            if (index < 0 || index > source.Count - (complementLastItem ? 1 : 2))
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            var array = new byte[2];
            var bytesToCopy = index <= source.Count - 2 ? 2 : source.Count - index;

            source.CopyTo(index, array, 0, bytesToCopy);

            return BitConverter.ToInt16(array);
        }

        /// <summary>Get one <see cref="uint"/> from <see cref="IList{T}"/> of <see cref="byte"/> by index of first byte.</summary>
        /// <param name="source">Source <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="index">Index of first byte of <see cref="uint"/> at source.</param>
        /// <param name="complementLastItem">Complement with additional zero bytes if <paramref name="index"/> is exceeds <paramref name="source"/>.Count - 4.</param>
        /// <returns>One <see cref="uint"/>.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="source"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException"><paramref name="source"/>.Count is zero.</exception>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="index"/> is less than 0.
        /// -or- <paramref name="index"/> is equal to or greater than <paramref name="source"/>.Count when <paramref name="complementLastItem"/> is <see langword="true"/>.
        /// -or- <paramref name="index"/> is equal to or greater than <paramref name="source"/>.Count - 3 when <paramref name="complementLastItem"/> is <see langword="false"/>.</exception>
        public static uint GetUInt32(this IList<byte> source, int index, bool complementLastItem = true)
        {
            if (source.Count == 0)
            {
                throw new ArgumentException("Source can not be empty.", nameof(source));
            }

            if (index < 0 || index > source.Count - (complementLastItem ? 1 : 4))
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            var array = new byte[4];
            var bytesToCopy = index <= source.Count - 4 ? 4 : source.Count - index;

            source.CopyTo(index, array, 0, bytesToCopy);

            return BitConverter.ToUInt32(array);
        }

        /// <summary>Get one <see cref="int"/> from <see cref="IList{T}"/> of <see cref="byte"/> by index of first byte.</summary>
        /// <param name="source">Source <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="index">Index of first byte of <see cref="int"/> at source.</param>
        /// <param name="complementLastItem">Complement with additional zero bytes if <paramref name="index"/> is exceeds <paramref name="source"/>.Count - 4.</param>
        /// <returns>One <see cref="int"/>.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="source"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException"><paramref name="source"/>.Count is zero.</exception>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="index"/> is less than 0.
        /// -or- <paramref name="index"/> is equal to or greater than <paramref name="source"/>.Count when <paramref name="complementLastItem"/> is <see langword="true"/>.
        /// -or- <paramref name="index"/> is equal to or greater than <paramref name="source"/>.Count - 3 when <paramref name="complementLastItem"/> is <see langword="false"/>.</exception>
        public static int GetInt32(this IList<byte> source, int index, bool complementLastItem = true)
        {
            if (source.Count == 0)
            {
                throw new ArgumentException("Source can not be empty.", nameof(source));
            }

            if (index < 0 || index > source.Count - (complementLastItem ? 1 : 4))
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            var array = new byte[4];
            var bytesToCopy = index <= source.Count - 4 ? 4 : source.Count - index;

            source.CopyTo(index, array, 0, bytesToCopy);

            return BitConverter.ToInt32(array);
        }

        /// <summary>Get one <see cref="ulong"/> from <see cref="IList{T}"/> of <see cref="byte"/> by index of first byte.</summary>
        /// <param name="source">Source <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="index">Index of first byte of <see cref="ulong"/> at source.</param>
        /// <param name="complementLastItem">Complement with additional zero bytes if <paramref name="index"/> is exceeds <paramref name="source"/>.Count - 8.</param>
        /// <returns>One <see cref="ulong"/>.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="source"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException"><paramref name="source"/>.Count is zero.</exception>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="index"/> is less than 0.
        /// -or- <paramref name="index"/> is equal to or greater than <paramref name="source"/>.Count when <paramref name="complementLastItem"/> is <see langword="true"/>.
        /// -or- <paramref name="index"/> is equal to or greater than <paramref name="source"/>.Count - 7 when <paramref name="complementLastItem"/> is <see langword="false"/>.</exception>
        public static ulong GetUInt64(this IList<byte> source, int index, bool complementLastItem = true)
        {
            if (source.Count == 0)
            {
                throw new ArgumentException("Source can not be empty.", nameof(source));
            }

            if (index < 0 || index > source.Count - (complementLastItem ? 1 : 8))
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            var array = new byte[8];
            var bytesToCopy = index <= source.Count - 8 ? 8 : source.Count - index;

            source.CopyTo(index, array, 0, bytesToCopy);

            return BitConverter.ToUInt64(array);
        }

        /// <summary>Get one <see cref="long"/> from <see cref="IList{T}"/> of <see cref="byte"/> by index of first byte.</summary>
        /// <param name="source">Source <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="index">Index of first byte of <see cref="long"/> at source.</param>
        /// <param name="complementLastItem">Complement with additional zero bytes if <paramref name="index"/> is exceeds <paramref name="source"/>.Count - 8.</param>
        /// <returns>One <see cref="long"/>.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="source"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException"><paramref name="source"/>.Count is zero.</exception>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="index"/> is less than 0.
        /// -or- <paramref name="index"/> is equal to or greater than <paramref name="source"/>.Count when <paramref name="complementLastItem"/> is <see langword="true"/>.
        /// -or- <paramref name="index"/> is equal to or greater than <paramref name="source"/>.Count - 7 when <paramref name="complementLastItem"/> is <see langword="false"/>.</exception>
        public static long GetInt64(this IList<byte> source, int index, bool complementLastItem = true)
        {
            if (source.Count == 0)
            {
                throw new ArgumentException("Source can not be empty.", nameof(source));
            }

            if (index < 0 || index > source.Count - (complementLastItem ? 1 : 8))
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            var array = new byte[8];
            var bytesToCopy = index <= source.Count - 8 ? 8 : source.Count - index;

            source.CopyTo(index, array, 0, bytesToCopy);

            return BitConverter.ToInt64(array);
        }


        /// <summary>Creates <see cref="List{T}"/> of <see cref="sbyte"/> from <see cref="IList{T}"/> of <see cref="byte"/>.</summary>
        /// <param name="source">Source <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <returns><see cref="List{T}"/> of <see cref="sbyte"/>.</returns>
        public static List<sbyte> GetInt8List(this IList<byte> source) => source.Select(b => unchecked((sbyte)b)).ToList();

        /// <summary>Creates <see cref="List{T}"/> of <see cref="ushort"/> from <see cref="IList{T}"/> of <see cref="byte"/>.</summary>
        /// <param name="source">Source <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="complementLastItem">Complement with additional zero byte if <paramref name="source"/>.Count is not divisible by 2.</param>
        /// <returns><see cref="List{T}"/> of <see cref="ushort"/>.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="source"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException"><paramref name="source"/>.Count is not divisible by 2 and <paramref name="complementLastItem"/> is <see langword="false"/>.</exception>
        public static List<ushort> GetUInt16List(this IList<byte> source, bool complementLastItem = true)
        {
            if (source.Count == 0)
            {
                return new List<ushort>(0);
            }

            if (source.Count % 2 != 0 && !complementLastItem)
            {
                throw new ArgumentException("Source count must be divisible by two if complementLastItem parameter is false.", nameof(source));
            }

            var result = new List<ushort>(source.Count / 2 + 1);

            for (var index = 0; index < source.Count; index += 2)
            {
                result.Add(source.GetUInt16(index, complementLastItem));
            }

            return result;
        }

        /// <summary>Creates <see cref="List{T}"/> of <see cref="short"/> from <see cref="IList{T}"/> of <see cref="byte"/>.</summary>
        /// <param name="source">Source <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="complementLastItem">Complement with additional zero byte if <paramref name="source"/>.Count is not divisible by 2.</param>
        /// <returns><see cref="List{T}"/> of <see cref="short"/>.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="source"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException"><paramref name="source"/>.Count is not divisible by 2 and <paramref name="complementLastItem"/> is <see langword="false"/>.</exception>
        public static List<short> GetInt16List(this IList<byte> source, bool complementLastItem = true)
        {
            if (source.Count == 0)
            {
                return new List<short>(0);
            }

            if (source.Count % 2 != 0 && !complementLastItem)
            {
                throw new ArgumentException("Source count must be divisible by two if complementLastItem parameter is false.", nameof(source));
            }

            var result = new List<short>(source.Count / 2 + 1);

            for (var index = 0; index < source.Count; index += 2)
            {
                result.Add(source.GetInt16(index, complementLastItem));
            }

            return result;
        }

        /// <summary>Creates <see cref="List{T}"/> of <see cref="uint"/> from <see cref="IList{T}"/> of <see cref="byte"/>.</summary>
        /// <param name="source">Source <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="complementLastItem">Complement with additional zero bytes if <paramref name="source"/>.Count is not divisible by 4.</param>
        /// <returns><see cref="List{T}"/> of <see cref="uint"/>.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="source"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException"><paramref name="source"/>.Count is not divisible by 4 and <paramref name="complementLastItem"/> is <see langword="false"/>.</exception>
        public static List<uint> GetUInt32List(this IList<byte> source, bool complementLastItem = true)
        {
            if (source.Count == 0)
            {
                return new List<uint>(0);
            }

            if (source.Count % 4 != 0 && !complementLastItem)
            {
                throw new ArgumentException("Source count must be divisible by two if complementLastItem parameter is false.", nameof(source));
            }

            var result = new List<uint>(source.Count / 4 + 1);

            for (var index = 0; index < source.Count; index += 4)
            {
                result.Add(source.GetUInt32(index, complementLastItem));
            }

            return result;
        }

        /// <summary>Creates <see cref="List{T}"/> of <see cref="int"/> from <see cref="IList{T}"/> of <see cref="byte"/>.</summary>
        /// <param name="source">Source <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="complementLastItem">Complement with additional zero bytes if <paramref name="source"/>.Count is not divisible by 4.</param>
        /// <returns><see cref="List{T}"/> of <see cref="int"/>.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="source"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException"><paramref name="source"/>.Count is not divisible by 4 and <paramref name="complementLastItem"/> is <see langword="false"/>.</exception>
        public static List<int> GetInt32List(this IList<byte> source, bool complementLastItem = true)
        {
            if (source.Count == 0)
            {
                return new List<int>(0);
            }

            if (source.Count % 4 != 0 && !complementLastItem)
            {
                throw new ArgumentException("Source count must be divisible by two if complementLastItem parameter is false.", nameof(source));
            }

            var result = new List<int>(source.Count / 4 + 1);

            for (var index = 0; index < source.Count; index += 4)
            {
                result.Add(source.GetInt32(index, complementLastItem));
            }

            return result;
        }

        /// <summary>Creates <see cref="List{T}"/> of <see cref="ulong"/> from <see cref="IList{T}"/> of <see cref="byte"/>.</summary>
        /// <param name="source">Source <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="complementLastItem">Complement with additional zero bytes if <paramref name="source"/>.Count is not divisible by 8.</param>
        /// <returns><see cref="List{T}"/> of <see cref="ulong"/>.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="source"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException"><paramref name="source"/>.Count is not divisible by 8 and <paramref name="complementLastItem"/> is <see langword="false"/>.</exception>
        public static List<ulong> GetUInt64List(this IList<byte> source, bool complementLastItem = true)
        {
            if (source.Count == 0)
            {
                return new List<ulong>(0);
            }

            if (source.Count % 8 != 0 && !complementLastItem)
            {
                throw new ArgumentException("Source count must be divisible by two if complementLastItem parameter is false.", nameof(source));
            }

            var result = new List<ulong>(source.Count / 8 + 1);

            for (var index = 0; index < source.Count; index += 8)
            {
                result.Add(source.GetUInt64(index, complementLastItem));
            }

            return result;
        }

        /// <summary>Creates <see cref="List{T}"/> of <see cref="long"/> from <see cref="IList{T}"/> of <see cref="byte"/>.</summary>
        /// <param name="source">Source <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="complementLastItem">Complement with additional zero bytes if <paramref name="source"/>.Count is not divisible by 8.</param>
        /// <returns><see cref="List{T}"/> of <see cref="long"/>.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="source"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException"><paramref name="source"/>.Count is not divisible by 8 and <paramref name="complementLastItem"/> is <see langword="false"/>.</exception>
        public static List<long> GetInt64List(this IList<byte> source, bool complementLastItem = true)
        {
            if (source.Count == 0)
            {
                return new List<long>(0);
            }

            if (source.Count % 8 != 0 && !complementLastItem)
            {
                throw new ArgumentException("Source count must be divisible by two if complementLastItem parameter is false.", nameof(source));
            }

            var result = new List<long>(source.Count / 8 + 1);

            for (var index = 0; index < source.Count; index += 8)
            {
                result.Add(source.GetInt64(index, complementLastItem));
            }

            return result;
        }


        /// <summary>Set one <see cref="byte"/> to <see cref="IList{T}"/> of <see cref="byte"/> by index.</summary>
        /// <param name="destination">Destination <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="index">Index of <see cref="byte"/> at destination.</param>
        /// <param name="value">One <see cref="byte"/>.</param>
        public static void SetUInt8(this IList<byte> destination, int index, byte value) => destination[index] = value;

        /// <summary>Set one <see cref="sbyte"/> to <see cref="IList{T}"/> of <see cref="byte"/> by index of byte.</summary>
        /// <param name="destination">Destination <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="index">Index of <see cref="sbyte"/> at destination.</param>
        /// <param name="value">One <see cref="sbyte"/>.</param>
        public static void SetInt8(this IList<byte> destination, int index, sbyte value) => destination[index] = unchecked((byte)value);

        /// <summary>Set one <see cref="bool"/> to <see cref="IList{T}"/> of <see cref="byte"/> by index of byte.</summary>
        /// <param name="destination">Destination <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="index">Index of <see cref="bool"/> at destination.</param>
        /// <param name="value">One <see cref="bool"/>.</param>
        public static void SetBoolean(this IList<byte> destination, int index, bool value)
        {
            var array = BitConverter.GetBytes(value);

            destination[index] = array[0];
        }

        /// <summary>Set one <see cref="ushort"/> to <see cref="IList{T}"/> of <see cref="byte"/> by index of first byte.</summary>
        /// <param name="destination">Destination <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="index">Index of first byte of <see cref="ushort"/> at destination.</param>
        /// <param name="value">One <see cref="ushort"/>.</param>
        public static void SetUInt16(this IList<byte> destination, int index, ushort value)
        {
            var array = BitConverter.GetBytes(value);

            destination[index] = array[0];
            destination[index + 1] = array[1];
        }

        /// <summary>Set one <see cref="short"/> to <see cref="IList{T}"/> of <see cref="byte"/> by index of first byte.</summary>
        /// <param name="destination">Destination <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="index">Index of first byte of <see cref="short"/> at destination.</param>
        /// <param name="value">One <see cref="short"/>.</param>
        public static void SetInt16(this IList<byte> destination, int index, short value)
        {
            var array = BitConverter.GetBytes(value);

            destination[index] = array[0];
            destination[index + 1] = array[1];
        }

        /// <summary>Set one <see cref="uint"/> to <see cref="IList{T}"/> of <see cref="byte"/> by index of first byte.</summary>
        /// <param name="destination">Destination <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="index">Index of first byte of <see cref="uint"/> at destination.</param>
        /// <param name="value">One <see cref="uint"/>.</param>
        public static void SetUInt32(this IList<byte> destination, int index, uint value)
        {
            var array = BitConverter.GetBytes(value);

            destination[index] = array[0];
            destination[index + 1] = array[1];
            destination[index + 2] = array[2];
            destination[index + 3] = array[3];
        }

        /// <summary>Set one <see cref="int"/> to <see cref="IList{T}"/> of <see cref="byte"/> by index of first byte.</summary>
        /// <param name="destination">Destination <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="index">Index of first byte of <see cref="int"/> at destination.</param>
        /// <param name="value">One <see cref="int"/>.</param>
        public static void SetInt32(this IList<byte> destination, int index, int value)
        {
            var array = BitConverter.GetBytes(value);

            destination[index] = array[0];
            destination[index + 1] = array[1];
            destination[index + 2] = array[2];
            destination[index + 3] = array[3];
        }

        /// <summary>Set one <see cref="ulong"/> to <see cref="IList{T}"/> of <see cref="byte"/> by index of first byte.</summary>
        /// <param name="destination">Destination <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="index">Index of first byte of <see cref="ulong"/> at destination.</param>
        /// <param name="value">One <see cref="ulong"/>.</param>
        public static void SetUInt64(this IList<byte> destination, int index, ulong value)
        {
            var array = BitConverter.GetBytes(value);

            destination[index] = array[0];
            destination[index + 1] = array[1];
            destination[index + 2] = array[2];
            destination[index + 3] = array[3];
            destination[index + 4] = array[4];
            destination[index + 5] = array[5];
            destination[index + 6] = array[6];
            destination[index + 7] = array[7];
        }

        /// <summary>Set one <see cref="long"/> to <see cref="IList{T}"/> of <see cref="byte"/> by index of first byte.</summary>
        /// <param name="destination">Destination <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="index">Index of first byte of <see cref="long"/> at destination.</param>
        /// <param name="value">One <see cref="long"/>.</param>
        public static void SetInt64(this IList<byte> destination, int index, long value)
        {
            var array = BitConverter.GetBytes(value);

            destination[index] = array[0];
            destination[index + 1] = array[1];
            destination[index + 2] = array[2];
            destination[index + 3] = array[3];
            destination[index + 4] = array[4];
            destination[index + 5] = array[5];
            destination[index + 6] = array[6];
            destination[index + 7] = array[7];
        }


        /// <summary>Converts list of bytes to hex string.</summary>
        /// <param name="source">Source <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        /// <param name="separator">Items separator in resukt <see cref="string"/>.</param>
        /// <param name="addHexHeader">Adds hex header to each items if set.</param>
        /// <returns>Result <see cref="string"/>.</returns>
        public static string ToHexString(this IList<byte> source, string separator = " ", bool addHexHeader = true)
        {
            return source.ConvertToStringWithSeparator(separator, b =>
            {
                var result = b.ToString("X2");
                if (addHexHeader)
                {
                    result = result.AddHexHeader();
                }
                return result;
            });
        }
    }
}
