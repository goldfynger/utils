﻿using System;

namespace Goldfynger.Utils.Extensions
{
    /// <summary><see cref="Enum"/> extensions.</summary>
    public static class EnumExtensions
    {
        /// <summary>Throws <see cref="ArgumentException"/> if specified value is not defined in target <see cref="Enum"/>.</summary>
        /// <typeparam name="TEnum">Type of <see cref="Enum"/>.</typeparam>
        /// <param name="value"><see cref="Enum"/> value.</param>
        /// <exception cref="ArgumentException"><paramref name="value"/> is not defined in target <see cref="Enum"/>.</exception>
        public static void ThrowIfNotDefined<TEnum>(this TEnum value) where TEnum : Enum
        {
            if (!Enum.IsDefined(typeof(TEnum), value))
            {
                throw new ArgumentException($"{typeof(TEnum).Name} has no value {value}.");
            }
        }

        /// <summary>Throws <see cref="ArgumentException"/> if specified value has flags that not defined in target <see cref="Enum"/>.</summary>
        /// <typeparam name="TEnum">Type of <see cref="Enum"/>.</typeparam>
        /// <param name="value"><see cref="Enum"/> value.</param>
        /// <exception cref="ArgumentException"><paramref name="value"/> is not defined in target <see cref="Enum"/>.</exception>
        /// <remarks><typeparamref name="TEnum"/> should has <see cref="FlagsAttribute"/>.</remarks>
        public static void ThrowIfUnknownFlags<TEnum>(this TEnum value) where TEnum : Enum
        {
            if (!Enum.IsDefined(typeof(TEnum), value))
            {
                var type = Enum.GetUnderlyingType(typeof(TEnum));

                if (type == typeof(byte))
                {
                    for (var index = 0; index < 8; index++)
                    {
                        var enumFlag = (TEnum)Enum.Parse(typeof(TEnum), (1 << index).ToString());

#pragma warning disable CA2248 // Provide correct 'enum' argument to 'Enum.HasFlag'
                        if (value.HasFlag(enumFlag)) /* It works correct if TEnum has FlagsAttribute */
#pragma warning restore CA2248 // Provide correct 'enum' argument to 'Enum.HasFlag'
                        {
                            if (!Enum.IsDefined(typeof(TEnum), enumFlag))
                            {
                                throw new ArgumentException($"{typeof(TEnum).Name} has no value {value}.");
                            }
                        }
                    }
                }
                else
                {
                    throw new NotSupportedException("Only enums with underlying type \"System.Byte\" supported by this extension method.");
                }
            }
        }
    }
}
