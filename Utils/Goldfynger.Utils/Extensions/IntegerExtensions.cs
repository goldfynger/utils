﻿namespace Goldfynger.Utils.Extensions
{
    /// <summary>Integer extensions.</summary>
    public static class IntegerExtensions
    {
        /// <summary>Checks if specified <see cref="byte"/> is power of two.</summary>
        /// <param name="number"><see cref="byte"/> value.</param>
        /// <returns><see langword="true"/> if <see cref="byte"/> is power of two and not equal to zero; otherwise <see langword="false"/>.</returns>
        public static bool IsPowerOfTwo(this byte number) => (number > 0) && ((number & (number - 1)) == 0);

        /// <summary>Checks if specified <see cref="sbyte"/> is power of two.</summary>
        /// <param name="number"><see cref="sbyte"/> value.</param>
        /// <returns><see langword="true"/> if <see cref="sbyte"/> is power of two and not equal to zero; otherwise <see langword="false"/>.</returns>
        public static bool IsPowerOfTwo(this sbyte number) => (number > 0) && ((number & (number - 1)) == 0);

        /// <summary>Checks if specified <see cref="ushort"/> is power of two.</summary>
        /// <param name="number"><see cref="ushort"/> value.</param>
        /// <returns><see langword="true"/> if <see cref="ushort"/> is power of two and not equal to zero; otherwise <see langword="false"/>.</returns>
        public static bool IsPowerOfTwo(this ushort number) => (number > 0) && ((number & (number - 1)) == 0);

        /// <summary>Checks if specified <see cref="short"/> is power of two.</summary>
        /// <param name="number"><see cref="short"/> value.</param>
        /// <returns><see langword="true"/> if <see cref="short"/> is power of two and not equal to zero; otherwise <see langword="false"/>.</returns>
        public static bool IsPowerOfTwo(this short number) => (number > 0) && ((number & (number - 1)) == 0);

        /// <summary>Checks if specified <see cref="uint"/> is power of two.</summary>
        /// <param name="number"><see cref="uint"/> value.</param>
        /// <returns><see langword="true"/> if <see cref="uint"/> is power of two and not equal to zero; otherwise <see langword="false"/>.</returns>
        public static bool IsPowerOfTwo(this uint number) => (number > 0) && ((number & (number - 1)) == 0);

        /// <summary>Checks if specified <see cref="int"/> is power of two.</summary>
        /// <param name="number"><see cref="int"/> value.</param>
        /// <returns><see langword="true"/> if <see cref="int"/> is power of two and not equal to zero; otherwise <see langword="false"/>.</returns>
        public static bool IsPowerOfTwo(this int number) => (number > 0) && ((number & (number - 1)) == 0);

        /// <summary>Checks if specified <see cref="ulong"/> is power of two.</summary>
        /// <param name="number"><see cref="ulong"/> value.</param>
        /// <returns><see langword="true"/> if <see cref="ulong"/> is power of two and not equal to zero; otherwise <see langword="false"/>.</returns>
        public static bool IsPowerOfTwo(this ulong number) => (number > 0) && ((number & (number - 1)) == 0);

        /// <summary>Checks if specified <see cref="long"/> is power of two.</summary>
        /// <param name="number"><see cref="long"/> value.</param>
        /// <returns><see langword="true"/> if <see cref="long"/> is power of two and not equal to zero; otherwise <see langword="false"/>.</returns>
        public static bool IsPowerOfTwo(this long number) => (number > 0) && ((number & (number - 1)) == 0);
    }
}
