﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Goldfynger.Utils.Extensions
{
    /// <summary><see cref="Stream"/> extensions.</summary>
    public static class StreamExtensions
    {
        /// <summary>Read from <see cref="Stream"/> to <see cref="Array"/> of <see cref="byte"/>.</summary>
        /// <param name="stream">Source <see cref="Stream"/>.</param>
        /// <param name="array">Destination <see cref="Array"/> of <see cref="byte"/>.</param>
        /// <returns>The total number of bytes read into array. This can be less than <see cref="Array"/> length.</returns>
        public static int Read(this Stream stream, byte[] array) => stream.Read(array, 0, array.Length);

        /// <summary>Write to <see cref="Stream"/> from <see cref="Array"/> of <see cref="byte"/>.</summary>
        /// <param name="stream">Destination <see cref="Stream"/>.</param>
        /// <param name="array">Source <see cref="Array"/> of <see cref="byte"/>.</param>
        public static void Write(this Stream stream, byte[] array) => stream.Write(array, 0, array.Length);

        /// <summary>Asynchronously read from <see cref="Stream"/> to <see cref="Array"/> of <see cref="byte"/>.</summary>
        /// <param name="stream">Source <see cref="Stream"/>.</param>
        /// <param name="array">Destination <see cref="Array"/> of <see cref="byte"/>.</param>
        /// <returns>Result <see cref="Task{T}"/> of <see cref="int"/> with total number of bytes read into array. This can be less than <see cref="Array"/> length.</returns>
        public static Task<int> ReadAsync(this Stream stream, byte[] array) => stream.ReadAsync(array, 0, array.Length);

        /// <summary>Asynchronously write to <see cref="Stream"/> from <see cref="Array"/> of <see cref="byte"/>.</summary>
        /// <param name="stream">Destination <see cref="Stream"/>.</param>
        /// <param name="array">Source <see cref="Array"/> of <see cref="byte"/>.</param>
        /// <returns>Result <see cref="Task"/>.</returns>
        public static Task WriteAsync(this Stream stream, byte[] array) => stream.WriteAsync(array, 0, array.Length);

        /// <summary>Asynchronously read from <see cref="Stream"/> to <see cref="Array"/> of <see cref="byte"/>.</summary>
        /// <param name="stream">Source <see cref="Stream"/>.</param>
        /// <param name="array">Destination <see cref="Array"/> of <see cref="byte"/>.</param>
        /// <param name="cancellationToken"><see cref="CancellationToken"/> to cancel.</param>
        /// <returns>Result <see cref="Task{T}"/> of <see cref="int"/> with total number of bytes read into array. This can be less than <see cref="Array"/> length.</returns>
        public static Task<int> ReadAsync(this Stream stream, byte[] array, CancellationToken cancellationToken) => stream.ReadAsync(array, 0, array.Length, cancellationToken);

        /// <summary>Asynchronously write to <see cref="Stream"/> from <see cref="Array"/> of <see cref="byte"/>.</summary>
        /// <param name="stream">Destination <see cref="Stream"/>.</param>
        /// <param name="array">Source <see cref="Array"/> of <see cref="byte"/>.</param>
        /// <param name="cancellationToken"><see cref="CancellationToken"/> to cancel.</param>
        /// <returns>Result <see cref="Task"/>.</returns>
        public static Task WriteAsync(this Stream stream, byte[] array, CancellationToken cancellationToken) => stream.WriteAsync(array, 0, array.Length, cancellationToken);
    }
}
