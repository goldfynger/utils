﻿using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Goldfynger.Utils.Extensions
{
    /// <summary>String extensions.</summary>
    public static class StringExtensions
    {
        /// <summary>Removes all the trailing white-space characters from each line of the current <see cref="string"/>.</summary>
        /// <param name="instance">Input instance of <see cref="string"/>.</param>
        /// <returns><see cref="string"/> without trailing white-space characters of each line or <see langword="null"/> if input instance is <see langword="null"/>.</returns>
        [return: NotNullIfNotNull("instance")]
        public static string? TrimEndMultiline(this string? instance) => instance?.TrimEndMultilineInternal().ToString();


        /// <summary>Removes all the trailing white-space characters from each line of the current <see cref="StringBuilder"/>.</summary>
        /// <param name="instance">Input instance of <see cref="StringBuilder"/>.</param>
        /// <returns><see cref="StringBuilder"/> without trailing white-space characters of each line or <see langword="null"/> if input instance is <see langword="null"/>.</returns>
        [return: NotNullIfNotNull("instance")]
        public static StringBuilder? TrimEndMultiline(this StringBuilder? instance) => instance?.ToString().TrimEndMultilineInternal();


        internal static StringBuilder TrimEndMultilineInternal(this string instance)
        {
            var result = new StringBuilder(instance.Length);
            var nonSpaceIdx = -1;

            for (var idx = 0; idx < instance.Length; idx++)
            {
                var c = instance[idx];

                if (c == '\r' || c == '\n')
                {
                    nonSpaceIdx = idx;
                    result.Append(c);
                }
                else if (c != ' ')
                {
                    if (nonSpaceIdx != idx - 1)
                    {
                        var count = idx - nonSpaceIdx - 1;

                        for (var i = 0; i < count; i++)
                        {
                            result.Append(' ');
                        }
                    }

                    nonSpaceIdx = idx;
                    result.Append(c);
                }
            }

            return result;
        }
    }
}
