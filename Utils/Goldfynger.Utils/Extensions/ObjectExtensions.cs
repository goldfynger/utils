﻿using System;
using System.Runtime.InteropServices;

namespace Goldfynger.Utils.Extensions
{
    /// <summary>Struct extensions.</summary>
    public static class ObjectExtensions
    {
        /// <summary>Allocates <see cref="object"/> of specified value type using <see cref="Array"/> of <see cref="byte"/>.</summary>
        /// <typeparam name="TStruct">Value type.</typeparam>
        /// <param name="array"><see cref="Array"/> of <see cref="byte"/>.</param>
        /// <returns>Allocated <see cref="object"/>.</returns>
        public static TStruct ToStruct<TStruct>(this byte[] array) where TStruct : struct
        {
            TStruct @struct;

            var handle = GCHandle.Alloc(array, GCHandleType.Pinned);

            try
            {
                @struct = Marshal.PtrToStructure<TStruct>(handle.AddrOfPinnedObject());
            }
            finally
            {
                handle.Free();
            }

            return @struct;
        }

        /// <summary>Allocates <see cref="object"/> of specified reference type using <see cref="Array"/> of <see cref="byte"/>.</summary>
        /// <typeparam name="TClass">Reference type.</typeparam>
        /// <param name="array"><see cref="Array"/> of <see cref="byte"/>.</param>
        /// <returns>Allocated <see cref="object"/>.</returns>
        public static TClass ToClass<TClass>(this byte[] array) where TClass : class, new()
        {
            TClass @class;

            var handle = GCHandle.Alloc(array, GCHandleType.Pinned);

            try
            {
                @class = Marshal.PtrToStructure<TClass>(handle.AddrOfPinnedObject()) ?? throw new ApplicationException($"{nameof(Marshal.PtrToStructure)} returned null.");
            }
            finally
            {
                handle.Free();
            }

            return @class;
        }

        /// <summary>Allocates <see cref="Array"/> of <see cref="byte"/> using specified reference or value type <see cref="object"/> .</summary>
        /// <typeparam name="TObject">Reference or value type.</typeparam>
        /// <param name="object"><see cref="object"/> of reference or value type.</param>
        /// <returns>Allocated <see cref="Array"/> of <see cref="byte"/>.</returns>
        public static byte[] ToByteArray<TObject>(this TObject @object) where TObject : notnull
        {
            var array = new byte[Marshal.SizeOf(@object)];

            var handle = GCHandle.Alloc(array, GCHandleType.Pinned);

            try
            {
                Marshal.StructureToPtr(@object, handle.AddrOfPinnedObject(), false);
            }
            finally
            {
                handle.Free();
            }

            return array;
        }
    }
}
