﻿using System;

namespace Goldfynger.Utils.Extensions
{
    /// <summary>Parse extensions.</summary>
    public static class ParserExtensions
    {
        /// <summary>Adds hex symbol '0x' at beginning of input text.</summary>
        /// <param name="text">Input <see cref="string"/>.</param>
        /// <returns>Input <see cref="string"/> with hex header.</returns>
        public static string AddHexHeader(this string text) => $"0x{text}";

        /// <summary>Adds bin symbol '0b' at beginning of input text.</summary>
        /// <param name="text">Input <see cref="string"/>.</param>
        /// <returns>Input <see cref="string"/> with bin header.</returns>
        public static string AddBinHeader(this string text) => $"0b{text}";

        /// <summary>Converts <see cref="sbyte"/> value to binary <see cref="string"/> without binary header (8 characters).</summary>
        /// <param name="value"><see cref="sbyte"/> value.</param>
        /// <returns>Result <see cref="string"/>.</returns>
        public static string ConvertToBinString(this sbyte value)
        {
            var bits = new char[8];
            var i = 0;

            while (i < bits.Length)
            {
                bits[i++] = (value & 1) == 1 ? '1' : '0';
                value >>= 1;
            }

            Array.Reverse(bits, 0, i);
            return new string(bits);
        }

        /// <summary>Converts <see cref="byte"/> value to binary <see cref="string"/> without binary header (8 characters).</summary>
        /// <param name="value"><see cref="byte"/> value.</param>
        /// <returns>Result <see cref="string"/>.</returns>
        public static string ConvertToBinString(this byte value)
        {
            var bits = new char[8];
            var i = 0;

            while (i < bits.Length)
            {
                bits[i++] = (value & 1) == 1 ? '1' : '0';
                value >>= 1;
            }

            Array.Reverse(bits, 0, i);
            return new string(bits);
        }

        /// <summary>Converts <see cref="short"/> value to binary <see cref="string"/> without binary header (16 characters).</summary>
        /// <param name="value"><see cref="short"/> value.</param>
        /// <returns>Result <see cref="string"/>.</returns>
        public static string ConvertToBinString(this short value)
        {
            var bits = new char[16];
            var i = 0;

            while (i < bits.Length)
            {
                bits[i++] = (value & 1) == 1 ? '1' : '0';
                value >>= 1;
            }

            Array.Reverse(bits, 0, i);
            return new string(bits);
        }

        /// <summary>Converts <see cref="ushort"/> value to binary <see cref="string"/> without binary header (16 characters).</summary>
        /// <param name="value"><see cref="ushort"/> value.</param>
        /// <returns>Result <see cref="string"/>.</returns>
        public static string ConvertToBinString(this ushort value)
        {
            var bits = new char[16];
            var i = 0;

            while (i < bits.Length)
            {
                bits[i++] = (value & 1) == 1 ? '1' : '0';
                value >>= 1;
            }

            Array.Reverse(bits, 0, i);
            return new string(bits);
        }

        /// <summary>Converts <see cref="int"/> value to binary <see cref="string"/> without binary header (32 characters).</summary>
        /// <param name="value"><see cref="int"/> value.</param>
        /// <returns>Result <see cref="string"/>.</returns>
        public static string ConvertToBinString(this int value)
        {
            var bits = new char[32];
            var i = 0;

            while (i < bits.Length)
            {
                bits[i++] = (value & 1) == 1 ? '1' : '0';
                value >>= 1;
            }

            Array.Reverse(bits, 0, i);
            return new string(bits);
        }

        /// <summary>Converts <see cref="uint"/> value to binary <see cref="string"/> without binary header (32 characters).</summary>
        /// <param name="value"><see cref="uint"/> value.</param>
        /// <returns>Result <see cref="string"/>.</returns>
        public static string ConvertToBinString(this uint value)
        {
            var bits = new char[32];
            var i = 0;

            while (i < bits.Length)
            {
                bits[i++] = (value & 1) == 1 ? '1' : '0';
                value >>= 1;
            }

            Array.Reverse(bits, 0, i);
            return new string(bits);
        }

        /// <summary>Converts <see cref="long"/> value to binary <see cref="string"/> without binary header (64 characters).</summary>
        /// <param name="value"><see cref="long"/> value.</param>
        /// <returns>Result <see cref="string"/>.</returns>
        public static string ConvertToBinString(this long value)
        {
            var bits = new char[64];
            var i = 0;

            while (i < bits.Length)
            {
                bits[i++] = (value & 1) == 1 ? '1' : '0';
                value >>= 1;
            }

            Array.Reverse(bits, 0, i);
            return new string(bits);
        }

        /// <summary>Converts <see cref="ulong"/> value to binary <see cref="string"/> without binary header (64 characters).</summary>
        /// <param name="value"><see cref="ulong"/> value.</param>
        /// <returns>Result <see cref="string"/>.</returns>
        public static string ConvertToBinString(this ulong value)
        {
            var bits = new char[64];
            var i = 0;

            while (i < bits.Length)
            {
                bits[i++] = (value & 1) == 1 ? '1' : '0';
                value >>= 1;
            }

            Array.Reverse(bits, 0, i);
            return new string(bits);
        }
    }
}
