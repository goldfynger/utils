﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Reflection;

namespace Goldfynger.Utils.Extensions
{
    /// <summary>Contains attribute extensions.</summary>
    public static class AttributeExtensions
    {
        /// <summary>Gets description of property of type using <see cref="DescriptionAttribute"/>.</summary>
        /// <typeparam name="T">Type of property owner.</typeparam>
        /// <param name="object">Property owner instance.</param>
        /// <param name="propertyName">Property name.</param>
        /// <returns><see cref="DescriptionAttribute"/> description.</returns>
        /// <exception cref="InvalidOperationException">Property with specified name not found or has no <see cref="DescriptionAttribute"/>.</exception>
        public static string GetPropertyDescription<T>(this T @object, string propertyName) where T : notnull => GetPropertyDescription(@object.GetType(), propertyName);

        /// <summary>Gets description of property of type using <see cref="DescriptionAttribute"/>.</summary>
        /// <param name="type">Type of property owner.</param>
        /// <param name="propertyName">Property name.</param>
        /// <returns><see cref="DescriptionAttribute"/> description.</returns>
        /// <exception cref="InvalidOperationException">Property with specified name not found or has no <see cref="DescriptionAttribute"/>.</exception>
        public static string GetPropertyDescription(this Type type, string propertyName)
        {
            var propertyInfo = type.GetProperty(propertyName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static) ??
                throw new InvalidOperationException($"Type \"{type.Name}\" has no property with name \"{propertyName}\".");
            var attribute = propertyInfo.GetCustomAttributes(typeof(DescriptionAttribute), false).FirstOrDefault() ??
                throw new InvalidOperationException($"Property \"{propertyName}\" of type \"{type.Name}\" has no \"{nameof(DescriptionAttribute)}\".");

            return ((DescriptionAttribute)attribute).Description;
        }

        /// <summary>Gets description of enum field using <see cref="DescriptionAttribute"/>.</summary>
        /// <typeparam name="TEnum">Type of <see cref="Enum"/>.</typeparam>
        /// <param name="value"><see cref="Enum"/> value.</param>
        /// <returns><see cref="DescriptionAttribute"/> description.</returns>
        /// <exception cref="ArgumentException"><paramref name="value"/> is not defined in target <see cref="Enum"/>.</exception>
        /// <exception cref="InvalidOperationException">Enum has no field with specified name or field has no <see cref="DescriptionAttribute"/>.</exception>
        public static string GetEnumDescription<TEnum>(this TEnum value) where TEnum : Enum
        {
            value.ThrowIfNotDefined();

            var type = value.GetType();
            var fieldInfo = type.GetField(value.ToString()) ??
                throw new InvalidOperationException($"Enum \"{type.Name}\" has no field with name \"{value}\".");
            var attribute = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false).FirstOrDefault() ??
                throw new InvalidOperationException($"Value \"{value}\" of enum \"{type.Name}\" has no \"{nameof(DescriptionAttribute)}\".");

            return ((DescriptionAttribute)attribute).Description;
        }

        /// <summary>Gets description of type using <see cref="DescriptionAttribute"/>.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="object">Object of type.</param>
        /// <returns><see cref="DescriptionAttribute"/> description.</returns>
        public static string GetTypeDescription<T>(this T @object) where T : notnull => GetTypeDescription(@object.GetType());

        /// <summary>Gets description of type using <see cref="DescriptionAttribute"/>.</summary>
        /// <param name="type">Type.</param>
        /// <returns><see cref="DescriptionAttribute"/> description.</returns>
        /// <exception cref="InvalidOperationException">Type has no <see cref="DescriptionAttribute"/>.</exception>
        public static string GetTypeDescription(this Type type)
        {
            var attribute = type.GetCustomAttributes(typeof(DescriptionAttribute), false).FirstOrDefault() ??
                throw new InvalidOperationException($"Type \"{type.Name}\" has no \"{nameof(DescriptionAttribute)}\".");

            return ((DescriptionAttribute)attribute).Description;
        }
    }
}