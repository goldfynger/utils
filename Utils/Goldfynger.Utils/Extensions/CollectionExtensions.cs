﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Goldfynger.Utils.Extensions
{
    /// <summary>Collection extensions.</summary>
    public static class CollectionExtensions
    {
        /// <summary>Clears <see cref="Array"/>.</summary>
        /// <typeparam name="T">Type of <see cref="Array"/> items.</typeparam>
        /// <param name="array"><see cref="Array"/>.</param>
        public static void Clear<T>(this T[] array) => Array.Clear(array, 0, array.Length);


        /// <summary>Copies content of <see cref="IList{T}"/> to new <see cref="Array"/>.</summary>
        /// <typeparam name="T">Type of <see cref="IList{T}"/> items.</typeparam>
        /// <param name="source">Source <see cref="IList{T}"/>.</param>
        /// <returns>Result <see cref="Array"/>.</returns>
        public static T[] Copy<T>(this IList<T> source) => Copy(source, 0, source.Count);

        /// <summary>Copies part of content of <see cref="IList{T}"/> to new <see cref="Array"/> with begin of <see cref="List{T}"/>.</summary>
        /// <typeparam name="T">Type of <see cref="IList{T}"/> items.</typeparam>
        /// <param name="source">Source <see cref="IList{T}"/>.</param>
        /// <param name="count">Count of items to copy from source <see cref="IList{T}"/>.</param>
        /// <returns>Result <see cref="Array"/>.</returns>
        public static T[] Copy<T>(this IList<T> source, int count) => Copy(source, 0, count);

        /// <summary>Copies part of content of <see cref="IList{T}"/> to new <see cref="Array"/>.</summary>
        /// <typeparam name="T">Type of <see cref="IList{T}"/> items.</typeparam>
        /// <param name="source">Source <see cref="IList{T}"/>.</param>
        /// <param name="sourceIndex">Start index at source <see cref="IList{T}"/>.</param>
        /// <param name="count">Count of items to copy from source <see cref="IList{T}"/>.</param>
        /// <returns>Result <see cref="Array"/>.</returns>
        public static T[] Copy<T>(this IList<T> source, int sourceIndex, int count)
        {
            var array = new T[count];

            source.CopyTo(sourceIndex, array, 0, count);

            return array;
        }


        /// <summary>Copies content of source <see cref="IList{T}"/> to destination <see cref="IList{T}"/>.</summary>
        /// <typeparam name="T">Type of <see cref="IList{T}"/> items.</typeparam>
        /// <param name="source">Source <see cref="IList{T}"/>.</param>
        /// <param name="destination">Destination <see cref="IList{T}"/>.</param>
        public static void CopyTo<T>(this IList<T> source, IList<T> destination) => CopyTo(source, 0, destination, 0, source.Count);

        /// <summary>Copies content of source <see cref="IList{T}"/> to destination <see cref="IList{T}"/>.</summary>
        /// <typeparam name="T">Type of <see cref="IList{T}"/> items.</typeparam>
        /// <param name="source">Source <see cref="IList{T}"/>.</param>
        /// <param name="destination">Destination <see cref="IList{T}"/>.</param>
        /// <param name="count">Count of items to copy from source <see cref="IList{T}"/> to destination <see cref="IList{T}"/>.</param>
        public static void CopyTo<T>(this IList<T> source, IList<T> destination, int count) => CopyTo(source, 0, destination, 0, count);

        /// <summary>Copies content of source <see cref="IList{T}"/> to destination <see cref="IList{T}"/>.</summary>
        /// <typeparam name="T">Type of <see cref="IList{T}"/> items.</typeparam>
        /// <param name="source">Source <see cref="IList{T}"/>.</param>
        /// <param name="sourceIndex">Start index at source <see cref="IList{T}"/>.</param>
        /// <param name="destination">Destination <see cref="IList{T}"/>.</param>
        /// <param name="destinationIndex">Start index at destination <see cref="IList{T}"/>.</param>
        /// <param name="count">Count of items to copy from source <see cref="IList{T}"/> to destination <see cref="IList{T}"/>.</param>
        public static void CopyTo<T>(this IList<T> source, int sourceIndex, IList<T> destination, int destinationIndex, int count)
        {
            if (source is byte[] sourceArray && destination is byte[] destinationArray)
            {
                Array.Copy(sourceArray, sourceIndex, destinationArray, destinationIndex, count);
            }
            else
            {
                for (var i = 0; i < count; i++)
                {
                    destination[i + destinationIndex] = source[i + sourceIndex];
                }
            }
        }


        /// <summary>Converts <see cref="IEnumerable{T}"/> to <see cref="string"/> with specified separator and optional converter.</summary>
        /// <typeparam name="T">Type of items in <see cref="IEnumerable{T}"/>.</typeparam>
        /// <param name="collection"><see cref="IEnumerable{T}"/>.</param>
        /// <param name="separator"><see cref="string"/> separator between items in result <see cref="string"/>.</param>
        /// <param name="converter">Converter that converts each item to <see cref="string"/>; if converter is <see langword="null"/> default <see cref="object.ToString()"/> conversion used.</param>
        /// <returns>Input collection <see cref="string"/> represenatation.</returns>
        public static string ConvertToStringWithSeparator<T>(this IEnumerable<T> collection, string separator = " ", Func<T, string>? converter = null)
        {
            return string.Join(separator, collection.Select(t => converter == null ? t?.ToString() : converter(t)));
        }


        /// <summary>Performs the specified action on each element of the <see cref="IEnumerable{T}"/>.</summary>
        /// <typeparam name="T">Type of items in <see cref="IEnumerable{T}"/>.</typeparam>
        /// <param name="collection"><see cref="IEnumerable{T}"/>.</param>
        /// <param name="action">The <see cref="Action{T}"/> delegate to perform on each element of the <see cref="IEnumerable{T}"/>.</param>
        /// <exception cref="ArgumentNullException">collection is <see langword="null"/> -or- action is <see langword="null"/>.</exception>
        public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            foreach (var item in collection)
            {
                action(item);
            }
        }

        /// <summary>Casts all elements of <see cref="IEnumerable"/> to specified type and performs the specified action on each element of the <see cref="IEnumerable"/>.</summary>
        /// <typeparam name="T">Type to cast items in <see cref="IEnumerable"/>.</typeparam>
        /// <param name="collection"><see cref="IEnumerable"/>.</param>
        /// <param name="action">The <see cref="Action{T}"/> delegate to perform on each element of the <see cref="IEnumerable"/>.</param>
        /// <exception cref="ArgumentNullException">collection is <see langword="null"/> -or- action is <see langword="null"/>.</exception>
        public static void ForEach<T>(this IEnumerable collection, Action<T> action)
        {
            foreach (var item in collection.Cast<T>())
            {
                action(item);
            }
        }

        /// <summary>Performs the specified action on each element of the <see cref="IEnumerable"/>.</summary>
        /// <param name="collection"><see cref="IEnumerable"/>.</param>
        /// <param name="action">The <see cref="Action"/> delegate to perform on each element of the <see cref="IEnumerable"/>.</param>
        /// <exception cref="ArgumentNullException">collection is <see langword="null"/> -or- action is <see langword="null"/>.</exception>
        public static void ForEach(this IEnumerable collection, Action<object> action)
        {
            foreach (var item in collection)
            {
                action(item);
            }
        }
    }
}