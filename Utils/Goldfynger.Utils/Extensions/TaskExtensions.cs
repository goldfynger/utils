﻿using System;
using System.Threading.Tasks;

namespace Goldfynger.Utils.Extensions
{
    /// <summary><see cref="Task"/> extensions.</summary>
    public static class TaskExtensions
    {
        /// <summary>Repeat func any times.</summary>
        /// <param name="func"><see cref="Func{TResult}"/> of <see cref="Task"/> to repeat.</param>
        /// <param name="times">Repeat times.</param>
        /// <returns><see cref="Task"/>.</returns>
        public static async Task Repeat(this Func<Task> func, int times)
        {
            if (func == null) throw new ArgumentNullException(nameof(func));
            if (times < 0) throw new ArgumentOutOfRangeException(nameof(times));

            for (var i = 0; i < times; i++)
            {
                await func();
            }
        }

        /// <summary>Runs task as <see langword="async void"/> and calls <paramref name="onFault"/> if task faulted.</summary>
        /// <param name="task"><see cref="Task"/> to run.</param>
        /// <param name="onFault">Callback that called if <paramref name="task"/> is faulted.</param>
        public static async void FireAndForgetAsync(this Task task, Action<Exception>? onFault = null)
        {
            try
            {
                await task;
            }
            catch (Exception ex)
            {
                onFault?.Invoke(ex);
            }
        }     
    }
}
