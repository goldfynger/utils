﻿using System;

namespace Goldfynger.Utils.Extensions
{
    /// <summary>
    /// Extensions that allow use <see cref="long"/> instead of <see cref="double"/> when working with <see cref="TimeSpan"/> for more precision.
    /// </summary>
    public static class TimeSpanExtensions
    {
        /// <summary>
        /// Gets the value of the current TimeSpan structure expressed in whole days.
        /// </summary>
        /// <param name="instance"><see cref="TimeSpan"/> instance.</param>
        /// <returns>The total number of whole days represented by this instance.</returns>
        /// <remarks>Can be safely converted to an <see cref="int"/>.</remarks>
        public static long GetWholeDays(this TimeSpan instance) => instance.Ticks / TimeSpan.TicksPerDay;

        /// <summary>
        /// Gets the value of the current TimeSpan structure expressed in whole hours.
        /// </summary>
        /// <param name="instance"><see cref="TimeSpan"/> instance.</param>
        /// <returns>The total number of whole hours represented by this instance.</returns>
        /// <remarks>Can be safely converted to an <see cref="int"/>.</remarks>
        public static long GetWholeHours(this TimeSpan instance) => instance.Ticks / TimeSpan.TicksPerHour;

        /// <summary>
        /// Gets the value of the current TimeSpan structure expressed in whole minutes.
        /// </summary>
        /// <param name="instance"><see cref="TimeSpan"/> instance.</param>
        /// <returns>The total number of whole minutes represented by this instance.</returns>
        public static long GetWholeMinutes(this TimeSpan instance) => instance.Ticks / TimeSpan.TicksPerMinute;

        /// <summary>
        /// Gets the value of the current TimeSpan structure expressed in whole seconds.
        /// </summary>
        /// <param name="instance"><see cref="TimeSpan"/> instance.</param>
        /// <returns>The total number of whole seconds represented by this instance.</returns>
        public static long GetWholeSeconds(this TimeSpan instance) => instance.Ticks / TimeSpan.TicksPerSecond;

        /// <summary>
        /// Gets the value of the current TimeSpan structure expressed in whole milliseconds.
        /// </summary>
        /// <param name="instance"><see cref="TimeSpan"/> instance.</param>
        /// <returns>The total number of whole milliseconds represented by this instance.</returns>
        public static long GetWholeMilliseconds(this TimeSpan instance) => instance.Ticks / TimeSpan.TicksPerMillisecond;

        /// <summary>
        /// Returns a <see cref="TimeSpan"/> that represents a specified number of days.
        /// </summary>
        /// <param name="value">A number of days.</param>
        /// <returns>An <see cref="TimeSpan"/> that represents value.</returns>
        public static TimeSpan FromDays(this long value) => TimeSpan.FromTicks(value * TimeSpan.TicksPerDay);

        /// <summary>
        /// Returns a <see cref="TimeSpan"/> that represents a specified number of hours.
        /// </summary>
        /// <param name="value">A number of hours.</param>
        /// <returns>An <see cref="TimeSpan"/> that represents value.</returns>
        public static TimeSpan FromHours(this long value) => TimeSpan.FromTicks(value * TimeSpan.TicksPerHour);

        /// <summary>
        /// Returns a <see cref="TimeSpan"/> that represents a specified number of minutes.
        /// </summary>
        /// <param name="value">A number of minutes.</param>
        /// <returns>An <see cref="TimeSpan"/> that represents value.</returns>
        public static TimeSpan FromMinutes(this long value) => TimeSpan.FromTicks(value * TimeSpan.TicksPerMinute);

        /// <summary>
        /// Returns a <see cref="TimeSpan"/> that represents a specified number of seconds.
        /// </summary>
        /// <param name="value">A number of seconds.</param>
        /// <returns>An <see cref="TimeSpan"/> that represents value.</returns>
        public static TimeSpan FromSeconds(this long value) => TimeSpan.FromTicks(value * TimeSpan.TicksPerSecond);

        /// <summary>
        /// Returns a <see cref="TimeSpan"/> that represents a specified number of milliseconds.
        /// </summary>
        /// <param name="value">A number of milliseconds.</param>
        /// <returns>An <see cref="TimeSpan"/> that represents value.</returns>
        public static TimeSpan FromMilliseconds(this long value) => TimeSpan.FromTicks(value * TimeSpan.TicksPerMillisecond);
    }
}
