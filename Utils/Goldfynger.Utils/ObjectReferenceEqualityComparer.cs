﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Goldfynger.Utils
{
    /// <summary>A generic object comparerer that would only use object's reference, ignoring any <see cref="System.IEquatable{T}"/> or <see cref="object.Equals(object)"/> overrides.</summary>
    public class ObjectReferenceEqualityComparer<T> : EqualityComparer<T> where T : class
    {
        /// <summary>Instance of comparer.</summary>
        public new static IEqualityComparer<T> Default { get; } = new ObjectReferenceEqualityComparer<T>();


        /// <summary>When overridden in a derived class, determines whether two objects of type T are equal.</summary>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare.</param>
        /// <returns><see langword="true"/> if the specified objects are equal; otherwise, <see langword="false"/>.</returns>
        public override bool Equals(T? x, T? y) => ReferenceEquals(x, y);

        /// <summary>When overridden in a derived class, serves as a hash function for the specified object for hashing algorithms and data structures, such as a hash table.</summary>
        /// <param name="obj">The object for which to get a hash code.</param>
        /// <returns>A hash code for the specified object.</returns>
        public override int GetHashCode(T obj) => RuntimeHelpers.GetHashCode(obj);
    }
}
