﻿using System;

namespace Goldfynger.Utils.DataContainers
{
    /// <summary></summary>
    [Obsolete("Locker is obsolete. There is no known application that uses it.", true)]
    public sealed class Locker
    {
        /// <summary></summary>
        public bool Flag { get; set; } = false;
    }
}