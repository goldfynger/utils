﻿using System;
using System.Diagnostics;

namespace Goldfynger.Utils.DataContainers
{
    /// <summary>Container that can be used for transmit any data over through event.</summary>
    /// <typeparam name="T">Type of item in <see cref="DataEventArgs{T}"/>.</typeparam>
    [DebuggerDisplay("Data = {Data}")]
    public class DataEventArgs<T> : EventArgs where T : notnull
    {
        /// <summary>Creates new instance of <see cref="DataEventArgs{T}"/> container with specified data.</summary>
        /// <param name="data">Contained data.</param>
        public DataEventArgs(T data) => Data = data;

        /// <summary>Contained data.</summary>
        public T Data { get; }
    }
}