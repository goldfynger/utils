﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Goldfynger.Utils.DataContainers
{
    /// <summary></summary>
    [Obsolete("NotifyContainer is obsolete. There is no known application that uses it.", true)]
    public abstract class NotifyContainer : INotifyPropertyChanged
    {
        /// <summary></summary>
        public event PropertyChangedEventHandler? PropertyChanged;

        /// <summary></summary>
        /// <param name="propertyName"></param>
        protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        /// <summary>Set field by <see langword="ref"/> and call <see cref="OnPropertyChanged"/> function.</summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        protected bool SetField<T>(ref T? field, T? value, [CallerMemberName]string? propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }
    }
}