﻿using System.Diagnostics;

namespace Goldfynger.Utils.DataContainers
{
    /// <summary>Container for storing values before and after value change.</summary>
    /// <typeparam name="T">Type of values in <see cref="ChangePair{T}"/>.</typeparam>
    [DebuggerDisplay("OldValue = {OldValue} NewValue = {NewValue}")]
    public record ChangePair<T>
    {
        /// <summary>Initialize new <see cref="ChangePair{T}"/> container.</summary>
        /// <param name="oldValue">Old value.</param>
        /// <param name="newValue">New value.</param>
        public ChangePair(T? oldValue, T? newValue)
        {
            OldValue = oldValue;
            NewValue = newValue;
        }

        /// <summary>Old value.</summary>
        public T? OldValue { get; }
        /// <summary>New value.</summary>
        public T? NewValue { get; }
    }
}