﻿using System;

namespace Goldfynger.Utils
{
    /// <summary>Base type for difference singleton types.</summary>
    /// <typeparam name="T">Type of singleton.</typeparam>
    public abstract class SingletonBase<T> where T : SingletonBase<T>, new()
    {
        private static readonly Lazy<T> __lazyInstance = new(() => new T());

        private static string? __alreadyCreatedStackTrace = null;
        private static readonly object __alreadyCreatedStackTraceLocker = new();


        /// <summary>Creates new singleton instance. Must be called only once.</summary>
        public SingletonBase()
        {
            lock (__alreadyCreatedStackTraceLocker)
            {
                if (__alreadyCreatedStackTrace != null)
                {
                    throw new InvalidOperationException($"Instance of {GetType()} was already instantiated here:{Environment.NewLine}{__alreadyCreatedStackTrace}");
                }
                else
                {
                    __alreadyCreatedStackTrace = Environment.StackTrace;
                }
            }
        }


        /// <summary>Singleton instance.</summary>
        public static T Instance => __lazyInstance.Value;
    }
}
