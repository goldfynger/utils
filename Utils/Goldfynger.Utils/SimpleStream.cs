﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Goldfynger.Utils
{
    /// <summary></summary>
    [Obsolete("SimpleStream is obsolete. There is no known application that uses it.", true)]
    public sealed class SimpleStream : Stream
    {
        /// <summary></summary>
        private readonly Stream _innerStream;

        /// <summary></summary>
        private readonly ConcurrentQueue<byte>? _queue;


        /// <summary></summary>
        public override bool CanRead => _innerStream.CanRead;

        /// <summary></summary>
        public override bool CanSeek => false;

        /// <summary></summary>
        public override bool CanWrite => _innerStream.CanWrite;

        /// <summary></summary>
        public override long Length => ThrowNotSupportedException<long>();

        /// <summary></summary>
        public override long Position
        {
            get { return ThrowNotSupportedException<long>(); }
            set { ThrowNotSupportedException(); }
        }

        /// <summary></summary>
        /// <param name="innerStream"></param>
        public SimpleStream(Stream innerStream)
        {
            _innerStream = innerStream ?? throw new ArgumentNullException(nameof(innerStream));

            if (_innerStream.CanRead)
            {
                _queue = new ConcurrentQueue<byte>();

                Task.Run(() =>
                {
                    try
                    {
                        while (true)
                        {
                            byte[] array = new byte[16];

                            var count = innerStream.Read(array);

                            for (var i = 0; i < count; i++) _queue.Enqueue(array[i]);
                        }
                    }
                    catch
                    {
                    }
                });
            }
        }

        /// <summary></summary>
        [DoesNotReturn]
        private void ThrowNotSupportedException()
        {
            throw new NotSupportedException("Supports only Write() and ReadWithTimeout() operations.");
        }

        /// <summary>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private T ThrowNotSupportedException<T>()
        {
            ThrowNotSupportedException();
            return default;
        }

        /// <summary></summary>
        public override void Flush() => ThrowNotSupportedException();

        /// <summary></summary>
        /// <param name="offset"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        public override long Seek(long offset, SeekOrigin origin) => ThrowNotSupportedException<long>();

        /// <summary></summary>
        /// <param name="value"></param>
        public override void SetLength(long value) => ThrowNotSupportedException();

        /// <summary></summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public override int Read(byte[] buffer, int offset, int count) => ReadWithTimeout(buffer, offset, count, TimeSpan.FromMilliseconds(int.MaxValue), CancellationToken.None);

        /// <summary></summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken) =>
            Task.Run(() => ReadWithTimeout(buffer, offset, count, TimeSpan.FromMilliseconds(int.MaxValue), cancellationToken));

        /// <summary></summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        /// <param name="timeout"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private int ReadWithTimeout(byte[] buffer, int offset, int count, TimeSpan timeout, CancellationToken cancellationToken)
        {
            if (offset < 0) throw new ArgumentOutOfRangeException(nameof(offset));
            if (count <= 0) throw new ArgumentOutOfRangeException(nameof(count));
            if (buffer.Length < offset + count) throw new ArgumentOutOfRangeException(nameof(buffer));

            if (!_innerStream.CanRead) throw new NotSupportedException("Reading not supported");

            var timeoutFlag = false;
            var timeoutFlagLocker = new object();
            var readedCount = 0;

            using (var timer = new System.Timers.Timer(timeout.TotalMilliseconds))
            {
                timer.Elapsed += (sender, e) =>
                {
                    lock (timeoutFlagLocker)
                    {
                        timeoutFlag = true;
                    }
                };
                timer.AutoReset = false;

                while (!cancellationToken.IsCancellationRequested)
                {
                    lock (timeoutFlagLocker)
                    {
                        if (timeoutFlag)
                        {
                            break;
                        }
                    }

                    if (_queue!.TryDequeue(out byte readedByte))
                    {
                        if (timer.Enabled) timer.Stop();

                        buffer[offset + readedCount++] = readedByte;

                        if (readedCount == count) break;
                    }
                    else
                    {
                        if (timeout == TimeSpan.Zero) break;

                        if (readedCount > 0) break;

                        if (!timer.Enabled) /* Timer not started yet or already fired. */
                        {
                            bool timeoutExpired = true;
                            lock (timeoutFlagLocker)
                            {
                                timeoutExpired = timeoutFlag;
                            }

                            if (!timeoutExpired) /* PVS studio warning here - flag can be set asynchronously in timer event. */
                            {
                                timer.Start();
                            }
                        }

                        Thread.Sleep(1);
                    }
                }
            }

            return readedCount;
        }

        /// <summary></summary>
        /// <param name="buffer"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public int ReadWithTimeout(byte[] buffer, TimeSpan timeout) => ReadWithTimeout(buffer, 0, buffer.Length, timeout, CancellationToken.None);

        /// <summary></summary>
        /// <param name="buffer"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public Task<int> ReadWithTimeoutAsync(byte[] buffer, TimeSpan timeout) => Task.Run(() => ReadWithTimeout(buffer, 0, buffer.Length, timeout, CancellationToken.None));

        /// <summary></summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        public override void Write(byte[] buffer, int offset, int count) => _innerStream.Write(buffer, offset, count);

        /// <summary></summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken) => _innerStream.WriteAsync(buffer, offset, count, cancellationToken);
    }
}
