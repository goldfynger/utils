﻿using Goldfynger.Utils.Extensions;

namespace Goldfynger.Utils.Parser
{
    /// <summary>Container that contains string parse results.</summary>
    public sealed record ParseValueContainer
    {
        private readonly ulong? _value;
        private readonly string? _string;


        /// <summary>Creates new instance of <see cref="ParseValueContainer"/> that contains <see cref="ValueTypes.U8"/> value.</summary>
        /// <param name="value">Contained value.</param>
        public ParseValueContainer(byte value)
        {
            _value = value;
            ValueType = ValueTypes.U8;
        }

        /// <summary>Creates new instance of <see cref="ParseValueContainer"/> that contains <see cref="ValueTypes.U16"/> value.</summary>
        /// <param name="value">Contained value.</param>
        public ParseValueContainer(ushort value)
        {
            _value = value;
            ValueType = ValueTypes.U16;
        }

        /// <summary>Creates new instance of <see cref="ParseValueContainer"/> that contains <see cref="ValueTypes.U32"/> value.</summary>
        /// <param name="value">Contained value.</param>
        public ParseValueContainer(uint value)
        {
            _value = value;
            ValueType = ValueTypes.U32;
        }

        /// <summary>Creates new instance of <see cref="ParseValueContainer"/> that contains <see cref="ValueTypes.U64"/> value.</summary>
        /// <param name="value">Contained value.</param>
        public ParseValueContainer(ulong value)
        {
            _value = value;
            ValueType = ValueTypes.U64;
        }

        /// <summary>Creates new instance of <see cref="ParseValueContainer"/> that contains <see cref="ValueTypes.String"/> value.</summary>
        /// <param name="str">Contained <see cref="string"/>.</param>
        public ParseValueContainer(string str)
        {
            _string = str;
            ValueType = ValueTypes.String;
        }


        /// <summary><see cref="byte"/> value or <see langword="null"/> if contained digital value is larger than <see cref="ValueTypes.U8"/> or <see cref="ValueTypes.String"/>.</summary>
        public byte? U8Value
        {
            get
            {
                if (ValueType == ValueTypes.U8)
                {
                    return (byte?)_value;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary><see cref="ushort"/> value or <see langword="null"/> if contained digital value is larger than <see cref="ValueTypes.U16"/> or <see cref="ValueTypes.String"/>.</summary>
        public ushort? U16Value
        {
            get
            {
                if (ValueType == ValueTypes.U8 || ValueType == ValueTypes.U16)
                {
                    return (ushort?)_value;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary><see cref="uint"/> value or <see langword="null"/> if contained digital value is larger than <see cref="ValueTypes.U32"/> or <see cref="ValueTypes.String"/>.</summary>
        public uint? U32Value
        {
            get
            {
                if (ValueType == ValueTypes.U8 || ValueType == ValueTypes.U16 || ValueType == ValueTypes.U32)
                {
                    return (uint?)_value;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary><see cref="ulong"/> value or <see langword="null"/> if contained value is <see cref="ValueTypes.String"/>.</summary>
        public ulong? U64Value
        {
            get
            {
                if (ValueType == ValueTypes.U8 || ValueType == ValueTypes.U16 || ValueType == ValueTypes.U32 || ValueType == ValueTypes.U64)
                {
                    return _value;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary><see cref="string"/> value or <see langword="null"/> if contained value is not <see cref="ValueTypes.String"/>.</summary>
        public string? String
        {
            get
            {
                if (ValueType == ValueTypes.String)
                {
                    return _string;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>Specified <see cref="ValueTypes"/> of contained item.</summary>
        public ValueTypes ValueType { get; }


        /// <summary>Returns a string that represents the current <see cref="ParseValueContainer"/>.</summary>
        /// <returns>A string that represents the current <see cref="ParseValueContainer"/>.</returns>
        public override string ToString()
        {
            return ValueType switch
            {
                ValueTypes.U8 => _value!.Value.ToString("X2").AddHexHeader(),
                ValueTypes.U16 => _value!.Value.ToString("X4").AddHexHeader(),
                ValueTypes.U32 => _value!.Value.ToString("X8").AddHexHeader(),
                ValueTypes.U64 => _value!.Value.ToString("X16").AddHexHeader(),
                ValueTypes.String => $"\"{_string}\"",
                _ => "Unknown value type",
            };
        }


        /// <summary>Container value types.</summary>
        public enum ValueTypes
        {
            /// <summary>Unsigned 8-bit value.</summary>
            U8,
            /// <summary>Unsigned 16-bit value.</summary>
            U16,
            /// <summary>Unsigned 32-bit value.</summary>
            U32,
            /// <summary>Unsigned 64-bit value.</summary>
            U64,
            /// <summary>String value.</summary>
            String,
        }
    }
}
