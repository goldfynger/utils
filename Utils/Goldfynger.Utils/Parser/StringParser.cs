﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;

namespace Goldfynger.Utils.Parser
{
    /// <summary>Parser that used to parse specified <see cref="string"/> to group of <see cref="ParseValueContainer"/> items.</summary>
    public sealed class StringParser
    {
        private static readonly ReadOnlyCollection<ParseValueContainer> __empty = new(Array.Empty<ParseValueContainer>());

        private static readonly ReadOnlyCollection<string> __binStartSymbolsDefault = new(new[] { "b", "0b", "B", "0B" });
        private static readonly ReadOnlyCollection<string> __hexStartSymbolsDefault = new(new[] { "x", "0x", "X", "0X" });
        private static readonly ReadOnlyCollection<char> __substringSplitSymbolsDefault = new(new[] { ' ', ',', '\r', '\n' });


        /// <summary>Creates new <see cref="StringParser"/> instanse with specified <see cref="AllowedInputs"/> flags.</summary>
        /// <param name="allowedInput"><see cref="AllowedInputs"/> flags.</param>
        public StringParser(AllowedInputs allowedInput = AllowedInputs.String | AllowedInputs.Binary | AllowedInputs.Decimal | AllowedInputs.Hexadecimal)
        {
            AllowedInput = allowedInput;
        }


        /// <summary><see cref="AllowedInputs"/> flags.</summary>
        public AllowedInputs AllowedInput { get; }


        /// <summary>Parse input <see cref="string"/> to <see cref="ReadOnlyCollection{T}"/> of <see cref="ParseValueContainer"/> items.</summary>
        /// <param name="input">Input <see cref="string"/>.</param>
        /// <returns><see cref="ReadOnlyCollection{T}"/> of <see cref="ParseValueContainer"/> items. Result collection is empty if parsing failed.</returns>
        public ReadOnlyCollection<ParseValueContainer> Parse(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                Debug.WriteLine("Parser input is null or white space.");

                return __empty;
            }

            var split = input.Split(__substringSplitSymbolsDefault.ToArray(), StringSplitOptions.RemoveEmptyEntries);
            var array = new ParseValueContainer[split.Length];
            var index = 0;

            foreach (var str in split)
            {
                ParseValueContainer? value = null;

                if (AllowedInput.HasFlag(AllowedInputs.Binary))
                {
                    foreach (var symbol in __binStartSymbolsDefault)
                    {
                        if (str.StartsWith(symbol))
                        {
                            value = StringToParseValueConverter.ConvertFromBin(str.Split(symbol, 2)[1]);
                            if (value != null)
                            {
                                break;
                            }
                        }
                    }
                }

                if (value == null && AllowedInput.HasFlag(AllowedInputs.Hexadecimal))
                {
                    foreach (var symbol in __hexStartSymbolsDefault)
                    {
                        if (str.StartsWith(symbol))
                        {
                            value = StringToParseValueConverter.ConvertFromHex(str.Split(symbol, 2)[1]);
                            if (value != null)
                            {
                                break;
                            }
                        }
                    }
                }

                if (value == null && AllowedInput.HasFlag(AllowedInputs.Decimal))
                {
                    value = StringToParseValueConverter.ConvertFromDec(str);
                }

                if (value == null && AllowedInput.HasFlag(AllowedInputs.String))
                {
                    value = new ParseValueContainer(str);
                }

                if (value != null)
                {
                    array[index++] = value;

                    Debug.WriteLine($"Substring \"{str}\" converted to \"{value.ValueType}\": \"{value}\".");
                }
                else
                {
                    Debug.WriteLine($"Substring \"{str}\" conversion failed.");

                    return __empty;
                }
            }

            return new ReadOnlyCollection<ParseValueContainer>(array);
        }


        /// <summary>Allowed input types.</summary>
        [Flags]
        public enum AllowedInputs
        {
            /// <summary>No type of input allowed.</summary>
            None = 0x00,
            /// <summary>Allow parse string as string.</summary>
            String = 0x01,
            /// <summary>Allow parse string as binary.</summary>
            Binary = 0x02,
            /// <summary>Allow parse string as decimal.</summary>
            Decimal = 0x04,
            /// <summary>Allow parse string as hexadecimal.</summary>
            Hexadecimal = 0x08,
        }
    }
}
