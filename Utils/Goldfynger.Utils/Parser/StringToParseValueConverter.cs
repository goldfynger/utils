﻿using System;
using System.Linq;

namespace Goldfynger.Utils.Parser
{
    /// <summary>Contains <see cref="string"/> to <see cref="ParseValueContainer"/> convertor functions.</summary>
    public static class StringToParseValueConverter
    {
        private static readonly char[] __knownBinDigits = { '0', '1' };
        private static readonly char[] __knownDecDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        private static readonly char[] __knownHexDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

        private const int __bin8MaxLen = 8;
        private const int __bin16MaxLen = 16;
        private const int __bin32MaxLen = 32;
        private const int __bin64MaxLen = 64;

        private const int __hex8MaxLen = 2;
        private const int __hex16MaxLen = 4;
        private const int __hex32MaxLen = 8;
        private const int __hex64MaxLen = 16;


        /// <summary>Converts binary <see cref="string"/> to <see cref="ParseValueContainer"/>.</summary>
        /// <param name="text"><see cref="string"/> in binary format where '11000011' is equal to decimal '195' value.</param>
        /// <returns><see cref="ParseValueContainer"/> if conversion is successful; otherwise <see langword="null"/>.</returns>
        public static ParseValueContainer? ConvertFromBin(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return null;
            }

            foreach (var ch in text)
            {
                if (!__knownBinDigits.Contains(ch))
                {
                    return null;
                }
            }

            if (text.Length <= __bin8MaxLen)
            {
                return new ParseValueContainer(Convert.ToByte(text, 2));
            }
            else if (text.Length <= __bin16MaxLen)
            {
                return new ParseValueContainer(Convert.ToUInt16(text, 2));
            }
            else if (text.Length <= __bin32MaxLen)
            {
                return new ParseValueContainer(Convert.ToUInt32(text, 2));
            }
            else if (text.Length <= __bin64MaxLen)
            {
                return new ParseValueContainer(Convert.ToUInt64(text, 2));
            }
            else
            {
                return null;
            }
        }

        /// <summary>Converts positive or negative decimal <see cref="string"/> to <see cref="ParseValueContainer"/>.</summary>
        /// <param name="text"><see cref="string"/> in decimal format where '-195' is negative '195' value.</param>
        /// <returns><see cref="ParseValueContainer"/> if conversion is successful; otherwise <see langword="null"/>.</returns>
        public static ParseValueContainer? ConvertFromDec(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return null;
            }

            var isNegative = false;
            var positive = text;

            if (text.StartsWith('-'))
            {
                isNegative = true;
                positive = text.Split('-', 2)[1];
            }

            if (string.IsNullOrWhiteSpace(positive))
            {
                return null;
            }

            foreach (var ch in positive)
            {
                if (!__knownDecDigits.Contains(ch))
                {
                    return null;
                }
            }

            if (isNegative)
            {
                if (long.TryParse(text, out long result))
                {
                    if (result >= sbyte.MinValue)
                    {
                        unchecked
                        {
                            return new ParseValueContainer((byte)result);
                        }
                    }
                    else if (result >= short.MinValue)
                    {
                        unchecked
                        {
                            return new ParseValueContainer((ushort)result);
                        }
                    }
                    else if (result >= int.MinValue)
                    {
                        unchecked
                        {
                            return new ParseValueContainer((uint)result);
                        }
                    }
                    else
                    {
                        unchecked
                        {
                            return new ParseValueContainer((ulong)result);
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                if (ulong.TryParse(text, out ulong result))
                {
                    if (result <= byte.MaxValue)
                    {
                        return new ParseValueContainer((byte)result);
                    }
                    else if (result <= ushort.MaxValue)
                    {
                        return new ParseValueContainer((ushort)result);
                    }
                    else if (result <= uint.MaxValue)
                    {
                        return new ParseValueContainer((uint)result);
                    }
                    else
                    {
                        return new ParseValueContainer(result);
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>Converts binary <see cref="string"/> to <see cref="ParseValueContainer"/>.</summary>
        /// <param name="text"><see cref="string"/> in binary format where 'C3' is equal to decimal '195' value.</param>
        /// <returns><see cref="ParseValueContainer"/> if conversion is successful; otherwise <see langword="null"/>.</returns>
        public static ParseValueContainer? ConvertFromHex(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return null;
            }

            var textLower = text.ToLowerInvariant();

            foreach (var ch in textLower)
            {
                if (!__knownHexDigits.Contains(ch))
                {
                    return null;
                }
            }

            if (text.Length <= __hex8MaxLen)
            {
                return new ParseValueContainer(Convert.ToByte(text, 16));
            }
            else if (text.Length <= __hex16MaxLen)
            {
                return new ParseValueContainer(Convert.ToUInt16(text, 16));
            }
            else if (text.Length <= __hex32MaxLen)
            {
                return new ParseValueContainer(Convert.ToUInt32(text, 16));
            }
            else if (text.Length <= __hex64MaxLen)
            {
                return new ParseValueContainer(Convert.ToUInt64(text, 16));
            }
            else
            {
                return null;
            }
        }
    }
}
