﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;

namespace Goldfynger.Utils.Collections
{
    /// <summary>Represents a thread-safe first in-first out (FIFO) collection with asynchronous access.</summary>
    [DebuggerDisplay("Count = {_queue.Count}")]
    public sealed class AsyncConcurrentQueue<T> : IProducerConsumerCollection<T>, IEnumerable<T>, IEnumerable, ICollection, IReadOnlyCollection<T>, IDisposable where T : notnull
    {
        private readonly ConcurrentQueue<T> _queue;

        private readonly SemaphoreSlim _semaphore = new(0);


        /// <summary>Initializes a new instance of the <see cref="AsyncConcurrentQueue{T}"/> class.</summary>
        public AsyncConcurrentQueue() => _queue = new ConcurrentQueue<T>();

        /// <summary>Initializes a new instance of the <see cref="AsyncConcurrentQueue{T}"/> class that contains elements copied from the specified collection.</summary>
        /// <param name="collection">The collection whose elements are copied to the new <see cref="AsyncConcurrentQueue{T}"/>.</param>
        /// <exception cref="ArgumentNullException">The collection argument is <see langword="null"/>.</exception>
        public AsyncConcurrentQueue(IEnumerable<T> collection) => _queue = new ConcurrentQueue<T>(collection);


        /// <summary>Gets a value that indicates whether the <see cref="AsyncConcurrentQueue{T}"/> is empty.</summary>
        /// <returns><see langword="true"/> if the <see cref="AsyncConcurrentQueue{T}"/> is empty; otherwise, <see langword="false"/>.</returns>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        public bool IsEmpty
        {
            get
            {
                CheckDispose();

                return _queue.IsEmpty;
            }
        }


        /// <summary>Adds an object to the end of <see cref="AsyncConcurrentQueue{T}"/>.</summary>
        /// <param name="item">The object to add to the end of the <see cref="AsyncConcurrentQueue{T}"/>.
        /// The value can be a <see langword="null"/> reference (<see langword="Nothing"/> in Visual Basic) for reference types.</param>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        public void Enqueue(T item)
        {
            CheckDispose();

            _queue.Enqueue(item);
            _semaphore.Release();
        }

        /// <summary>Asynchronously waits to remove and return the object at the beginning of the <see cref="AsyncConcurrentQueue{T}"/>.</summary>
        /// <returns>The object that was removed.</returns>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        public Task<T> DequeueAsync() => DequeueAsync(Timeout.InfiniteTimeSpan, default);

        /// <summary>Asynchronously waits to remove and return the object at the beginning of the <see cref="AsyncConcurrentQueue{T}"/>, while observing a <see cref="CancellationToken"/>.</summary>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> to observe.</param>
        /// <returns>The object that was removed.</returns>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        /// <exception cref="OperationCanceledException">cancellationToken was canceled.</exception>
        public Task<T> DequeueAsync(CancellationToken cancellationToken) => DequeueAsync(Timeout.InfiniteTimeSpan, cancellationToken);

        /// <summary>Asynchronously waits to remove and return the object at the beginning of the <see cref="AsyncConcurrentQueue{T}"/>,
        /// using a <see cref="TimeSpan"/> to measure the time interval.</summary>
        /// <param name="timeout">A <see cref="TimeSpan"/> that represents the number of milliseconds to wait, a <see cref="Timeout.InfiniteTimeSpan"/> that represents -1 milliseconds to wait indefinitely,
        /// or a <see cref="TimeSpan"/> that represents 0 milliseconds to test the wait handle and return immediately.</param>
        /// <returns>The object that was removed.</returns>
        /// <exception cref="ArgumentOutOfRangeException">millisecondsTimeout is a negative number other than -1, which represents an infinite timeout -or- timeout is greater than System.Int32.MaxValue.</exception>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        public Task<T> DequeueAsync(TimeSpan timeout) => DequeueAsync(timeout, default);

        /// <summary>Asynchronously waits to remove and return the object at the beginning of the <see cref="AsyncConcurrentQueue{T}"/>,
        /// using a <see cref="TimeSpan"/> to measure the time interval, while observing a <see cref="CancellationToken"/>.</summary>
        /// <param name="timeout">A <see cref="TimeSpan"/> that represents the number of milliseconds to wait, a <see cref="Timeout.InfiniteTimeSpan"/> that represents -1 milliseconds to wait indefinitely,
        /// or a <see cref="TimeSpan"/> that represents 0 milliseconds to test the wait handle and return immediately.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> to observe.</param>
        /// <returns>The object that was removed.</returns>
        /// <exception cref="ArgumentOutOfRangeException">millisecondsTimeout is a negative number other than -1, which represents an infinite timeout -or- timeout is greater than System.Int32.MaxValue.</exception>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        /// <exception cref="OperationCanceledException">cancellationToken was canceled.</exception>
        public async Task<T> DequeueAsync(TimeSpan timeout, CancellationToken cancellationToken)
        {
            CheckDispose();

            if (!await _semaphore.WaitAsync(timeout, cancellationToken))
            {
                throw new TimeoutException();
            }

            if (!_queue.TryDequeue(out T? result))
            {
                throw new ApplicationException();
            }

            return result;
        }

        /// <summary>Asynchronously waits to take and return the object at the beginning of the <see cref="AsyncConcurrentQueue{T}"/> without removing it.</summary>
        /// <returns>The object that was taken.</returns>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        public Task<T> PeekAsync() => PeekAsync(Timeout.InfiniteTimeSpan, default);

        /// <summary>Asynchronously waits to take and return the object at the beginning of the <see cref="AsyncConcurrentQueue{T}"/> without removing it, while observing a <see cref="CancellationToken"/>.</summary>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> to observe.</param>
        /// <returns>The object that was taken.</returns>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        /// <exception cref="OperationCanceledException">cancellationToken was canceled.</exception>
        public Task<T> PeekAsync(CancellationToken cancellationToken) => PeekAsync(Timeout.InfiniteTimeSpan, cancellationToken);

        /// <summary>Asynchronously waits to take and return the object at the beginning of the <see cref="AsyncConcurrentQueue{T}"/> without removing it,
        /// using a <see cref="TimeSpan"/> to measure the time interval.</summary>
        /// <param name="timeout">A <see cref="TimeSpan"/> that represents the number of milliseconds to wait, a <see cref="Timeout.InfiniteTimeSpan"/> that represents -1 milliseconds to wait indefinitely,
        /// or a <see cref="TimeSpan"/> that represents 0 milliseconds to test the wait handle and return immediately.</param>
        /// <returns>The object that was taken.</returns>
        /// <exception cref="ArgumentOutOfRangeException">millisecondsTimeout is a negative number other than -1, which represents an infinite timeout -or- timeout is greater than System.Int32.MaxValue.</exception>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        public Task<T> PeekAsync(TimeSpan timeout) => PeekAsync(timeout, default);

        /// <summary>Asynchronously waits to take and return the object at the beginning of the <see cref="AsyncConcurrentQueue{T}"/> without removing it,
        /// using a <see cref="TimeSpan"/> to measure the time interval, while observing a <see cref="CancellationToken"/>.</summary>
        /// <param name="timeout">A <see cref="TimeSpan"/> that represents the number of milliseconds to wait, a <see cref="Timeout.InfiniteTimeSpan"/> that represents -1 milliseconds to wait indefinitely,
        /// or a <see cref="TimeSpan"/> that represents 0 milliseconds to test the wait handle and return immediately.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> to observe.</param>
        /// <returns>The object that was taken.</returns>
        /// <exception cref="ArgumentOutOfRangeException">millisecondsTimeout is a negative number other than -1, which represents an infinite timeout -or- timeout is greater than System.Int32.MaxValue.</exception>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        /// <exception cref="OperationCanceledException">cancellationToken was canceled.</exception>
        public async Task<T> PeekAsync(TimeSpan timeout, CancellationToken cancellationToken)
        {
            CheckDispose();

            if (!await _semaphore.WaitAsync(timeout, cancellationToken))
            {
                throw new TimeoutException();
            }

            if (!_queue.TryPeek(out T? result))
            {
                throw new ApplicationException();
            }

            _semaphore.Release();

            return result;
        }

        /// <summary>Tries to remove and return the object at the beginning of the <see cref="AsyncConcurrentQueue{T}"/>.</summary>
        /// <param name="result">When this method returns, result contains the object removed. If no object was available to be removed, the value is unspecified.</param>
        /// <returns><see langword="true"/> if an element was removed and returned from the beginning of the <see cref="AsyncConcurrentQueue{T}"/> successfully; otherwise, <see langword="false"/>.</returns>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        public bool TryDequeue([NotNullWhen(true)] out T? result)
        {
            CheckDispose();

            if (!_semaphore.Wait(TimeSpan.Zero))
            {
                result = default;
                return false;
            }

            if (!_queue.TryDequeue(out result))
            {
                throw new ApplicationException();
            }

            return true;
        }

        /// <summary>Tries to return an object from the beginning of the <see cref="AsyncConcurrentQueue{T}"/> without removing it.</summary>
        /// <param name="result">When this method returns, result contains an object from the beginning of the <see cref="AsyncConcurrentQueue{T}"/> or an unspecified value if the operation failed.</param>
        /// <returns><see langword="true"/> if an object was returned successfully; otherwise, <see langword="false"/>.</returns>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        public bool TryPeek([NotNullWhen(true)] out T? result)
        {
            CheckDispose();

            if (!_semaphore.Wait(TimeSpan.Zero))
            {
                result = default;
                return false;
            }

            if (!_queue.TryPeek(out result))
            {
                throw new ApplicationException();
            }

            _semaphore.Release();

            return true;
        }

        /// <summary>Removes all objects from the <see cref="AsyncConcurrentQueue{T}"/>.</summary>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        public void Clear()
        {
            CheckDispose();

            _queue.Clear();
        }


        /// <summary>Copies the <see cref="AsyncConcurrentQueue{T}"/> elements to an existing one-dimensional <see cref="Array"/>, starting at the specified array index.</summary>
        /// <param name="array">The one-dimensional <see cref="Array"/> that is the destination of the elements copied from the <see cref="AsyncConcurrentQueue{T}"/>.
        /// The <see cref="Array"/> must have zero-based indexing.</param>
        /// <param name="index">The zero-based index in array at which copying begins.</param>
        /// <exception cref="ArgumentNullException">array is a <see langword="null"/> reference (<see langword="Nothing"/> in Visual Basic).</exception>
        /// <exception cref="ArgumentOutOfRangeException">index is less than zero.</exception>
        /// <exception cref="ArgumentException">index is equal to or greater than the length of the array -or-
        /// The number of elements in the source <see cref="AsyncConcurrentQueue{T}"/> is greater than the available space from index to the end of the destination array.</exception>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        public void CopyTo(T[] array, int index)
        {
            CheckDispose();

            _queue.CopyTo(array, index);
        }

        /// <summary>Attempts to add an object to the <see cref="IProducerConsumerCollection{T}"/>.</summary>
        /// <param name="item">The object to add to the <see cref="IProducerConsumerCollection{T}"/>.
        /// The value can be a <see langword="null"/> reference (<see langword="Nothing"/> in Visual Basic) for reference types.</param>
        /// <returns><see langword="true"/> if the object was added successfully; otherwise, <see langword="false"/>.</returns>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        bool IProducerConsumerCollection<T>.TryAdd(T item)
        {
            CheckDispose();

            Enqueue(item);
            return true;
        }

        /// <summary>Attempts to remove and return an object from the <see cref="IProducerConsumerCollection{T}"/>.</summary>
        /// <param name="item">When this method returns, if the operation was successful, item contains the object removed. If no object was available to be removed, the value is unspecified.</param>
        /// <returns><see langword="true"/> if an element was removed and returned successfully; otherwise, <see langword="false"/>.</returns>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        bool IProducerConsumerCollection<T>.TryTake([NotNullWhen(true)] out T? item)
        {
            CheckDispose();

            return TryDequeue(out item);
        }

        /// <summary>Copies the elements stored in the <see cref="AsyncConcurrentQueue{T}"/> to a new array.</summary>
        /// <returns>A new array containing a snapshot of elements copied from the <see cref="AsyncConcurrentQueue{T}"/>.</returns>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        public T[] ToArray()
        {
            CheckDispose();

            return _queue.ToArray();
        }

        /// <summary>Returns an enumerator that iterates through the <see cref="AsyncConcurrentQueue{T}"/>.</summary>
        /// <returns>An enumerator for the contents of the <see cref="AsyncConcurrentQueue{T}"/>.</returns>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        public IEnumerator<T> GetEnumerator()
        {
            CheckDispose();

            return _queue.GetEnumerator();
        }

        /// <summary>Returns an enumerator that iterates through a collection.</summary>
        /// <returns>An <see cref="IEnumerator"/> that can be used to iterate through the collection.</returns>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        IEnumerator IEnumerable.GetEnumerator()
        {
            CheckDispose();

            return (_queue as IEnumerable).GetEnumerator();
        }

        /// <summary>Gets the number of elements contained in the <see cref="AsyncConcurrentQueue{T}"/>.</summary>
        /// <returns>The number of elements contained in the <see cref="AsyncConcurrentQueue{T}"/>.</returns>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        public int Count
        {
            get
            {
                CheckDispose();

                return _queue.Count;
            }
        }

        /// <summary>Gets an object that can be used to synchronize access to the <see cref="ICollection"/>. This property is not supported.</summary>
        /// <returns>Returns <see langword="null"/> (<see langword="Nothing"/> in Visual Basic).</returns>
        /// <exception cref="NotSupportedException">The SyncRoot property is not supported.</exception>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        object ICollection.SyncRoot
        {
            get
            {
                CheckDispose();

                return (_queue as ICollection).SyncRoot;
            }
        }

        /// <summary>Gets a value indicating whether access to the <see cref="ICollection"/> is synchronized with the SyncRoot.</summary>
        /// <returns>Always returns <see langword="false"/> to indicate access is not synchronized.</returns>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        bool ICollection.IsSynchronized
        {
            get
            {
                CheckDispose();

                return (_queue as ICollection).IsSynchronized;
            }
        }

        /// <summary>Copies the elements of the <see cref="ICollection"/> to an <see cref="Array"/>, starting at a particular <see cref="Array"/> index.</summary>
        /// <param name="array">The one-dimensional <see cref="Array"/> that is the destination of the elements copied from the <see cref="AsyncConcurrentQueue{T}"/>.
        /// The <see cref="Array"/> must have zero-based indexing.</param>
        /// <param name="index">The zero-based index in array at which copying begins.</param>
        /// <exception cref="ArgumentNullException">array is a <see langword="null"/> reference (<see langword="Nothing"/> in Visual Basic).</exception>
        /// <exception cref="ArgumentOutOfRangeException">index is less than zero.</exception>
        /// <exception cref="ArgumentException">array is multidimensional. -or- array does not have zero-based indexing. -or- index is equal to or greater than the length of the array -or-
        /// The number of elements in the source <see cref="ICollection"/> is greater than the available space from index to the end of the destination array. -or-
        /// The type of the source <see cref="ICollection"/> cannot be cast automatically to the type of the destination array.</exception>
        /// <exception cref="ObjectDisposedException">The current instance has already been disposed.</exception>
        void ICollection.CopyTo(Array array, int index)
        {
            CheckDispose();

            (_queue as ICollection).CopyTo(array, index);
        }


        private void CheckDispose()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(nameof(AsyncConcurrentQueue<T>));
            }
        }

        private bool _disposed;

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    _semaphore.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~AsyncConcurrentQueue()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        /// <summary>Releases all resources used by the current instance of <see cref="AsyncConcurrentQueue{T}"/>.</summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
