﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;

namespace Goldfynger.Utils.Collections
{
    /// <summary></summary>
    /// <typeparam name="T"></typeparam>
    public sealed class NotifyCollectionChangedEventArgs<T>
    {
        /// <summary></summary>
        /// <param name="args"></param>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedEventArgs args)
        {
            Args = args;

            if (args.OldItems != null)
            {
                OldItems = new ReadOnlyCollection<T>(args.OldItems.Cast<T>().ToList());
            }

            if (args.NewItems != null)
            {
                NewItems = new ReadOnlyCollection<T>(args.NewItems.Cast<T>().ToList());
            }
        }


        /// <summary></summary>
        public ReadOnlyCollection<T>? OldItems { get; } = null;

        /// <summary></summary>
        public ReadOnlyCollection<T>? NewItems { get; } = null;

        /// <summary></summary>
        public NotifyCollectionChangedEventArgs Args { get; }
    }

    /// <summary></summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void NotifyCollectionChangedEventHandler<T>(object sender, NotifyCollectionChangedEventArgs<T> e);
}