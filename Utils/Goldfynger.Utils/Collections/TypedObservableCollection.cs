﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Goldfynger.Utils.Collections
{
    /// <summary></summary>
    /// <typeparam name="T"></typeparam>
    public class TypedObservableCollection<T> : ObservableCollection<T>
    {
        /// <summary></summary>
        public TypedObservableCollection() { }

        /// <summary></summary>
        /// <param name="list"></param>
        public TypedObservableCollection(List<T> list) : base(list) { }

        /// <summary></summary>
        /// <param name="collection"></param>
        public TypedObservableCollection(IEnumerable<T> collection) : base(collection) { }

        /// <summary></summary>
        /// <param name="args"></param>
        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs args)
        {
            base.OnCollectionChanged(args);

            TypedCollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs<T>(args));
        }

        /// <summary></summary>
        public event NotifyCollectionChangedEventHandler<T>? TypedCollectionChanged;
    }
}