﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Goldfynger.Utils.Extensions;

namespace Goldfynger.Utils.Collections
{
    /// <summary></summary>
    [DebuggerDisplay("Count = {_dictionary.Count}")]
    [Obsolete("HashCollection is obsolete. There is no known application that uses it.", true)]
    public sealed class HashCollection<T> : IEnumerable, ICollection, IEnumerable<T>, ICollection<T>, IReadOnlyCollection<T> where T : notnull
    {
        /// <summary></summary>
        private readonly Dictionary<T, T> _dictionary;


        /// <summary></summary>
        public HashCollection() => _dictionary = new Dictionary<T, T>();

        /// <summary></summary>
        /// <param name="enumerable"></param>
        public HashCollection(IEnumerable<T> enumerable) => _dictionary = new Dictionary<T, T>(enumerable.ToDictionary(x => x));

        /// <summary></summary>
        public int Count => _dictionary.Count;

        /// <summary></summary>
        /// <param name="item"></param>
        public void Add(T item) => _dictionary.Add(item, item);

        /// <summary> </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Remove(T item) => _dictionary.Remove(item);

        /// <summary></summary>
        public void Clear() => _dictionary.Clear();

        /// <summary></summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(T item) => _dictionary.ContainsKey(item);

        /// <summary></summary>
        /// <param name="array"></param>
        /// <param name="arrayIndex"></param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));

            if (arrayIndex < 0 || arrayIndex > array.Length || array.Length - arrayIndex < _dictionary.Count) throw new ArgumentOutOfRangeException(nameof(arrayIndex));

            _dictionary.ForEach(p => array[arrayIndex++] = p.Key);
        }

        /// <summary></summary>
        /// <param name="array"></param>
        /// <param name="index"></param>
        public void CopyTo(Array array, int index)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));

            if (index < 0 || index > array.Length || array.Length - index < _dictionary.Count) throw new ArgumentOutOfRangeException(nameof(index));


            if (array is T[] values)
            {
                CopyTo(values, index);
            }
            else
            {
                if (array is not object[] objects) throw new ArgumentException("Invalid array type.");

                _dictionary.ForEach(p => objects[index++] = p.Key);
            }
        }

        /// <summary></summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            foreach (var pair in _dictionary)
            {
                yield return pair.Key;
            }
        }

        /// <summary></summary>
        /// <returns></returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            foreach (var pair in _dictionary)
            {
                yield return pair.Key;
            }
        }

        /// <summary></summary>
        public bool IsReadOnly => false;

        /// <summary></summary>
        public bool IsSynchronized => ((ICollection)_dictionary).IsSynchronized;

        /// <summary></summary>
        public object SyncRoot => ((ICollection)_dictionary).SyncRoot;
    }
}