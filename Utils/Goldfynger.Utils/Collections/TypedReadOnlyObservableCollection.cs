﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Goldfynger.Utils.Collections
{
    /// <summary></summary>
    /// <typeparam name="T"></typeparam>
    public class TypedReadOnlyObservableCollection<T> : ReadOnlyObservableCollection<T>
    {
        /// <summary></summary>
        /// <param name="list"></param>
        public TypedReadOnlyObservableCollection(ObservableCollection<T> list) : base(list) { }

        /// <summary></summary>
        /// <param name="args"></param>
        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs args)
        {
            base.OnCollectionChanged(args);

            TypedCollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs<T>(args));
        }
        
        /// <summary></summary>
        public event NotifyCollectionChangedEventHandler<T>? TypedCollectionChanged;
    }
}